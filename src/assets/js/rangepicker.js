
function setrangepicker(min, start, end){
    $('input[name="daterange"]').daterangepicker({
        opens: 'left',
        showDropdowns: true,
        minDate: min,
        startDate: start,
        endDate: end,
        locale: {
            format: "DD-MM-YYYY"
        }
    });
}

function dateDiff(startdate, enddate) {
    //define moments for the startdate and enddate
    var startdateMoment = moment(startdate,'DD-MM-YYYY');
    var enddateMoment = moment(enddate,'DD-MM-YYYY');

    if (startdateMoment.isValid() === true && enddateMoment.isValid() === true) {
      //getting the difference in years
      var years = enddateMoment.diff(startdateMoment, 'years');

      //moment returns the total months between the two dates, subtracting the years
      var months = enddateMoment.diff(startdateMoment, 'months') - (years * 12);

      //to calculate the days, first get the previous month and then subtract it
      startdateMoment.add(years, 'years').add(months, 'months');
      var days = enddateMoment.diff(startdateMoment, 'days')

      return days;
    }
    else {
      return 0;
    }

  }