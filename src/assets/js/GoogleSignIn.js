﻿function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 150,
        'height': 33,
        'margin-left': 42,
        'margin-top': 6,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

// Sign-in success callback
function onSuccess(googleUser) {
    // Get the Google profile data (basic)
    //var profile = googleUser.getBasicProfile();
    // Retrieve the Google account data
    gapi.client.load('oauth2', 'v2', function () {
        var request = gapi.client.oauth2.userinfo.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            var data = {
                Id: resp.id,
                name: resp.name,
                Email: resp.email,
                profile: resp.picture
            };

            $.ajax({
                type: "POST",
                url: "../handler/LoginDetail.asmx/CheckGoogleSession",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.response != 0) {
                        CheckloginUser();
                        if (Request != "") {
                            if (Request == "profile") {
                                window.location.href = "User/UserProfile.aspx";
                            }
                            if (Request == "bookings") {
                                window.location.href = "User/VisaList.aspx";
                            }
                            if (Request == "bookVisa") {
                                window.location = "../User/AddVisaDetails.aspx?&Name=" + $("#Name").val() + "&Price=" + $("#Price").val() + "&Country=" + $("#Country").val();
                            }
                        }
                        $("#log_out").attr("onclick", "signOut()");
                        $("#ModelLogin").modal('hide');
                    }
                },
            });
        });
    });
}


function onFailure(error) {
    alert(error);
}


//function onSignIn(googleUser) {
//    // Useful data for your client-side scripts:
//    var profile = googleUser.getBasicProfile();
//    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
//    console.log('Full Name: ' + profile.getName());
//    console.log('Given Name: ' + profile.getGivenName());
//    console.log('Family Name: ' + profile.getFamilyName());
//    console.log("Image URL: " + profile.getImageUrl());
//    console.log("Email: " + profile.getEmail());

//    // The ID token you need to pass to your backend:
//    var id_token = googleUser.getAuthResponse().id_token;
//    console.log("ID Token: " + id_token);
//    var Id = profile.getId();
//    var name = profile.getName();
//    var Email = profile.getEmail();
//    var Img = profile.getImageUrl();

//    // alert("Login Successfully.");
//    // if (Name == null || Name == undefined && Price == null || Price== undefined) {
//    //     $("#ModelLogin").modal('hide');
//    // }
//    //if (Id != null || Id != undefined)
//    //{
//    //    var Name = $("#Name").val();
//    //    var Price = $("#Price").val();
//    //    var Country = $("#Country").val();
//    //        CheckSession();

//    //   window.location = "../User/AddVisaDetails.aspx?&Name=" + Name + "&Price=" + Price + "&Country=" + Country;
//    // }

//    var data = {
//        Id: Id,
//        name: name,
//        Email: Email,
//        profile: profile
//    };

//    $.ajax({
//        type: "POST",
//        url: "../handler/LoginDetail.asmx/CheckGoogleSession",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.response != 0) {
//                CheckloginUser();
//                if (Request != "") {
//                    if (Request == "profile") {
//                        window.location.href = "User/UserProfile.aspx";
//                    }
//                    if (Request == "bookings") {
//                        window.location.href = "User/VisaList.aspx";
//                    }
//                    if (Request == "bookVisa") {
//                        window.location = "../User/AddVisaDetails.aspx?&Name=" + $("#Name").val() + "&Price=" + $("#Price").val() + "&Country=" + $("#Country").val();
//                    }
//                }
//                $("#log_out").attr("onclick", "signOut()");
//                $("#ModelLogin").modal('hide');
//            }
//        },
//    });
//};

window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '2331184353819458', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    // Check whether the user already logged in
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            alert('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'email' });
}

// Fetch the user profile data from facebook
function getFbUserData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
    function (response) {
        if (response.id != null || response.id != undefined) {
            var email = "";
            if (response.email != undefined) {
                email = response.email;
            }
            var data = {
                Id: response.id,
                name: response.first_name + " " + response.last_name,
                Email: email,
            };
            $.ajax({
                type: "POST",
                url: "../handler/LoginDetail.asmx/CheckGoogleSession",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.response != 0) {
                        CheckloginUser();
                        if (Request != "") {
                            if (Request == "profile") {
                                window.location.href = "User/UserProfile.aspx";
                            }
                            if (Request == "bookings") {
                                window.location.href = "User/VisaList.aspx";
                            }
                            if (Request == "bookVisa") {
                                window.location = "../User/AddVisaDetails.aspx?&Name=" + $("#Name").val() + "&Price=" + $("#Price").val() + "&Country=" + $("#Country").val();
                            }
                        }
                    }

                },
            });
            $("#log_out").attr("onclick", "fbLogout()");
            $("#ModelLogin").modal('hide');
        }
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function (response) {
        Logout();
        $("#log_out").attr("onclick", "Logout()");
    });
}


function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        Logout();
        $("#log_out").attr("onclick", "Logout()");
    });
    auth2.disconnect();
}
//function signOut() {
//    var auth2 = gapi.auth2.getAuthInstance();
//    auth2.signOut().then(function () {
//        console.log('User signed out.');
//        Logout();
//        $("#log_out").attr("onclick", "Logout()");
//    });
//    //alert("googleLogout done.");
//    //var auth2 = gapi.auth2.getAuthInstance();
//    //auth2.signOut().then(function () {
//    //    console.log('User signed out.');
//    //    auth2.disconnect();

//    //});
//    //auth2.disconnect();
//    //window.location = "https://mail.google.com/mail/u/0/?logout&hl=en";
//}


