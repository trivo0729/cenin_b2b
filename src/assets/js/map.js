	function closeInfoBox() {
		$('div.infoBox').remove();
	};

	function getInfoBox(item) {
		debugger
		return new InfoBox({
			content:
			'<div class="marker_info" id="marker_info">' +
			'<img  style="width:280px;height:180px" src="' + item.map_image_url + '" alt="Image"/>' +
			'<h3>'+ item.name_point +'</h3>' +
			'<span>'+ item.description_point +'</span>' +
			'<div class="marker_tools">' +
			'<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
				'<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
				'</div>' +
				'<a (click)="ShowTour(\''+item.id+'\')" class="btn_infobox">Details</a>' +
			'</div>',
			disableAutoPan: false,
			maxWidth: 0,
			pixelOffset: new google.maps.Size(10, 125),
			closeBoxMargin: '5px -20px 2px 2px',
			closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
			isHidden: false,
			alignBottom: true,
			pane: 'floatPane',
			enableEventPropagation: true
		});


	};