import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { AccountService } from 'src/app/services/agent/account.service';
import { MenuService } from 'src/app/services/commons/menu.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
declare function Menu(): any;

@Component({
  selector: 'app-travel-header',
  templateUrl: './travel-header.component.html',
  styleUrls: ['./travel-header.component.css']
})
export class TravelHeaderComponent implements OnInit {
  @Output() loginDialog = new EventEmitter();
  @Output() RegtrDialog = new EventEmitter();
  @Output() _logout = new EventEmitter();
  @Input() login : any;
  CustomerName: string = null;
  CustomerEmail: string = null;
  UsetId: number; 
  Destination : string;
  constructor(
    private menuService: MenuService,
    private userService: UserService,
    public objGlobal : ApiUrlService,
    private router : Router,
    private cookie: CommonCookieService,
    private accountService: AccountService,
    private session :SessionService,
  ) { }

  ngOnInit() {
    Menu();
    this.login = this.login;
    debugger;
    console.log(this.login)
    debugger
    this.login = this.login;
    debugger
    var destination = this.session.get("destination");
    if(destination == undefined || destination=="")
        this.session.set("destination",JSON.stringify({"destination":"Jeddah"}))
     this.Destination = this.session.get("destination").destination;
  }

  onSigninDialog() {
    this.loginDialog.emit();
  }

  onRegisterDialog() {
    this.RegtrDialog.emit();
  }

  onSignout() {
    this._logout.emit();
  }

  RouterLink(Type: string){
    debugger;
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url;

    if (currentUrl === '/package-list') {
      this.router.navigateByUrl(currentUrl)
        .then(() => {
          this.router.navigated = false;
          this.router.navigate([this.router.url], { queryParams: { Type : Type} });
        });
    }
    else
          this.router.navigate(['/package-list'] ,{ queryParams : { Type : Type}  } );




  }
  setDeatination(detiny:string){
    try{
      this.Destination = detiny;
      this.session.set("destination",JSON.stringify({"destination": this.Destination}))
      window.location.reload();
    }catch(e){}
  }

}
