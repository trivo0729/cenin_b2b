import { Component, OnInit, Input } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-travle-things-icons',
  templateUrl: './travle-things-icons.component.html',
  styleUrls: ['./travle-things-icons.component.css']
})
export class TravleThingsIconsComponent implements OnInit {
  @Input() Destination:string
  constructor(
    private session :SessionService,
  ) { }

  ngOnInit() {
    var destination = this.session.get("destination");
    if(destination == undefined || destination=="")
        this.session.set("destination",JSON.stringify({"destination":"Jeddah"}))
     this.Destination = this.session.get("destination").destination;
  }

}
