import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
declare function setimage(img:string):any;

@Component({
  selector: 'app-detsination-jeddah',
  templateUrl: './detsination-jeddah.component.html',
  styleUrls: ['./detsination-jeddah.component.css']
})
export class DetsinationJeddahComponent implements OnInit {
  Destination :string;
  constructor(
    private session :SessionService,
  ) { }

  ngOnInit() {
    var destination = this.session.get("destination");
    if(destination == undefined || destination=="")
        this.session.set("destination",JSON.stringify({"destination":"Jeddah"}))
     this.Destination = this.session.get("destination").destination;
    setTimeout(() => {
      setimage('assets/img/travelsphere/bg_blog.jpg');
    }, 20);
  }

}
