import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
declare function allScript() :any
@Component({
  selector: 'app-travel-footer',
  templateUrl: './travel-footer.component.html',
  styleUrls: ['./travel-footer.component.css']
})
export class TravelFooterComponent implements OnInit {

  constructor(
    public objGlobal: ApiUrlService,
  ) { }

  ngOnInit() {
    allScript() ;
  }

}
