import { Component, OnInit } from '@angular/core';
declare function setimage(img:string):any;
@Component({
  selector: 'app-travel-about',
  templateUrl: './travel-about.component.html',
  styleUrls: ['./travel-about.component.css']
})
export class TravelAboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      setimage('assets/img/travelsphere/bg_blog.jpg');
    }, 20);
  }

}
