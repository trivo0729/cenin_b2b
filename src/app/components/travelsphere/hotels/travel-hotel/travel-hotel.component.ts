import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare function owalCarousel():any;
@Component({
  selector: 'app-travel-hotel',
  templateUrl: './travel-hotel.component.html',
  styleUrls: ['./travel-hotel.component.css']
})
export class TravelHotelComponent implements OnInit {
 Hotels:any[];
 @Input() Destination:string
  constructor(
    private httpClient: HttpClient,
   
  ) { }

  ngOnInit() {
    //this.load()
    setTimeout(()=>{owalCarousel();},1000) 
   }
  load(){
    var temp =[];
    this.Hotels=[];
    this.getHotelsfromjson().subscribe((res) => {
      debugger;
      temp  = res.Hotels;
      temp.filter(d=>{
        if(d.Destination== this.Destination)
            this.Hotels.push(d);
      })
     
    });
  }
  getHotelsfromjson(): Observable<any> {
    return this.httpClient.get("./assets/json/hotels.json");
  }

}
