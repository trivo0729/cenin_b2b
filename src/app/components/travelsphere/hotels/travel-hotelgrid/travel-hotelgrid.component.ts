import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from 'src/app/services/session.service';
declare function setimage(img:string):any;
@Component({
  selector: 'app-travel-hotelgrid',
  templateUrl: './travel-hotelgrid.component.html',
  styleUrls: ['./travel-hotelgrid.component.css']
})
export class TravelHotelgridComponent implements OnInit {
  Destination :string;
  Hotels:any[];
  constructor(
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private cd: ChangeDetectorRef,
    private session :SessionService,
  ) { }

  ngOnInit() {
    var destination = this.session.get("destination");
    if(destination == undefined || destination=="")
        this.session.set("destination",JSON.stringify({"destination":"Jeddah"}))
     this.Destination = this.session.get("destination").destination;
    // this.Destination = this.route.snapshot.queryParamMap.get('name');
    setTimeout(() => {
      setimage('https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2015/03/03/1625/Park-Hyatt-Jeddah-P208-Exterior-Fountains.jpg/Park-Hyatt-Jeddah-P208-Exterior-Fountains.16x9.jpg');
    }, 20);
    this.load();
  }

  load(){
    var temp =[];
    this.Hotels=[];
    this.getHotelsfromjson().subscribe((res) => {
      debugger;
      temp  = res.Hotels;
      temp.filter(d=>{
       if(d.Destination== this.Destination)
            this.Hotels.push(d);
      })
      this.cd.detectChanges();
    });
  }
  getHotelsfromjson(): Observable<any> {
    return this.httpClient.get("./assets/json/hotels.json");
  }
}
