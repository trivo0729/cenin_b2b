import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-travel-resturants',
  templateUrl: './travel-resturants.component.html',
  styleUrls: ['./travel-resturants.component.css']
})
export class TravelResturantsComponent implements OnInit {
  Hotels:any[];
  @Input() Destination:string;
   constructor(
     private httpClient: HttpClient,
   ) { }
 
   ngOnInit() {
  //  var temp =[];
  //    this.Hotels=[];
  //    this.getHotelsfromjson().subscribe((res) => {
  //      temp  = res.Hotels;
  //      temp.filter(d=>{
  //       // //  if(d.Destination== Destination)
  //            this.Hotels.push(d);
  //      })
  //    });
   }  
   
   getHotelsfromjson(): Observable<any> {
     return this.httpClient.get("./assets/json/SearchHotelresforDiffOcc.json");
   }

}
