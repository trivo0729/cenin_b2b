import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from 'src/app/services/session.service';
declare function setimage(img:string):any;
@Component({
  selector: 'app-travel-reasturantgrid',
  templateUrl: './travel-reasturantgrid.component.html',
  styleUrls: ['./travel-reasturantgrid.component.css']
})
export class TravelReasturantgridComponent implements OnInit {
  Destination :string;
  Resturants:any[];
  constructor(
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private cd: ChangeDetectorRef,
    private session :SessionService,
  ) { }

  ngOnInit() {
    var destination = this.session.get("destination");
    if(destination == undefined || destination=="")
        this.session.set("destination",JSON.stringify({"destination":"Jeddah"}))
     this.Destination = this.session.get("destination").destination;
    // this.Destination = this.route.snapshot.queryParamMap.get('name');
    setTimeout(() => {
      setimage('http://www.belajio.com/gallery/d6a85f7d-f6c5-422f-b3a7-d319cdb3d7f6.JPG');
    }, 20);
    this.load();
  }

  load(){
    var temp =[];
    this.Resturants=[];
    this.getHotelsfromjson().subscribe((res) => {
      debugger;
      temp  = res.Resturants;
      temp.filter(d=>{
        if(d.Destination== this.Destination)
            this.Resturants.push(d);
      })
      this.cd.detectChanges();
    });
  }
  getHotelsfromjson(): Observable<any> {
    return this.httpClient.get("./assets/json/hotels.json");
  }
}
