import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { HotelService } from 'src/app/services/hotel/hotel.service';
declare function Carousel(): any;

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css']
})
export class HotelDetailsComponent implements OnInit {
  @Input() selectedTab: any;
  @Input() id: any;
  @Input() HotelDetails: any;
  Images: any[];
  zoom: number = 14;
  lat: number;
  lng: number;
  loading: boolean;
  constructor(private cd: ChangeDetectorRef,
    private hotelService: HotelService
  ) { }

  ngOnInit() {
    debugger;
    this.loading = false;
    this.Images = [];
    this.getHotelDetails();
  }

  ngAfterViewInit() {
    //Carousel();
  }

  setImage(event: any) {
    Carousel();
    this.lat = parseFloat(this.HotelDetails.Latitude);
    this.lng = parseFloat(this.HotelDetails.Langitude);
  }

  getHotelDetails() {
    this.loading = true;
    this.hotelService.getHotelImages(this.id, this.HotelDetails.ResultIndex).subscribe((res: any) => {
      this.loading = false;
      if (res.retCode === 1) {
        this.Images = res.Images;
      }
      this.cd.detectChanges();
      Carousel();
    }, error => {
      this.loading = false;
    });
  }

}
