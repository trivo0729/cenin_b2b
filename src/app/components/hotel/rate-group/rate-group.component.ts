import { Component, OnInit, Input } from '@angular/core';
import { SearchRateGroup, ListRateGroup } from 'src/app/modals/hotel/hotel.modal';
import { HotelService } from 'src/app/services/hotel/hotel.service';

@Component({
  selector: 'app-rate-group',
  templateUrl: './rate-group.component.html',
  styleUrls: ['./rate-group.component.css']
})
export class RateGroupComponent implements OnInit {
   @Input() arrSearch:any;
   @Input() HotelCode:any;
   @Input() RateGroups: ListRateGroup;
  Rooms(length: number): Array<any> {
    if (length >= 0) {
      return new Array(length);
    }
  }
  constructor(private hotelService : HotelService) {}
  ngOnInit() {
    // this.arrRateGroup = new SearchRateGroup();
    // this.arrRateGroup.TokenId = this.TokenId;
    // this.arrRateGroup.HotelCode = this.HotelCode;
    // this.arrRateGroup.Checkin = this.arrSearch.Checkin;
    // this.arrRateGroup.Checkout = this.arrSearch.Checkout;
    // this.arrRateGroup.nationality = this.arrSearch.nationality;
    // this.arrRateGroup.SearchType = this.arrSearch.SearchType;
    // this.arrRateGroup.UserName = this.arrSearch.username;
    // this.arrRateGroup.Password = this.arrSearch.Password;
    // this.arrRateGroup.Rooms = this.arrSearch.Rooms;
    // this.getRategroup(); 
  }
 /** Get rate group response **/
  // getRategroup(arrSearch:any, HotelCode:any, TokenId:any){
  //   debugger;
  //   this.arrRateGroup = new SearchRateGroup();
  //   this.arrRateGroup.TokenId = TokenId;
  //   this.arrRateGroup.HotelCode = HotelCode;
  //   this.arrRateGroup.Checkin = arrSearch.Checkin;
  //   this.arrRateGroup.Checkout = arrSearch.Checkout;
  //   this.arrRateGroup.nationality = arrSearch.nationality;
  //   this.arrRateGroup.SearchType = arrSearch.SearchType;
  //   this.arrRateGroup.UserName = arrSearch.username;
  //   this.arrRateGroup.Password = arrSearch.Password;
  //   this.arrRateGroup.Rooms = arrSearch.Rooms;
  //   // this.hotelService.getRategroup(this.arrRateGroup).subscribe((res: any) => {
  //   //   this.ListRateGroups = res;
  //   // });
  //     this.hotelService.getRategroup().subscribe((res: any) => {
  //       this.RateGroups = res;
  //       console.log(res);
  //   });
  // }
}
