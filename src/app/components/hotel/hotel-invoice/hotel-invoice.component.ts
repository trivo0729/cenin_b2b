import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ReportService } from 'src/app/services/commons/report.service';
import { ArrResrvation, AgentDetails, Guestlist, Roomlist, Invoice, HotelDetails, AdminUser } from 'src/app/modals/hotel/report.modal';

import { ActivatedRoute } from '@angular/router';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-hotel-invoice',
  templateUrl: './hotel-invoice.component.html',
  styleUrls: ['./hotel-invoice.component.css']
})
export class HotelInvoiceComponent implements OnInit {
  @Input() UserId: any;
  @Input() ReservationID: any;
  @Output() bookingAction = new EventEmitter();
  userdetails: Userdetails;
  public scale = 0.6;
  InvoiceDetails: Invoice;
  isb2c: boolean;
  constructor(private reportService: ReportService,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private cookie: CommonCookieService
  ) { }

  ngOnInit() {
    debugger
    this.userdetails = new Userdetails();
    this.InvoiceDetails = new Invoice();
    this.InvoiceDetails.arrResrvation = new ArrResrvation();
    this.InvoiceDetails.arrPaxes = [];
    this.InvoiceDetails.arrRooms = [];
    this.InvoiceDetails.RateBreakups = [];
    this.InvoiceDetails.CancellationPolicy = [];
    this.InvoiceDetails.dtHotelAdd = new HotelDetails();
    this.InvoiceDetails.AgentDetails = new AgentDetails();
    this.InvoiceDetails.AdminUser = new AdminUser();
    console.log(this.UserId);
    console.log(this.ReservationID);
    // this.UserId = this.route.snapshot.queryParamMap.get('parm_1');
    // this.ReservationID = this.route.snapshot.queryParamMap.get('parm_2');
    this.reportService.GetHotelInvoice(this.UserId, this.ReservationID).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.InvoiceDetails = res;
        this.bookingAction.emit("Invoice");
      }
      console.log(res);
    }, error => {
      console.log(error);
    });
    this.getUserdata();
  }

  getUserdata() {
    debugger
    if (this.cookie.checkcookie('login')) {
      let user = JSON.parse(this.cookie.getcookie('login'));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B")
        this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
    this.cd.detectChanges();
  }
}
