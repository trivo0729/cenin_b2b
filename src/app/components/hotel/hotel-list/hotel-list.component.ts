import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { Hotels, SearchRateGroup, _arrRoom } from 'src/app/modals/hotel/hotel.modal';
import { objTraveller, Traveller, Childs } from 'src/app/modals/hotel/search.modal';
import { objSearch } from 'src/app/modals/hotel/search.modal';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelService } from 'src/app/services/hotel/hotel.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { arrBlock, BookingDetails } from 'src/app/modals/hotel/block.modal';
import { AlertService } from 'src/app/services/alert.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css'],
  providers: [NgbPaginationConfig], // add NgbPaginationConfig to the component providers
  changeDetection: ChangeDetectionStrategy.OnPush,
})


export class HotelListComponent implements OnInit {
  Traveller: Traveller[];
  public Ages: Array<any>;
  Childs: Childs[]
  objSearch: objSearch;
  objTraveller: objTraveller;
  Hotels: Hotels[];
  FilterHotels: Hotels[];
  TockenID: string;
  page: number = 1;
  arrRateGroup: SearchRateGroup;
  Adult: number = 0;
  Child: number = 0;
  _arrRoom: _arrRoom;
  PriviousSelected: any;
  nextSelected: any;
  Category: any;
  Facility: any;
  MaxPrice: any;
  MinPrice: any;
  CountHotel: number;
  DisplayRequest: any;
  Location: any;
  Currency: string;
  _nearby: string;
  _response: boolean;
  arrBlock: arrBlock;
  BookingDetails: BookingDetails[];
  hoteldetail: boolean;
  _hotelData: any;
  selectedTab: number;
  error: boolean = false;
  errorStatus: any;
  Rindex: number;
  zoom: number;
  selectedHotel: Hotels;
  SupplierVisible: boolean;
  pageSize = 50;
  arrFiterRates  : any[]; /* set data into filter rate group*/
  arrFiterRoom={ ResultIndex  : "",  arrRooms :[] , arrMeals: [], List_RateGroup:[], RoomType:"" , Meal:"" ,Room:"" };/* to bind data in rate group filter*/
  arrRoomType:[];
  constructor(
    private route: ActivatedRoute,
    private hotelService: HotelService,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog,
    private Alert: AlertService,
    private ngxService: NgxUiLoaderService,
    private router: Router,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }

  ngOnInit() {
    this.selectedHotel = new Hotels();
    this.zoom = 12;
    this.hoteldetail = false;
    this._response = false;
    this.Hotels = [];
    this.FilterHotels = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objSearch = new objSearch();
    this.objTraveller = new objTraveller();
    this.objSearch = this.hotelService.getHotelSerachParams();
    this.setSearchpars();
    this.gethotelResponse(this.objSearch)
  }


  onDetails(Hotelcode: string) {
    this.Hotels = this.Hotels.filter(h => h.HotelId === Hotelcode);
    setTimeout(() => {
      let element: HTMLElement = document.getElementById('det_' + Hotelcode) as HTMLElement;
      element.click();
    }, 1500);
  }

  clickedMarker(ResultIndex: any) {
    this.selectedHotel = new Hotels();
    this.selectedHotel = this.Hotels.filter(h => h.ResultIndex === ResultIndex)[0];
  }

  gethotelResponse(objSearch: any) {
    this.hotelService.getHotels(objSearch).subscribe((res: any) => {
      this._response = true;
      console.log(res);
      if (res.retCode === 1) {
        this.setHotelResponse(res);
        //this.tokenSession();
      }
      this.cd.detectChanges();
    }, error => {
      this.hendel(error);
    });
  }


  selecteRoomFiter(no: number, event: any, ResultIndex: any){
    try {
      debugger
      this.arrFiterRoom.arrRooms=[];
      var arrRateGroup = this.arrFiterRates.find(h => h.ResultIndex === ResultIndex).List_RateGroup;
      const filterValue = event.target.value.toLowerCase();
      if(no==2){
       if (filterValue.length !=0) {
        this.arrFiterRoom =  this.arrFiterRates.find(h => h.ResultIndex === ResultIndex);
        var arrRooms = this._getListRoom(arrRateGroup)
        this.arrFiterRoom.arrRooms= arrRooms.filter(d=>d.toLowerCase().indexOf(filterValue) === 0)
       }
       else{
         arrRateGroup= this.arrFiterRoom.List_RateGroup
         this.Hotels.find(d=>d.ResultIndex == ResultIndex).List_RateGroup = arrRateGroup;
         console.log( this.Hotels.find(d=>d.ResultIndex == ResultIndex).List_RateGroup);
       }
      }else if(no==3) {
        this.arrFiterRoom =  this.arrFiterRates.find(h => h.ResultIndex === ResultIndex);
        var arrMeals = this._getListMeals(arrRateGroup)
        this.arrFiterRoom.arrMeals= arrMeals.filter(d=>d.toLowerCase().indexOf(filterValue) === 0)
      }
      this.cd.detectChanges();
    }catch(e){
      console.log(e.message);
    }
  }
  
  filterRateGroup(no: number, event: any, ResultIndex: any) {
    debugger
    try{
       var hotel=  this.Hotels.find(d=>d.ResultIndex == ResultIndex);
      if (no==1) {
        
      }
      else if (no==2) {
        const selValue = event.source.value.toLowerCase();
        this.arrFiterRoom.Room = selValue;
        // var arrGroup = hotel.List_RateGroup
        // this._getListRateGroup(arrGroup,selValue);
        // this.Hotels.find(d=>d.ResultIndex == ResultIndex).List_RateGroup = arrGroup;
       
      }
      else if (no==3) {
        const selValue = event.source.value.toLowerCase();
      }
      this.cd.detectChanges();
    }catch(e){ console.log(e.message);}
  
  }

  hendel(error: any) {
    this._response = true;
    this.error = true;
    this.errorStatus = error.status;
    this.cd.detectChanges();
  }

  setHotelResponse(res: any) {
    debugger;
    this.objSearch.TockenID = res.arrResult.TockenID;
    this.hotelService.setHotelSerachParams(this.objSearch);
    this.TockenID = res.arrResult.TockenID;
    this.FilterHotels = res.arrResult.CommonHotelDetails;
    this.Category = res.arrResult.Category;
    this.Facility = res.arrResult.Facility;
    this.MaxPrice = res.arrResult.MaxPrice.toFixed(2);
    this.MinPrice = res.arrResult.MinPrice.toFixed(2);
    this.CountHotel = res.arrResult.CountHotel;
    this.DisplayRequest = res.arrResult.DisplayRequest;
    this.Location = res.arrResult.Location;
    this.Currency = res.arrResult.Currency;
    this.SupplierVisible = res.arrResult.SupplierVisible
    this.setMapData();
  }

  setMapData() {
    this.FilterHotels.forEach(hotel => {
      hotel.Latitude = parseFloat(hotel.Latitude)
      hotel.Langitude = parseFloat(hotel.Langitude)
    });
    this.FilterHotels = (this.FilterHotels || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice < b.Charge.TotalPrice ? -1 : 1)
    this.Hotels = this.FilterHotels;
  }

  setPrice(no: any, list: any) {
    for (let i = 0; i < list.length; i++) {
      var Price = 0;
      for (let j = 0; j < list[i].RoomOccupancy.length; j++) {
        if (list[i].RoomOccupancy[j].Rooms.length)
          Price += (list[i].RoomOccupancy[j].Rooms[0].Total) * (list[i].RoomOccupancy[j].RoomCount);
        for (let r = 0; r < list[i].RoomOccupancy[j].Rooms.length; r++) {
          this._arrRoom = new _arrRoom();
          this._arrRoom._selected = 0;
          this._arrRoom._rooms = [];
          for (let l = 0; l <= this.objSearch.Rooms.length; l++) {
            if (r === 0)
              this._arrRoom._selected += l;
            this._arrRoom._rooms.push();
          }
          list[i].RoomOccupancy[j].Rooms._roomCount = new _arrRoom();
          list[i].RoomOccupancy[j].Rooms._roomCount._selected = this._arrRoom._selected;
          list[i].RoomOccupancy[j].Rooms._roomCount._rooms = this._arrRoom._rooms;
        }
      }
      $("#Price_" + no + "_" + i).text(list[i].RoomOccupancy[0].Rooms[0].RoomRateTypeCurrency + ". " + Price.toFixed(2));
      this.cd.detectChanges();
    }
  }


  setFilterRoom(List_RateGroup:any[],ResultIndex:any){
    try{
        if( this.arrFiterRates == undefined)
        this.arrFiterRates=[];
       this.arrFiterRates.push({ResultIndex  :ResultIndex,  arrRooms :[] , arrMeals: [], List_RateGroup });
       console.log(this.arrFiterRates);
    }catch(e){console.log(e.message)}
  }

  _getListRoom(List_RateGroup:any[]){
    var arrRoom = new Array();
    try{
      List_RateGroup.forEach(gr=>{
          gr.RoomOccupancy.forEach(r=>{
              r.Rooms.forEach(room=>{
                if (arrRoom.filter.length !=0 && arrRoom.filter(d=>d== room.RoomTypeName ).length ==0) {
                  arrRoom.push(room.RoomTypeName)
                }
                else  if (arrRoom.filter.length ==0){
                  arrRoom.push(room.RoomTypeName)
                }
              })
          })
        
      })
    }catch(e){}
    return arrRoom;
  }
  _getListRateGroup(List_RateGroup:any[],value:string){
    try{
       List_RateGroup.forEach(gr=>{
        gr.RoomOccupancy.forEach(r=>{
          r.Rooms.forEach(room=>{
            r.Rooms = r.Rooms.filter(room=> room.RoomTypeName.toLowerCase() == value)
          })
        })
      })
    }catch(e){}
    return List_RateGroup;
  }

  _getListMeals(List_RateGroup:any[]){
    var arrMeals= new Array();
    try{
      List_RateGroup.forEach(gr=>{
          gr.RoomOccupancy.forEach(r=>{
              r.Rooms.forEach(room=>{
                if (arrMeals.filter.length !=0 && arrMeals.filter(d=>d== room.RoomDescription ).length ==0) {
                  arrMeals.push(room.RoomDescription)
                }
                else  if (arrMeals.filter.length ==0){
                  arrMeals.push(room.RoomDescription )
                }
              })
          })
      })
    }catch(e){}
    return arrMeals;
  }

  setRates(ResultIndex: any) {
    this.arrFiterRoom.arrRooms=[];
    this.arrFiterRoom.arrMeals=[];
    this.arrFiterRoom.Meal ="";
    this.arrFiterRoom.Room ="";
    this.arrRateGroup = new SearchRateGroup();
    this.arrRateGroup.TokenId = this.TockenID;
    this.arrRateGroup.ResultIndex = ResultIndex;
    this.arrRateGroup.Checkin = this.objSearch.Checkin;
    this.arrRateGroup.Checkout = this.objSearch.Checkout;
    this.arrRateGroup.nationality = this.objSearch.nationality;
    this.arrRateGroup.SearchType = this.objSearch.SearchType;
    this.arrRateGroup.UserName = this.objSearch.username;
    this.arrRateGroup.Password = this.objSearch.Password;
    this.arrRateGroup.Rooms = this.objSearch.Rooms;
    for (let hotel of this.Hotels) {
      if (hotel.ResultIndex === ResultIndex) {
        hotel.loading = true;
        if (hotel.clicked === false || hotel.clicked === undefined) {
          hotel.List_RateGroup = [];
          this.hotelService.getRategroup(this.arrRateGroup).subscribe((res: any) => {
            console.log(res);
            hotel.clicked = true;
            hotel.loading = false;
            if (res.retCode == 1) {
              if (this.objSearch.Rooms.length > 1) {
                var list = res.arrResult;
                hotel.List_RateGroup = this.updateRateGroup(list);
                let singleHotel = this.FilterHotels.find(sh => sh.ResultIndex === ResultIndex);
                singleHotel.List_RateGroup = list;
              }
              else {
                hotel.List_RateGroup = res.arrResult;
                let singleHotel = this.FilterHotels.find(sh => sh.ResultIndex === ResultIndex);
                singleHotel.List_RateGroup = res.arrResult;
                //if(this.arrFiterRates.filter(d=>d.ResultIndex ==ResultIndex ).length ==0)
               
              }
              this.setFilterRoom(res.arrResult,ResultIndex);
            }
            else
              this.Alert.succsess('Sorry!', 'no rooms available in this hotel', 'warning', function () {
              });
            this.cd.detectChanges();
          });
        }
        else {
          hotel.clicked = false;
          hotel.loading = false;
        }
        break;
      }
    }

  }

  updateRateGroup(list: any) {
    var Rate = [];
    for (let i = 0; i < list.length; i++) {
      var Price = 0;
      for (let j = 0; j < list[i].RoomOccupancy.length; j++) {
        if (list[i].RoomOccupancy[j].Rooms.length)
          Price += (list[i].RoomOccupancy[j].Rooms[0].objCharges.TotalPrice) * (list[i].RoomOccupancy[j].RoomCount);
        for (let r = 0; r < list[i].RoomOccupancy[j].Rooms.length; r++) {
          this._arrRoom = new _arrRoom();
          var select = 0
          this._arrRoom._rooms = [];
          for (let l = 0; l <= list[i].RoomOccupancy[j].RoomCount; l++) {
            if (r === 0) {
              if (l != 0) {
                select++;
              }
            }
            this._arrRoom._rooms.push(l);
          }
          this._arrRoom._selected = select;
          list[i].RoomOccupancy[j].Rooms[r]._roomCount = new _arrRoom();
          list[i].RoomOccupancy[j].Rooms[r]._roomCount._selected = this._arrRoom._selected;
          list[i].RoomOccupancy[j].Rooms[r]._roomCount._rooms = this._arrRoom._rooms;
        }
      }
      list[i]._commonPrice = Price.toFixed(2);
      Rate.push(list[i]);
    }
    return Rate;
  }


  public SetRoomoccupancy(event: any, occupancy: any, room: any, rate: any) {
    var Selected = rate.RoomOccupancy[occupancy].Rooms[room];
    var sRooms = parseInt(rate.RoomOccupancy[occupancy].Rooms[room]._roomCount._selected);
    var otherRooms = rate.RoomOccupancy[occupancy].Rooms;
    var noRooms = rate.RoomOccupancy[occupancy].RoomCount;
    var remainingToBook = noRooms - sRooms;
    for (let r of otherRooms) {
      if (remainingToBook != r._roomCount._selected) {
        if (r._roomCount._selected > 0 && r != Selected) {
          if (r._roomCount._selected - remainingToBook >= 0) {
            r._roomCount._selected = remainingToBook
            remainingToBook = r._roomCount._selected - remainingToBook;
          }
          else {
            remainingToBook = noRooms - 1
            continue
          }
        }
        else if (r != Selected) {
          r._roomCount._selected = 0;
        }
      }
      else {
        if (r._roomCount._selected != 0 && remainingToBook != sRooms) {
          r._roomCount._selected = remainingToBook - 1
        }
        else {
          if (this.PriviousSelected != null && this.PriviousSelected == r) {
            if (remainingToBook != 0) {
              r._roomCount._selected = remainingToBook - 1;
            }

          }
        }
      }
    }
    rate._commonPrice = this.updatePrice(rate.RoomOccupancy);
    this.PriviousSelected = Selected;
  }


  updatePrice(rooms: any) {
    var Price = 0;
    rooms.forEach(occupancy => {
      occupancy.Rooms.forEach(element => {
        Price += (element.objCharges.TotalPrice) * (element._roomCount._selected);
      });
    });

    return Price.toFixed(2);
  }


  getMap(hotel: any) {
    this.hotelService.Singlemap(hotel);
  }

  gethotelMaps(Hotels: any) {
    //this.hotelService.loadHotelmap(Hotels);
  }

  setSearchpars() {
    this.Traveller = [];
    this.objSearch.Rooms.forEach(element => {
      this.Childs = [];
      for (let i = 0; i < element.ChildCount; i++) {
        this.Childs.push({ count: i + 1, childAge: element.ChildAges[i], ages: this.Ages })
      }
      this.Traveller.push({ adults: element.AdultCount, childs: this.Childs });
      this.Adult += element.AdultCount;
      this.Child += this.Childs.length;
    });
    this.objTraveller.Location = this.objSearch.Destination.City + ", " + this.objSearch.Destination.Country;
    this.objTraveller.Date = {
      start: moment(this.objSearch.Checkin, 'DD-MM-YYYY'),
      end: moment(this.objSearch.Checkout, 'DD-MM-YYYY'),
    }
    this.objTraveller.Nationality = this.objSearch.nationality;
  }


  public FilteredHotel(hotel: Hotels[]) {
    debugger;
    this.Hotels = [];
    this.ngxService.start();
    this.Hotels = hotel;
    var Address = this.hotelService.getSelectedlocation();
    if (Address !== null)
      this._nearby = Address.name;
    this.ngxService.stop();
  }

  setOrder(type: string, event: any) {
    debugger
    let Hotels = this.Hotels;
    this.Hotels = [];
    this.ngxService.start();
    if (type === 'Price') {
      if (event.target.value === 'lower') {
        this.Hotels = (Hotels || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice < b.Charge.TotalPrice ? -1 : 1)
      } else if (event.target.value === 'higher') {
        this.Hotels = (Hotels || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice > b.Charge.TotalPrice ? -1 : 1)
      }
    }
    else if (type === 'Category') {
      if (event.target.value === 'lower') {
        this.Hotels = (Hotels || []).sort((a: Hotels, b: Hotels) => a.Category < b.Category ? -1 : 1)
      } else if (event.target.value === 'higher') {
        this.Hotels = (Hotels || []).sort((a: Hotels, b: Hotels) => a.Category > b.Category ? -1 : 1)
      }
    }
    this.ngxService.stop();
  }


  //#region block room

  //#region for single room


  blockRoomParams(data: any, noRooms: any, ResultIndex: any, GroupIndex: any) {
    
    this.ngxService.start();
    this.arrBlock = new arrBlock();
    this.BookingDetails = [];
    this.BookingDetails.push({
      RoomTypeID: data.RoomTypeId, RoomDescription: data.RoomDescriptionId, Total: data.Total.toFixed(2), noRooms: noRooms, AdultCount: 0, ChildCount: 0, ChildAges: "",
    });
    this.arrBlock.BookingDetails = this.BookingDetails;
    this.arrBlock.TokenId = this.TockenID;
    this.arrBlock.CustomerList = [];
    this.arrBlock.ResultIndex = ResultIndex;
    this.arrBlock.GroupIndex = GroupIndex;
    this.arrBlock.Supplier = 0;
    this.arrBlock.Remark = '';
    this.arrBlock.Company = '';
    this.arrBlock.Mobile = '';
    this.arrBlock.Email = ''
    this.blockRoomRequest(this.arrBlock)
  }
  //#endregion

  //#region for multiple room

  blockMultipleRoom(o: any, ResultIndex: any, GroupIndex: any) {
    debugger
    this.ngxService.start();
    this.arrBlock = new arrBlock();
    this.BookingDetails = [];

    o.forEach(occupancy => {
      var room = occupancy.Rooms.filter(r => r._roomCount._selected !== 0);
      if (occupancy.RoomCount == room.length) {
        for (let r = 0; r < occupancy.RoomNo.length; r++) {
          this.BookingDetails.push({
            RoomTypeID: room[r].RoomTypeId, RoomDescription: room[r].RoomDescriptionId, Total: room[r].Total.toFixed(2), noRooms: occupancy.RoomNo[r], AdultCount: 0, ChildCount: 0, ChildAges: "",
          });
        }
      }
      else if (room.length == 1) {
        for (let r = 0; r < occupancy.RoomCount; r++) {
          this.BookingDetails.push({
            RoomTypeID: room[0].RoomTypeId, RoomDescription: room[0].RoomDescriptionId, Total: room[0].Total.toFixed(2), noRooms: r + 1, AdultCount: 0, ChildCount: 0, ChildAges: "",
          });
        }
      }
    });
    this.arrBlock.BookingDetails = this.BookingDetails;
    this.arrBlock.TokenId = this.TockenID;
    this.arrBlock.CustomerList = [];
    this.arrBlock.ResultIndex = ResultIndex;
    this.arrBlock.GroupIndex = GroupIndex;
    this.arrBlock.Supplier = 0;
    this.arrBlock.Remark = '';
    this.arrBlock.Company = '';
    this.arrBlock.Mobile = '';
    this.arrBlock.Email = ''
    this.blockRoomRequest(this.arrBlock)
  }

  //#endregion

  blockRoomRequest(reqData: any) {
    debugger;
    this.hotelService.setarrBlock(reqData);
    console.log(JSON.stringify(reqData));
    this.hotelService._blockRoom(reqData).subscribe((res: any) => {
      this.ngxService.stop();
      if (res.retCode === 1) {
        this.hotelService.setBlockRoomDetails(res);
        this.router.navigate(['/hotel-booking']);
      }
      else {
        this.Alert.succsess('Sorry!', res.ex, 'warning', function () {
        })
      }
    });
  }
  //#endregion

  tokenSession() {
    setInterval(() => {
      this.CheckHoteSession();
    }, 20 * 60000)
  }

  CheckHoteSession() {
    const self = this;
    this.hotelService.CheckHoteSession(this.TockenID).subscribe((res: any) => {
      if (res.retCode === 0) {
        this.Alert.succsess('Oops', res.error, 'warning', function () {
          self._response = false;
          this.Hotels = [];
          self.cd.detectChanges();
          self.objSearch.TockenID = '';
          self.gethotelResponse(self.objSearch);
          //window.location.href = 'hotel';
        });
        setTimeout(() => {
          self._response = false;
          this.Hotels = [];
          self.cd.detectChanges();
          self.objSearch.TockenID = '';
          self.gethotelResponse(self.objSearch);
        }, 10000);
      }
    })
  }

  showHotelDetails(ResultIndex: any, no: any) {
    debugger;
    this.selectedTab = no;
    this.Rindex = ResultIndex;
    this._hotelData = this.Hotels.filter(h => h.ResultIndex === ResultIndex)[0];
    this.hoteldetail = true;
  }

  hideHotelDetails() {
    this.hoteldetail = false;
    this.cd.detectChanges();
  }

}
