import { Component, OnInit } from '@angular/core';
import { Hotels } from 'src/app/modals/hotel/hotel.modal';
import { MatDialog } from '@angular/material';
declare const google: any;
declare function getInfoBox(item): any;
declare function closeInfoBox(): any;

@Component({
  selector: 'app-hotel-map',
  templateUrl: './hotel-map.component.html',
  styleUrls: ['./hotel-map.component.css']
})
export class HotelMapComponent implements OnInit {
  mapObject:any;
  markersData:any;
  markers:[];
  constructor(public dialog: MatDialog,) { }

  ngOnInit() {
  }

    
  /*Map For Hotel*/

  loadHotelmap(hotels: Hotels[])
  {
    debugger;
    try{
       this.GetHotelMapData(hotels);
        
        var mapOptions = {
          zoom: 12,
          center: new google.maps.LatLng(hotels[0].Latitude, hotels[0].Langitude),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
  
          mapTypeControl: false,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
          },
          panControl: false,
          panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          },
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_RIGHT
          },
           scrollwheel: false,
          scaleControl: false,
          scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
          },
          streetViewControl: true,
          streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
          },
        };
        var
        marker;
        var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
        //for (var key in this.markersData)
        this.markersData.forEach(function (item) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
              map: mapObject,
              icon: '../../../assets/img/pins/Hotels.png',
            });
            google.maps.event.addListener(marker, 'click', (function () {
              closeInfoBox();
              getInfoBox(item).open(mapObject, this);
              mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
       }));
    });
    
  }catch(e){
      console.log(e);
    }
   const dialogRef = this.dialog.open(HotelMapComponent);
   dialogRef.afterClosed().subscribe(result => {
   }); 
  }

  GetHotelMapData(hotels: Hotels[])
  {
    try{
      this.markersData = new Array();
      hotels.forEach(hotel => {
        this.markersData.push({
          id:hotel.HotelId,
          name: hotel.HotelName,
          get_directions_start_address:hotel.Address,
          location_latitude: hotel.Latitude, 
          location_longitude: hotel.Langitude ,
          map_image_url: hotel.Image[0].Url,
          name_point: hotel.HotelName,
          description_point:"",// tour.Description,
        });
      });
    }catch(e){}
  }

  Singlemap(hotel:any){
    this.markersData = new Array();
    this.markersData.push({
      id:hotel.HotelId,
      name: hotel.HotelName,
      get_directions_start_address:hotel.Address,
      location_latitude: hotel.Latitude, 
      location_longitude: hotel.Langitude ,
      map_image_url: hotel.Image[0].Url,
      name_point: hotel.HotelName,
      description_point:"",// tour.Description,
    });
        var mapOptions = {
          zoom: 15,
          center: new google.maps.LatLng(hotel.Latitude, hotel.Langitude),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
  
          mapTypeControl: false,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
          },
          panControl: false,
          panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          },
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_RIGHT
          },
           scrollwheel: false,
          scaleControl: false,
          scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
          },
          streetViewControl: true,
          streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
          },
        };
        var
        marker;
        var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
        //for (var key in this.markersData)
        this.markersData.forEach(function (item) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
              map: mapObject,
              icon: '../../../assets/img/pins/Hotels.png',
            });
            google.maps.event.addListener(marker, 'click', (function () {
              closeInfoBox();
              getInfoBox(item).open(mapObject, this);
              mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
       }));
    });
  }
}
