import { Component, OnInit } from '@angular/core';
import { objTraveller, Traveller, Childs } from 'src/app/modals/hotel/search.modal';
import * as moment from 'moment';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
  Traveller: Traveller[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];
  AdminCode: any;

  constructor(public apiUrlService: ApiUrlService) {
    this.AdminCode = apiUrlService.Code;
  }

  ngOnInit() {
    debugger;
    this.Traveller = [];
    this.Childs = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objTraveller = new objTraveller();
    this.objTraveller.Location = '';
    this.objTraveller.Guests = "";
    var start = new Date();
    var end = new Date();
    end.setDate(end.getDate() + 1);
    this.objTraveller.Date = {
      start: moment(start),
      end: moment(end),
    }
    this.Traveller.push({ adults: 1, childs: this.Childs });
  }

}
