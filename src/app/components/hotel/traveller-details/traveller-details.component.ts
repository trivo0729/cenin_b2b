import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { arrConfirm } from 'src/app/modals/hotel/booking.modal';
import { loginDetails, User } from 'src/app/modals/user.model';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
declare var Razorpay: any;

@Component({
  selector: 'app-traveller-details',
  templateUrl: './traveller-details.component.html',
  styleUrls: ['./traveller-details.component.css']
})
export class TravellerDetailsComponent implements OnInit {

  @Input() CustumerDetail: arrConfirm;
  @Input() Reservationdata: any;
  @Input() usePaymentGetway: boolean;
  @Output() editCustomer = new EventEmitter();
  @Output() Pay = new EventEmitter();
  show: boolean;
  login: loginDetails = new loginDetails();
  User: User = new User();
  rzp1: any;
  options: any;
  CompanyName: string;
  constructor(public userService: UserService,
    private cookie: CommonCookieService,
    private apiUrl: ApiUrlService) {
    this.CompanyName = apiUrl.CompanyName;
  }

  ngOnInit() {
    console.log(this.CustumerDetail);
  }

  updateStatus() {
    debugger;
    this.show = false;
    this.editCustomer.emit(this.show)
  }

  RazorPay() {
    debugger
    if (this.getUser() === "B2C" || this.getUser() === "") {
      this.RazorPayModel();
    }
    else if (this.getUser() === "B2B") {
      if (this.usePaymentGetway) {
        this.RazorPayModel();
      }
      if (!this.usePaymentGetway) {
        this.Book("", "");
      }
    }
  }

  RazorPayModel() {
    const _self = this;
    var total = this.Reservationdata.objCharge.TotalPrice;
    total = (parseFloat(total) * 100).toFixed(2).toString();
    console.log(total);
    this.options = {
      "key": this.apiUrl.RazorpayKey,
      "currency": this.Reservationdata.Currency,
      "amount": total, // 2000 paise = INR 20
      "name": this.CompanyName,
      "description": "Hotel Booking",
      "image": this.apiUrl.logo,
      "handler": function (response) {
        _self.Book(response.razorpay_payment_id, 'Razorpay');
      },
      // "prefill": {
      //   "name": this.User.Name,
      //   "email": this.User.Email,
      //   "mobile": this.User.ContacDetails.Mobile,
      // },
      // "notes": {
      //   "address": this.User.ContacDetails.Address
      // },
      "theme": {
        "color": "#FF8000"
      }
    };
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }

  Book(payment_id: string, PayementGetWay: string) {
    debugger
    var payment = {
      payment_id, PayementGetWay
    }
    this.Pay.emit(payment)
  }

  getUser() {
    let userType = "";
    if (this.cookie.checkcookie('login')) {
      let userdata = JSON.parse(this.cookie.getcookie('login'));
      userType = userdata.LoginDetail.UserType
    }
    return userType;
  }

}
