import { Component, OnInit, Input } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { Observable } from 'rxjs';
import { TourTypes, PackageCities, objTour } from 'src/app/modals/tours.modals';
import { FormControl } from '@angular/forms';
import { login } from 'src/app/modals/user.model';
declare function allScript(): any;
import * as moment from 'moment';
import { Traveller, objTraveller, Childs } from 'src/app/modals/hotel/search.modal';
import { MenuService } from 'src/app/services/commons/menu.service';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { UserService } from 'src/app/services/user.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  logedin: boolean;
  objTour: objTour;
  UsetId: number;
  arrPopulars: TourTypes[];
  PackageCity: PackageCities[];
  filterCities: Observable<PackageCities[]>;
  cityCtrl = new FormControl();
  Traveller: Traveller[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];
  menu: Menulist[];
  _homeTabs: Menulist[];
  AdminCode: any;
  email: string;
  constructor(
    private router: ActivatedRoute,
    private tourService: ToursService,
    private Alert: AlertService,
    private userService: UserService,
    private menuService: MenuService,
    public objGlobalService: ApiUrlService,
    private cookie: CommonCookieService
  ) { }
  ngOnInit() {
    debugger;
    this.logedin = false;
    this.AdminCode = this.objGlobalService.Code;
    this.objTour = new objTour();
    this.objTour.Location = '';
    this.objTour.LocationName = "";
    this.objTour.TourType = "";
    var date = new Date();
    this.objTour.Date = moment(date, 'DD-MM-YYYY');
    this.objTour.TourType = '';
    this.tourService.getPopularTours().subscribe((res: any) => {
      this.arrPopulars = res.arrTourTypes;
    });
    /* set hotel travel data */
    this.Hoteltravel();
    this.getTabs();
    if (this.AdminCode === "CUT" || this.AdminCode === "TRIVO" || this.AdminCode === "CENNIN") {
      if (this.cookie.getcookie('login')) {
        let data = JSON.parse(this.cookie.getcookie('login'))
        if (data.LoginDetail.UserType === "B2B") {
          this.logedin = true;
        }
      }
    }
    this.email = this.router.snapshot.queryParamMap.get('email');
    if (this.email !== null)
      this.verify(this.email);
  }



  Hoteltravel() {
    this.Traveller = [];
    this.Childs = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objTraveller = new objTraveller();
    this.objTraveller.Location = '';
    var start = new Date();
    var end = new Date();
    end.setDate(end.getDate() + 1);
    this.objTraveller.Date = {
      start: moment(start),
      end: moment(end),
    }
    this.Traveller.push({ adults: 1, childs: this.Childs });
  }

  getTabs() {
    this._homeTabs = [];
    this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
      this.menu = res;
      this.generateTabs();
    }, error => {
      console.log(error);
    });
  }

  generateTabs() {
    this.menu.forEach(menu => {
      if (menu.Name === 'Hotel' || menu.Name === 'Sightseeing' || menu.Name === 'Package')
        this._homeTabs.push(menu);
    });
  }

  getUserId() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.objGlobalService.UserId;
        }
      }
    }
    else {
      this.UsetId = this.objGlobalService.UserId;
    }
    return this.UsetId;
  }

  SignIn() {
    login();
  }

  verify(email: string) {
    this.userService.verifyEmail(email).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.Alert.confirm('Email Verification has been done. <br> Email containing username and password sent on your registered mail', 'success');
        setTimeout(() => {
          window.location.href = "/home";
        }, 4500);
      }
      if (res.retCode !== 1) {
        this.Alert.confirm('Your verification request submitted will check and get back to you', 'warning');
        setTimeout(() => {
          window.location.href = "/home";
        }, 4500);
      }
    }, error => {
      console.log(error);
    })
  }
}
