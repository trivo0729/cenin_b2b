import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { loginDetails } from 'src/app/modals/user.model';
import { UserService } from 'src/app/services/user.service';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Router } from '@angular/router';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { AccountService } from 'src/app/services/agent/account.service';
import { arrBalance } from '../../trivo/trivo-header/trivo-header.component';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { MenuService } from 'src/app/services/commons/menu.service';
declare function allScript(): any;
@Component({
  selector: 'app-cennin-header',
  templateUrl: './cennin-header.component.html',
  styleUrls: ['./cennin-header.component.css']
})
export class CenninHeaderComponent implements OnInit {
  @Output() loginDialog = new EventEmitter();
  @Output() RegtrDialog = new EventEmitter();
  @Output() _logout = new EventEmitter();
  @Input() login: any;
  CustomerName: string = null;
  CustomerEmail: string = null;
  arrBalance: arrBalance;
  menu: Menulist[];
  _mainMenu: Menulist[];
  _userMenu: Menulist[];
  UsetId: number;
  logo: string;
  loggedin: boolean;
  constructor(
    private menuService: MenuService,
    private userService: UserService,
    public objGlobal: ApiUrlService,
    private router: Router,
    private cookie: CommonCookieService,
    private accountService: AccountService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.login = this.login;
    this.loggedin = false;
    debugger;
    this.arrBalance = new arrBalance();
    this.login = this.login
    if (this.cookie.checkcookie('login')) {
      debugger
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.loggedin = true;
        this.getlogo(data.LoginDetail.sid);
        this.getManu();
        this.GetBalanceInformation(data.LoginDetail.sid)
      }
    }
  }

  getlogo(id: any) {
    this.userService.AgentInfo(id).subscribe((res: any) => {
      console.log(res);
      if (res.retCode === 1) {
        this.logo = res.arrUser.CompanyDetails.LogoLink;
        this.cd.detectChanges();
      }
    })
  }

  onSigninDialog() {
    this.loginDialog.emit();
  }

  onRegisterDialog() {
    this.RegtrDialog.emit();
  }

  onSignout() {
    this._logout.emit();
  }

  RouterLink(Type: string) {
    debugger;
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url;

    if (currentUrl === '/package-list') {
      this.router.navigateByUrl(currentUrl)
        .then(() => {
          this.router.navigated = false;
          this.router.navigate([this.router.url], { queryParams: { Type: Type } });
        });
    }
    else
      this.router.navigate(['/package-list'], { queryParams: { Type: Type } });




  }

  GetBalanceInformation(uid: number) {
    this.accountService.GetBalanceInformation(uid).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.arrBalance = res.arrBalance;
      }
      console.log(res);
    })
  }

  getManu() {
    debugger;
    this.menu = [];
    if (this.cookie.checkcookie('menu')) {
      let data = JSON.parse(this.cookie.getcookie('menu'));
      if (data.UserId === this.getUserId()) {
        this.menu = data.Menu;
        this.generateMenu();
      }
      if (data.UserId !== this.getUserId()) {
        this.cookie.deletecookie('menu');
      }
    }
    if (!(this.cookie.checkcookie('menu')) || this.menu.length == 0) {
      this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
        this.menu = res;
        const menudetails = { UserId: this.getUserId(), Menu: this.menu }
        this.cookie.setcookie('menu', JSON.stringify(menudetails), 1);
        this.generateMenu();
      }, error => {
        console.log(error);
      });
    }
  }

  getUserId() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.UsetId = data.LoginDetail.sid;
      }
    } else {
      this.UsetId = this.objGlobal.UserId;
    }
    return this.UsetId;
  }

  generateMenu() {
    this._mainMenu = [];
    this._userMenu = []
    this.menu.forEach(menu => {
      if (menu.Name === 'Home' || menu.Name === 'Hotel' || menu.Name === 'Flights' || menu.Name === 'Sightseeing' || menu.Name === 'Visa' || menu.Name === 'OTB' || menu.Name === 'Package')
        this._mainMenu.push(menu);
      if (menu.Name === 'Login')
        this._userMenu.push(menu)
    });
  }

}
