import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Contact } from 'src/app/modals/contact.modal';
import { ContactService } from 'src/app/services/contact.service';
declare function setimage(img:string):any;
declare function singleBanner(): any;
declare function MapDesign(lat: any, lng: any, logo: string, address: string): any;
@Component({
  selector: 'app-cennin-contact',
  templateUrl: './cennin-contact.component.html',
  styleUrls: ['./cennin-contact.component.css']
})
export class CenninContactComponent implements OnInit {
  contactfrm: FormGroup;
  submitted = false;
  contact: Contact;
  AdminCode:any;
  img: any;
  constructor(private formBuilder: FormBuilder,
              private ContactService : ContactService,
              public objGlobalService :ApiUrlService
             ) { }

  ngAfterViewInit(){
    debugger
    singleBanner();
    setimage(this.img)
  }           

  ngOnInit() {
    MapDesign(this.objGlobalService.lat, this.objGlobalService.lng, this.objGlobalService.logo, this.objGlobalService.address);
   this.img ='../assets/img/header_bg.jpg' ;
    this.AdminCode = this.objGlobalService.Code;
    this.contact= new Contact();
    this.contact.AdminID = this.objGlobalService.AdminID;
    this.contactfrm = this.formBuilder.group({
      Firstname: ['', Validators.required],
      Lastname: ['', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      Phone: ['', Validators.required],
      Message: ['', [Validators.required]],
  },);
  }

  get f() { return this.contactfrm.controls; }

  public CheckValidation():void {
    debugger
    this.submitted = true;
    // stop here if form is invalid
    if (this.contactfrm.invalid) {
        return;
    }
    else{
       this.SendContactinfo();
    }
  }
  SendContactinfo(){
    debugger;
    this.ContactService.sendContactinfo(this.contact).subscribe((res:any) => {
       if(res.retCode==1){
        alert("Your request sent.We will get in touch with you soon.");
        this.contact.Firstname="";
        this.contact.Lastname="";
        this.contact.Email="";
        this.contact.Phone="";
        this.contact.Message="";
       }
       else{
        alert("request Not sent");
       }
    })  
  }

}
