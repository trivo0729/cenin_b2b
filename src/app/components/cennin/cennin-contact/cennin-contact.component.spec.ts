import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CenninContactComponent } from './cennin-contact.component';

describe('CenninContactComponent', () => {
  let component: CenninContactComponent;
  let fixture: ComponentFixture<CenninContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CenninContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CenninContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
