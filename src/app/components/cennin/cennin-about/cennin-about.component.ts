import { Component, OnInit } from '@angular/core';
declare function setimage(img:string):any;
declare function singleBanner(): any;
@Component({
  selector: 'app-cennin-about',
  templateUrl: './cennin-about.component.html',
  styleUrls: ['./cennin-about.component.css']
})
export class CenninAboutComponent implements OnInit {
  img:any;
  constructor() { }
  ngAfterViewInit(){
    debugger
    singleBanner();
    setimage(this.img)
  }     
  ngOnInit() {
    this.img ='../assets/img/cennin/about-us.jpg' ;
  }

}
