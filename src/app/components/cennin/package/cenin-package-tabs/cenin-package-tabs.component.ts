import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PackageService } from 'src/app/services/packages/package.service';
import { UserService } from 'src/app/services/user.service';
import { Package } from 'src/app/modals/packages/package.modal';
declare function allTabs(): any;
@Component({
  selector: 'app-cenin-package-tabs',
  templateUrl: './cenin-package-tabs.component.html',
  styleUrls: ['./cenin-package-tabs.component.css']
})
export class CeninPackageTabsComponent implements OnInit {
  theme: string = "International";
  Packages: Package[];
  count = 0;
  bloder = true;
  minimum: Array<number>;
  arrlocations: string[];
  constructor(
    public packageService: PackageService,
    public userService: UserService,
    private cd: ChangeDetectorRef,
  ) { }
  Themes: any[] = [{ Name: "International Destinations", color: "#0072bc", type: "International", url: 'International', icon: "icon-flight" },
  { Name: "Popular Destinations In India", color: "#17684f", type: "Domestic", url: 'Domestic', icon: "icon-globe-1" },
  { Name: "Honeymoon Destinations", color: "pink", type: "Honeymoon", url: '', icon: "icon_set_2_icon-108" },
  {
    Name: "Wildlife In India", color: "#82ca9c", type: 'Wildlife', url: '', icon: "icon_set_1_icon-30", tabs: [
      { Name: "Central India", color: "#82ca9c", type: 'Domestic', url: '' },
      { Name: "Others", color: "#82ca9c", type: 'International', url: '' }
    ]
  },
  { Name: "Offbeat Destinations", color: "#575356", type: 'Offbeat', url: '', icon: "icon_set_1_icon-24" },
  { Name: "Groups", color: "#f7941d", type: 'All', url: '', icon: "icon_set_1_icon-3" }
  ];
  ngOnInit() {
    this.Packages = [];
    this.showTabs("International");
    setTimeout(() => {
      try {
        allTabs();
      } catch (e) { }
    }, 1000);
  }

  setTab(event) {
    debugger;
    var Index = event.index - 1
    this.showTabs(this.Themes[Index].Name);
  }

  showTabs(Type: string) {
    debugger;
    this.bloder = true;
    this.Packages = [];
    this.count = 0;
    if (Type == "International" || Type == "Domestic") {
      this.packageService.SearchPackagesByType(this.userService.getUserdata(), Type).subscribe((res: Package[]) => {
        this.Packages = res;
        this.bloder = false;
        
        this.SetMinimumPrice();
        this.setLocations();
        var arrData =  this.Packages;
        this.Packages =[];
        arrData.forEach(p=>{
            if( Type == "International"){
              var list = p.Themes.filter(d=>d =="Offbeat");
              if(list.length ==0)
                this.Packages.push(p);
            }
            else 
            {
              var list = p.Themes.filter(d=>d =="Honeymoon" || d == "Wildlife" );
              if(list.length ==0)
                this.Packages.push(p);
            }
            
        });
        this.count = this.Packages.length;
        this.Packages = this.Packages.slice(0, 9)
        this.cd.detectChanges();
        console.log(this.Packages);
      });
    }
    else if (Type === "Offbeat") {
      this.packageService.getPackage(this.userService.getUserdata()).subscribe((res: Package[]) => {

        this.Packages = [];
        /* Theme */
        res.forEach(arrPackage => {
          if (arrPackage.Themes.filter(d => d == Type).length != 0) {
            if (this.Packages.indexOf(arrPackage) == -1)
              this.Packages.push(arrPackage)
          }
        });
        this.bloder = false;
        this.count = this.Packages.length;
        this.SetMinimumPrice();
        this.setLocations();
        this.Packages = this.Packages.slice(0, 9)
        this.cd.detectChanges();
      });
    }
    else {
      this.packageService.getPackage(this.userService.getUserdata()).subscribe((res: Package[]) => {

        this.Packages = [];
        /* Theme */
        res.forEach(arrPackage => {
          if (arrPackage.Themes.filter(d => d == Type && Type != "All").length != 0) {
            if (this.Packages.indexOf(arrPackage) == -1)
              this.Packages.push(arrPackage)
          }
          else if (arrPackage.Themes.filter(d => d != "WildLife" || d != "Honeymoon").length != 0) {
            if (this.Packages.indexOf(arrPackage) == -1)
              this.Packages.push(arrPackage)
          }

        });
        this.bloder = false;
        this.count = this.Packages.length;
        this.SetMinimumPrice();
        this.setLocations();
        console.log(this.Packages);
        this.Packages = this.Packages.slice(0, 9)
        this.cd.detectChanges();
      });
    }
  }

  SetMinimumPrice() {
    try {
      debugger;
      this.Packages.forEach(arrPackage => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (arrPackage.Category[0].Single == 0 && arrPackage.Category[0].Double == 0 && arrPackage.Category[0].Triple == 0 && arrPackage.Category[0].Quad == 0 && arrPackage.Category[0].Quint == 0) {
            this.minimum.push(0);
          }
          if(this.minimum != undefined &&  this.minimum.length !=0)
             arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
        }
        else {
          arrPackage.MinPrice = 0;
        }

        //this.min = 
        this.Packages.push(arrPackage);
      });
      this.Packages = this.Packages;
    } catch (e) { console.log(e.message); this.Packages = this.Packages }

    //this.min = this.PackagePrice.reduce((a, b) => Math.min(a, b));
    //.minimum = this.min;
  }

  setLocations() {
    try {

      this.arrlocations = [];
      console.log(this.Packages.length);
      this.Packages.forEach(arrPackage => {
        arrPackage.Locations = [];
        arrPackage.Locations = arrPackage.City.split('|');
        arrPackage.Locations.forEach(location => {
          if (!this.arrlocations.includes(location))
            this.arrlocations.push(location);
        });
      });
      var arrData = this.Packages;
      this.Packages = [];
      this.arrlocations.forEach(r => {
        var arr = arrData.filter(d => d.Locations.includes(r));
        if (arr.length != 0)
          if (this.Packages.indexOf(arr[0]) == -1)
            this.Packages.push(arr[0])
      });
      console.log(this.Packages.length);
    } catch (e) { console.log(e.message); this.Packages = this.Packages }
  }

}

