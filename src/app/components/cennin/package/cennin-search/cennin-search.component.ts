import { Component, OnInit } from '@angular/core';
import { PackageCities, PackageNames } from 'src/app/modals/tours.modals';
import { FormControl } from '@angular/forms';
import { PackageService } from 'src/app/services/packages/package.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Package } from 'src/app/modals/packages/package.modal';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-cennin-search',
  templateUrl: './cennin-search.component.html',
  styleUrls: ['./cennin-search.component.css']
})
export class CenninSearchComponent implements OnInit {
  Package: Package[];
  PackageID: any;
  PackageCity: PackageCities[];
  filterCities: Observable<PackageCities[]>;
  cityCtrl = new FormControl();
  PackageName: PackageNames[];
  arrPackagname: string[];
  arrpackageID: any[];
  filterpackageName: Observable<PackageNames[]>;
  NameCtrl = new FormControl();
  IDCtrl = new FormControl();
  constructor(private router: Router,
    private packageService: PackageService,
    public alert: AlertService,
    private userService: UserService

  ) { }

  ngOnInit() {
    this.Package = [];
    this.packageService.getPackageLocation().subscribe((res: PackageCities[]) => {
      debugger;
      this.PackageCity = res;
      this.filterCities = this.cityCtrl.valueChanges
        .pipe(
          startWith(''),
          map(CityName => CityName ? this._filterCities(CityName) : this.PackageCity.slice())
        );
    });

    this.packageService.getPackage(this.userService.getUserdata()).subscribe((res: Package[]) => {
      debugger;
      this.Package = res;
      this.getpackageName(this.Package);
      this.filterpackageName = this.NameCtrl.valueChanges
        .pipe(
          startWith(''),
          map(PackageName => PackageName ? this._filterName(PackageName) : this.PackageName.slice())
        );

    });


  }

  getpackageName(Packages: any[]) {
    this.arrPackagname = [];
    this.arrpackageID = [];
    this.PackageName = [];
    this.Package.forEach(arrPackage => {
      // this.arrPackagname.push( arrPackage.PackageName); 
      // this.arrpackageID.push(arrPackage.Packageid) ;
      this.PackageName.push({ PackageName: arrPackage.PackageName, PackageID: arrPackage.Packageid });
      // arrPackage.PackageName.toString();
    });

    console.log(this.PackageName);
  }

  private _filterName(value: string): PackageNames[] {
    debugger;
    const filterValue = value.toLowerCase();
    return this.PackageName.filter(PackageName => PackageName.PackageName.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterCities(value: string): PackageCities[] {
    const filterValue = value.toLowerCase();
    return this.PackageCity.filter(CityName => CityName.City.toLowerCase().indexOf(filterValue) === 0);
  }

  OnPackageSelect(value: any) {
    debugger;
    this.PackageID = value.PackageID;

  }

  getPackages(City: string) {
    debugger;
    if (City != "") {
      this.router.navigate(['/package-list'], { queryParams: { City: City } });
    }
    else {
      debugger;
      this.alert.confirm('Please Enter City Name', 'warning')
    }
  }

  getpackagebyname(Name: string) {
    debugger;
    if (Name != "") {
      this.router.navigate(['/package-list'], { queryParams: { PackageID: this.PackageID } });
    }
    else {
      debugger;
      this.alert.confirm('Please Enter Package Name', 'warning')
    }


  }
}
