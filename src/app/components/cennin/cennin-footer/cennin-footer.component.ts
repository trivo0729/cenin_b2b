import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-cennin-footer',
  templateUrl: './cennin-footer.component.html',
  styleUrls: ['./cennin-footer.component.css']
})
export class CenninFooterComponent implements OnInit {

  constructor(
    public objGlobal : ApiUrlService,
  ) { }

  ngOnInit() {
  }
  
  subscribe()
  {
    window.location.href="https://cenintravels.us20.list-manage.com/subscribe/post?u=6af0279a20b944d62b53a22ab&id=5a95510f4f"
  }
}
