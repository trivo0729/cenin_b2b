import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  AdminCode:string="";
  constructor(
    public objGlobal: ApiUrlService
  ) { }
  ngOnInit() {
    this.AdminCode = this.objGlobal.Code; 
  }
}
