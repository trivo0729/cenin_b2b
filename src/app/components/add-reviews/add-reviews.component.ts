import { Component, OnInit, Input , Inject } from '@angular/core';
import { Review } from 'src/app/modals/reviews/review.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material'
import { ReviewService } from 'src/app/services/review.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-reviews',
  templateUrl: './add-reviews.component.html',
  styleUrls: ['./add-reviews.component.css']
})
export class AddReviewsComponent implements OnInit {
  ProductType: string ;
  ProdcutID: Number;
  
  arrReview:Review = new Review();
  constructor( 
    private reviewservice:ReviewService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddReviewsComponent>,
   
  ) { }
  ngOnInit() {
    debugger;
    console.log(this.data)
      this.arrReview.ProdctType = this.data.ProductType.toString();
      this.arrReview.ProductID = this.data.ProdcutID.toString();
    
    //console.log(this.arrReview)
  }
 SaveReview()
 {
   debugger; 
  //  console.log( this.ProductType);
  this.reviewservice.SaveReviews(this.arrReview).subscribe((result: any) => {
   
    if(result.retCode)
    {
      this.toastr.success("your review !!","Thanks for submitting")
      
    }
    else
    {
      this.toastr.success(result.Message)
    }
    this.dialogRef.close(this.ProdcutID);
  });
 }
}
