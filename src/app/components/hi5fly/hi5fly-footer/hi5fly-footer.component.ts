import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-hi5fly-footer',
  templateUrl: './hi5fly-footer.component.html',
  styleUrls: ['./hi5fly-footer.component.css']
})
export class Hi5flyFooterComponent implements OnInit {

  constructor(public objGlobal : ApiUrlService) 
  {

   }

  ngOnInit() {
  }

}
