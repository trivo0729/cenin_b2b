import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Hi5flyHeaderComponent } from './hi5fly-header.component';

describe('Hi5flyHeaderComponent', () => {
  let component: Hi5flyHeaderComponent;
  let fixture: ComponentFixture<Hi5flyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Hi5flyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Hi5flyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
