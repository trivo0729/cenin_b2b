import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Destinations, objSearch } from 'src/app/modals/flight/flight-search.modal';


@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  JourneyType: string;
  Class: string;
  JourneyDate: any;
  singleDatePicker: boolean;
  Destinations: Destinations[];
  Search: objSearch;
  TotelGuest: number;
  Direct: boolean;

  constructor() { }

  ngOnInit() {
    this.Destinations = [];
    this.JourneyType = "1";
    this.Class = "All";
    this.singleDatePicker = true;
    var start = new Date();
    this.JourneyDate = moment(start);
    this.Destinations.push(
      {
        Origin: '',
        OriginCode: '',
        Destination: '',
        DestinationCode: '',
        JourneyDate: this.JourneyDate,
        MinDate: moment(start),
        OriginAirports: [],
        DestinationAirports: []
      }
    );
    this.Search = new objSearch();
    this.Search.AdultCount = 1;
    this.Search.ChildCount = 0;
    this.Search.InfantCount = 0;
    this.TotelGuest = 1;
    this.Direct = false;
  }

}


