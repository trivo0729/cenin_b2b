import { Component, OnInit, ViewChild } from '@angular/core';
import { FlightService } from 'src/app/services/flight/flight.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Router } from '@angular/router';
//import { FlightBookingDetailsComponent } from '../../user/booking-details/flight-booking-details/flight-booking-details.component';
declare function StickySidebar(): any;
@Component({
  selector: 'app-flight-confirm',
  templateUrl: './flight-confirm.component.html',
  styleUrls: ['./flight-confirm.component.css']
})
export class FlightConfirmComponent implements OnInit {
  userId: any;
  Details: ArrDetails[];
  //@ViewChild(FlightBookingDetailsComponent) bDetails: FlightBookingDetailsComponent;
  constructor(private flightService: FlightService,
    private commonobj: ApiUrlService,
    private router: Router,
    private cookie: CommonCookieService
  ) { }

  ngOnInit() {
    this.Details = [];
    StickySidebar();
    //this.getConfirmationDetails();
    this.userId = this.GetLoginDetails();
    //this.bDetails.getAirlines(this.userId, this.Details[0].BookingID);
  }


  GetLoginDetails() {
    if (this.cookie.checkcookie('login')) {
      let login = JSON.parse(this.cookie.getcookie('login'));
      if (login.LoginDetail.UserType === "B2B") {
        return login.LoginDetail.sid
      }
      else {
        return this.commonobj.UserId
      }
    }
    else {
      this.router.navigate['/home'];
    }
  }

  ngAfterViewInit() {
    let position: HTMLElement = document.getElementById('position') as HTMLElement;
    position.style.display = 'none';

    let mainbooking: HTMLElement = document.getElementById('main-booking') as HTMLElement;
    mainbooking.style.paddingRight = '0px';
    mainbooking.style.paddingLeft = '0px';
  }

  // getConfirmationDetails() {
  //   debugger
  //   let Data = this.flightService.getFligthBookingParams();
  //   this.Details = Data.PNR
  // }

}

export class ArrDetails {
  BookingID: any;
  ErrorStatus: any;
  OnHold: any;
  PNR: any;
  Success: any;
  TicketID: any
}
