import { Component, OnInit, Input } from '@angular/core';
import { ArrFilter } from 'src/app/modals/flight/flight-response.modal';

@Component({
  selector: 'app-flight-filter',
  templateUrl: './flight-filter.component.html',
  styleUrls: ['./flight-filter.component.css']
})
export class FlightFilterComponent implements OnInit {
  @Input() Filter: ArrFilter;
  @Input() JourneyType: any;
  @Input() IsDomestic: boolean;
  @Input() Flightlength : number;
  constructor() { }

  ngOnInit() {
    this.Filter.arrOutBounds = [];
    this.Filter.arrInbounds = [];
    console.log(this.Filter)
  }

}
