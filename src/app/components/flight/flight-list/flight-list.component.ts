import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { objSearch, Destinations, Seat } from 'src/app/modals/flight/flight-search.modal';
import { Flights, DomesticRoundTrip, ArrFilter } from 'src/app/modals/flight/flight-response.modal';
import { FlightService } from 'src/app/services/flight/flight.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ConfirmComponent } from '../../common/confirm/confirm.component';


@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.css']
})
export class FlightListComponent implements OnInit {

  Seats: Seat[] = [{ name: 'All', id: '1' }, { name: 'Economy', id: '2' }, { name: 'Premium Economy', id: '3' }, { name: 'Business', id: '4' }, { name: 'Premium Business', id: '5' }, { name: 'First', id: '6' }];

  // const for bottom 
  JourneyType: string;
  Class: string;
  JourneyDate: any;
  singleDatePicker: boolean;
  DestinationCode: any;

  OriginCode: any;
  Destinations: Destinations[];
  TotelGuest: number;


  Onward = {
    slected: false,
    index: 0
  }

  total: any;

  Return = {
    slected: false,
    index: 0
  }
  // const for end 
  Direct: boolean;
  closeResult: string;
  objSearch: objSearch;
  flights: Flights[];
  IsDomestic: boolean;
  RoundTrip: DomesticRoundTrip;
  //origin : any;
  error: boolean = false;
  errorStatus: any;
  destination: any;
  TokenId: any;
  arrRules: [];
  loading: boolean;
  Filter: ArrFilter;
  image: '';
  flightlength: number;
  _response: boolean;
  constructor(private flightService: FlightService,
    private ngxService: NgxUiLoaderService,
    private router: Router,
    private modalService: NgbModal,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    debugger;

    // dropPanel();
    this.objSearch = new objSearch();
    this._response = false;
    this.objSearch = this.flightService.getFlightSerachParams();
    console.log(this.objSearch);
    this.Destinations = [];
    this.RoundTrip = new DomesticRoundTrip();
    this.RoundTrip.Inbound = [];
    this.RoundTrip.Outbound = [];

    this.IsDomestic = false;
    this.flights = [];
    this.Filter = new ArrFilter();
    // For Modify Search 
    this.getflightsearchpara(this.objSearch);

    // For Flight Search Response 
    this.getFlightResponse(this.objSearch);
  }


  // ngAfterViewInit() {
  //   let chk: any = document.getElementsByClassName('ltr');
  //   chk[0].style.left = '-320px';
  //   dropPanel();
  // }

  getflightsearchpara(objSearch: any) {
    this.JourneyType = this.objSearch.JourneyType;
    this.Class = this.Seats.find(s => s.id === this.objSearch.Segments[0].FlightCabinClass).name;

    if (this.objSearch.DirectFlight === "true")
      this.Direct = true
    else
      this.Direct = false

    this.objSearch.Segments.forEach((segment, i) => {
      var startDate = moment(segment.PreferredDepartureTime, 'DD-MM-YYYY');

      if (this.JourneyType === "2") {
        this.singleDatePicker = false;
        let length = this.objSearch.Segments.length - 1;
        var endDate = moment(this.objSearch.Segments[length].PreferredDepartureTime, 'DD-MM-YYYY');
        setTimeout(() => {
          let chk: any = document.getElementsByClassName('ltr');
          chk[0].style.left = '-320px';
        }, 50);
        if (i == 0) {
          this.Destinations.push(
            {
              Origin: segment.OriginCity,
              OriginCode: segment.Origin,
              Destination: segment.DestinationCity,
              DestinationCode: segment.Destination,
              JourneyDate: {
                start: startDate,
                end: endDate,
              },
              MinDate: startDate,
              OriginAirports: [],
              DestinationAirports: []
            }
          );
        }

      }
      else if (this.JourneyType === "3") {
        this.singleDatePicker = true;
        let i = this.Destinations.length - 1;
        this.Destinations.push(
          {
            Origin: segment.OriginCity,
            OriginCode: segment.Origin,
            Destination: segment.DestinationCity,
            DestinationCode: segment.Destination,
            JourneyDate:
            {
              start: moment(startDate),
              end: moment(segment.PreferredArrivalTime, 'DD-MM-YYYY'),
            },
            MinDate: moment(segment.PreferredDepartureTime, 'DD-MM-YYYY'),
            OriginAirports: [],
            DestinationAirports: []

          }
        );

      }
      else {
        var endDate = moment(segment.PreferredArrivalTime, 'DD-MM-YYYY');
        this.singleDatePicker = true;
        this.Destinations.push(
          {
            Origin: segment.OriginCity,
            OriginCode: segment.Origin,
            Destination: segment.DestinationCity,
            DestinationCode: segment.Destination,
            JourneyDate:
            {
              start: moment(startDate),
              end: moment(segment.PreferredArrivalTime, 'DD-MM-YYYY'),
            },
            MinDate: startDate,
            OriginAirports: [],
            DestinationAirports: []
          });
      }
    });

    this.objSearch.AdultCount = this.objSearch.AdultCount;
    this.objSearch.ChildCount = this.objSearch.ChildCount;
    this.objSearch.InfantCount = this.objSearch.InfantCount;
    this.TotelGuest = this.objSearch.AdultCount + this.objSearch.ChildCount + this.objSearch.InfantCount;
    console.log(JSON.stringify(this.objSearch));
    this.cd.detectChanges();
  }

  getFlightResponse(ObjSearch: any) {
    debugger;

    this.ngxService.start();
    this.flightService.searchFlight(ObjSearch).subscribe((res: any) => {
      this.ngxService.stop();
      this._response = true;
      if (res.retCode === 1) {
        this.IsDomestic = res.arrResult.IsDomestic;
        this.TokenId = res.arrResult.objSearch.TokenID;
        this.objSearch.TokenId = res.arrResult.TockenID;
        this.flightService.SetFligthSearchParam(this.objSearch);
        this.Filter = res.arrResult.arrFilter;
        this.setflightResponse(res);
      }
      this.cd.detectChanges();
    }, error => {
      this.hendel(error);
    });
  }

  hendel(error: any) {
    this._response = true;
    this.error = true;
    this.errorStatus = error.status;
    this.cd.detectChanges();
  }

  setflightResponse(res: any) {
    debugger;
    this._response = true;
    res.arrResult.Flights.forEach((flights, i) => {
      flights.forEach(flight => {
        flight.Segments.forEach((segments) => {
          segments.forEach(segment => {
            var minutes = segment.Duration % 60
            var hours = (segment.Duration - minutes) / 60
            segment.Duration = `${hours}h ${minutes}m`;
            segment.Origin.DepTime = { time: moment(segment.Origin.DepTime).format('LT'), date: moment(segment.Origin.DepTime).format('ll') };
            segment.Destination.ArrTime = { time: moment(segment.Destination.ArrTime).format('LT'), date: moment(segment.Destination.ArrTime).format('ll') }
          });
        });
      });
    });
    if (res.arrResult.JourneyType === '1' || (res.arrResult.JourneyType === '2' && !res.arrResult.IsDomestic) || res.arrResult.JourneyType === '3') {
      this.flights = res.arrResult.Flights[0];
      this.flightlength = res.arrResult.Flights.length
    }
    else if ((res.arrResult.JourneyType === '2' && res.arrResult.IsDomestic && res.arrResult.Flights.length === 1)) {
      this.flights = res.arrResult.Flights[0];
      this.flightlength = res.arrResult.Flights.length
    }
    else {
      this.flightlength = res.arrResult.Flights.length
      this.RoundTrip.Inbound = res.arrResult.Flights[0];
      this.RoundTrip.Outbound = res.arrResult.Flights[1];
    }
  }

  getFareRules(ResultIndex: any) {
    debugger;
    let flight = this.flights.find(f => f.ResultIndex === ResultIndex);
    flight.isloading = true;
    this.flightService.getFareRules(ResultIndex, this.TokenId).subscribe((res: any) => {
      flight.FareDetails = res.arrResult[0].FareRuleDetail;
      flight.isloading = false;
    })



    // res.arrResult.Flights.forEach((flights,j)=>{
    //   flights.forEach((flight)=>{
    //    this.ResultIndex = flight.ResultIndex;
    //    this.setFareRules(this.ResultIndex);
    //   })
    // });
  }

  getFareRulesRDomestic(ResultIndex: any, type: any) {
    debugger;
    let flight;
    this.loading = true;
    if (type == 'inbound') {
      flight = this.RoundTrip.Inbound.find(f => f.ResultIndex === ResultIndex);
      flight.isloading = true;

    }
    else if (type == 'outbound') {
      flight = this.RoundTrip.Outbound.find(f => f.ResultIndex === ResultIndex);
      flight.isloading = true;
    }
    this.flightService.getFareRules(ResultIndex, this.TokenId).subscribe((res: any) => {
      flight.isloading = false;
      if (res.retCode === 1) {
        flight.FareDetails = res.arrResult[0].FareRuleDetail;
        console.log(this.arrRules);
        this.cd.detectChanges();
      }
    })

  }


  onSetActiveClass(classname: string, index: any, no: any) {
    debugger
    $('.' + classname + '').removeClass('activeroundblock');
    $('.' + no + '-' + index).addClass('activeroundblock');
    if (classname === 'roubdbk1') {
      this.Onward.index = index;
      this.Onward.slected = true
    }
    else if (classname === 'roubdbk2') {
      this.Return.index = index;
      this.Return.slected = true
    }
    this.total = (this.RoundTrip.Inbound[this.Onward.index].Fare.Charge.TotalPrice +
      this.RoundTrip.Outbound[this.Return.index].Fare.Charge.TotalPrice).toFixed(2)
  }

  GetFareQuote(ResultIndex: any) {
    debugger
    let Rindex = [];
    Rindex.push(ResultIndex)
    this.flightService.GetFareQuote(Rindex, this.TokenId).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.router.navigate(['/flight-booking'], { queryParams: { Index: ResultIndex, Id: this.TokenId } })
      }
      else {
        //this.confirmationModel(res.error);
      }
      console.log(res)
    })
  }

  GetFareQuoteRDomestic(InboundResultIndex: any, OutboundResultIndex: any) {
    debugger;
    let OnwardIndex = [];
    OnwardIndex.push(InboundResultIndex, OutboundResultIndex)
    this.flightService.GetFareQuote(OnwardIndex, this.TokenId).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.router.navigate(['/flight-booking'], { queryParams: { Index: InboundResultIndex, Index2: OutboundResultIndex, Id: this.TokenId } })
      } else {
        alert('' + res.error + '');
      }
      console.log(res);
    })



  }


  confirmationModel(message: any) {
    let data = {
      title: 'Price Change',
      message: message,
      cancel: 'Try another',
      ok: 'Continue'
    }

    const modal: NgbModalRef = this.modalService.open(ConfirmComponent, { centered: true });
    modal.componentInstance.ModelData = data;
    modal.result.then((result) => {
      debugger
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {
    debugger
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

