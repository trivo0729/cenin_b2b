import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm, FormArray } from '@angular/forms';
import { FlightService } from 'src/app/services/flight/flight.service';
import { ActivatedRoute } from '@angular/router';
import { arrResult, PaxDetail, FlightDetails, SSR, Passengers, RateBreack, Breackdowns } from 'src/app/modals/flight/FlightBooking.modal';
import * as moment from 'moment';
import { GenralService } from 'src/app/services/genral.service';
import { BookingRequest, ObjFlightBooking, Fare } from 'src/app/modals/flight/flight-booking-request.modal';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';

declare function StickySidebar(): any;
declare function Showtimepicker(): any;
@Component({
  selector: 'app-flight-booking',
  templateUrl: './flight-booking.component.html',
  styleUrls: ['./flight-booking.component.css']
})
export class FlightBookingComponent implements OnInit {
  PassengerDetails: Passengers[]
  bookingform: FormGroup;
  formarray: FormArray;
  panelOpenState = false;
  isloaded: boolean;
  submitted: boolean;
  opens: string;
  drops: string;
  singleDatePicker: boolean;
  autoApply: boolean;
  closeOnAutoApply: boolean;
  showDropdowns: boolean;
  lockStartDate: boolean;
  finalbooking: boolean;
  MinDate: any;
  MaxDate: any;
  ResultIndex: any;
  ResultIndex2: any;
  TokenID: any;
  Passengers: PaxDetail[];
  FlightDetail: FlightDetails[];
  arrFlightDetails: arrResult;
  SSR: SSR[];
  Breackdowns: Breackdowns[];
  RateBreack: RateBreack[];
  Baggages: Baggages[];
  MealDynamic: MealDynamic[];
  Meal: Meal[];
  Country: any[] = [];
  BookingRequest: BookingRequest;
  objFlightBooking: ObjFlightBooking;
  Fare: Fare;
  step = 0;

  constructor(
    private flightService: FlightService,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private genralService: GenralService,
    private cookie: CommonCookieService,
    private ServiceUrl: ApiUrlService
  ) {
    this.opens = 'down';
    this.drops = 'down';
    this.singleDatePicker = true;
  }

  ngOnInit() {
    debugger;
    StickySidebar();
    Showtimepicker();
    this.singleDatePicker = true;
    this.isloaded = false;
    this.submitted = false;
    this.finalbooking = false;
    this.Passengers = [];
    this.FlightDetail = [];
    this.SSR = [];
    this.Baggages = [];
    this.MealDynamic = [];
    this.Meal = [];
    this.Breackdowns = [];
    this.PassengerDetails = [];
    this.ResultIndex = this.route.snapshot.queryParamMap.get('Index');
    this.ResultIndex2 = this.route.snapshot.queryParamMap.get('Index2');
    this.TokenID = this.route.snapshot.queryParamMap.get('Id');
    this.arrFlightDetails = new arrResult();
    this.getFlightDetails();
    this.getCountry();
    var start = new Date();
    this.MinDate = moment(start);
    this.MaxDate = moment(start);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  getFlightDetails() {
    debugger;
    let RIndex = [];
    RIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null) {
      RIndex.push(this.ResultIndex2);
    }
    this.flightService.GetFlightDetails(RIndex, this.TokenID).subscribe((res: any) => {
      if (res.retCode == 1) {
        this.isloaded = true;
        console.log(JSON.stringify(res));
        this.arrFlightDetails = res.arrResult;
        this.Passengers = res.arrResult.Validate.PaxDetails;
        this.onBreackups(res.arrResult.Breackdowns);
        this.setSpecialService();
        this.formInit();
        this.onDetails()
        this.cd.detectChanges();
      }
    })
  }

  onDetails() {
    debugger
    this.arrFlightDetails.FlightDetails.forEach(flight => {
      flight.Segments.forEach(Segments => {
        Segments.forEach((segment, i) => {
          if (i !== 0 || i != Segments.length - 1) {
            let dep = "";
            let ariv = "";
          }
          var minutes = segment.Duration % 60
          var hours = (segment.Duration - minutes) / 60
          segment.Duration = `${hours}h ${minutes}m`;
          segment.Origin.DepTime = moment(segment.Origin.DepTime).format('llll');
          segment.Destination.ArrTime = moment(segment.Destination.ArrTime).format('llll');


        });
      });
    });
    this.FlightDetail = this.arrFlightDetails.FlightDetails;
  }

  setSpecialService() {
    debugger
    this.arrFlightDetails.SSR.forEach(ssr => {
      // for baggage

      if (ssr.Baggage) {
        ssr.Baggage.forEach((baggage) => {
          this.Baggages.push({
            from: baggage[0].Origin,
            to: baggage[0].Destination,
            baggage: baggage
          });
        });
      }
      // for Meal Dynamic

      if (ssr.MealDynamic) {
        ssr.MealDynamic.forEach(MealDynamic => {
          this.MealDynamic.push({
            from: MealDynamic[0].Origin,
            to: MealDynamic[0].Destination,
            mealdynamic: MealDynamic
          })
        });
      }

      // for Meal

      if (ssr.Meal) {
        this.Meal = ssr.Meal
      }

    });
  }


  choosedDOB(event: any, i: any) {

  }

  getCountry() {
    this.genralService.getCountry().subscribe((res: any) => {
      this.Country = res;
    });
  }

  onBreackups(Breackdowns: any) {
    debugger
    this.Breackdowns = [];
    Breackdowns.forEach((Breackdown) => {
      this.RateBreack = [];
      Breackdown.forEach((breackdown) => {
        this.RateBreack.push({
          Currency: breackdown.Currency,
          PassengerType: breackdown.PassengerType,
          PassengerCount: breackdown.PassengerCount,
          Fare: breackdown.Fare,
          Fees: breackdown.Fees,
          Taxes: breackdown.Taxes,
          TaxComponent: breackdown.TaxComponent,
          Total: breackdown.Total,
          Baggage: "",
          Meal: "",
        })
      });
      this.Breackdowns.push({ Breackdown: this.RateBreack });
    })
  }

  onBaggage(event: any, p: any, bg: any) {
    debugger
    this.Breackdowns.forEach((Breackdowns, i) => {
      Breackdowns.Breackdown.forEach((breackdown, b) => {
        if (i === bg && b === p) {
          breackdown.Baggage = event.Price
        }
      });
    });
  }

  onMeal(event: any) {
    debugger
  }

  onMealDynamic(event: any, p: any, md: any) {
    debugger
    this.Breackdowns[p].Breackdown[md].Meal = event.Price
    // this.Breackdowns.forEach((Breackdowns, i) => {
    //   Breackdowns.Breackdown.forEach((breackdown, b) => {
    //     if (i === md && b === p) {
    //       breackdown.Meal = event.Price
    //     }
    //   });
    // });
  }


  /* #region   form validation */

  formInit() {
    this.bookingform = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      mobile: ['', Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      customer: this.fb.array([this.create_Customer(0)]),
      gstnumber: ['', Validators.compose([])],
      gstcompanyname: ['', Validators.compose([])],
      gstcompanycontactno: ['', Validators.compose([])],
      gstcompanyaddress: ['', Validators.compose([])],
      gstcompanyemail: ['', Validators.compose([])],
    });
    this.formarray = this.bookingform.get('customer') as FormArray;
    this.addCustomer();
  }

  addCustomer() {
    debugger
    this.Passengers.forEach((passenger, i) => {
      if (i > 0)
        this.formarray.push(this.create_Customer(i));
    });
    console.log(this.bookingform);
  }

  create_Customer(i: any): FormGroup {
    let title; let name; let lastName; let dob; let passport;

    if (this.Passengers[i].Title.IsRequired)
      title = [Validators.required]
    else
      title = []
    if (this.Passengers[i].FirstName.IsRequired)
      name = [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]
    else
      name = []
    if (this.Passengers[i].LastName.IsRequired)
      lastName = [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]
    else
      lastName = []
    if (this.Passengers[i].Passport.IsRequired)
      passport = [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]
    else
      passport = []
    if (this.Passengers[i].DOB.IsRequired)
      dob = [Validators.required]
    else
      dob = []

    return this.fb.group({
      title: ['', Validators.compose(title)],
      name: ['', Validators.compose(name)],
      lastName: ['', Validators.compose(lastName)],
      dob: ['', Validators.compose(dob)],
      passportno: ['', Validators.compose(passport)],
      expirydate: ['', Validators.compose(passport)],
    });

  }

  getcustomerFormGroup(index): FormGroup {
    const formGroup = this.formarray.controls[index] as FormGroup;
    return formGroup;
  }

  get customerFormGroup() {
    return this.bookingform.get('customer') as FormArray;
  }
  /* #endregion */


  onSubmit() {
    debugger
    this.submitted = true;

    if (this.bookingform.invalid)
      return
    this.onConfirm();
  }

  onConfirm() {
    let ResultIndex = [];
    ResultIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null)
      ResultIndex.push(this.ResultIndex2);

    this.flightService.ConfirmFlights(ResultIndex, 0, 0, this.TokenID).subscribe((res: any) => {
      console.log(JSON.stringify(res));
      if (res.retCode === 1) {
        this.finalbooking = true;
        this.onBooking();
      }

    });
  }

  onBooking() {
    const controls = this.bookingform.controls;
    this.BookingRequest = new BookingRequest();
    this.BookingRequest.objFlightBooking = new ObjFlightBooking();
    this.BookingRequest.objFlightBooking.Passengers = [];
    this.Fare = new Fare();
    this.Fare.BaseFare = this.FlightDetail[0].Fare.BaseFare;
    this.Fare.Tax = this.FlightDetail[0].Fare.Tax;
    this.Fare.YqTax = this.FlightDetail[0].Fare.YqTax;
    this.Fare.AdditionalTxnFeeOfrd = this.FlightDetail[0].Fare.AdditionalTxnFeeOfrd;
    this.Fare.AdditionalTxnFeePub = this.FlightDetail[0].Fare.AdditionalTxnFeePub;
    this.Fare.OtherCharges = this.FlightDetail[0].Fare.OtherCharges;
    for (let i = 0; i < this.Passengers.length; i++) {
      let formGroup = this.formarray.controls[i] as FormGroup;
      let City = "", CountryCode = "", CountryName = "", Email = "", ContactNo = "", AddressLine1 = "", Type = 0, Gender = 0;

      if (i === 0) {
        City = controls.city.value;
        CountryCode = controls.country.value.Country
        CountryName = controls.country.value.Countryname
        Email = controls.email.value
        ContactNo = controls.mobile.value
        AddressLine1 = controls.address.value
      }

      if (this.Passengers[i].PaxType === "AD") {
        Gender = 2;
        Type = 1;
      }
      else if (this.Passengers[i].PaxType === "CH") {
        Gender = 1;
        Type = 2
      }

      else if (this.Passengers[i].PaxType === "In") {
        Gender = 1;
        Type = 3
      }

      let Passenger = {
        Title: formGroup.controls['title'].value,
        FirstName: formGroup.controls['name'].value,
        LastName: formGroup.controls['lastName'].value,
        PaxType: Type,
        DateOfBirth: formGroup.controls['dob'].value,
        Gender: Gender,
        PassportNo: formGroup.controls['passportno'].value,
        PassportExpiry: formGroup.controls['expirydate'].value,
        AddressLine1: AddressLine1,
        AddressLine2: "",
        City: City,
        CountryCode: CountryCode,
        CountryName: CountryName,
        Nationality: CountryName,
        ContactNo: ContactNo,
        Email: Email,
        IsLeadPax: this.Passengers[i].IsLeading,
        Fare: this.Fare,
        FfAirlineCode: "",
        FfNumber: "",
        GstCompanyAddress: "",
        GstCompanyContactNumber: "",
        GstCompanyName: null,
        GstNumber: null,
        GstCompanyEmail: null,
        Baggage: [],
        MealDynamic: [],
        SeatDynamic: [],
      }

      this.BookingRequest.objFlightBooking.Passengers.push(Passenger)
    }

    this.BookingRequest.objFlightBooking.PreferredCurrency = null;
    let ResultIndex = [];
    ResultIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null)
      ResultIndex.push(this.ResultIndex2);
    this.BookingRequest.objFlightBooking.ResultIndexes = ResultIndex;

    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.BookingRequest.objFlightBooking.AgentReferenceNo = data.LoginDetail.sid;
      }
      if (data.LoginDetail.UserType === "B2C") {
        this.BookingRequest.objFlightBooking.AgentReferenceNo = this.ServiceUrl.UserId.toString();
      }
    }
    else
      return

    this.BookingRequest.objFlightBooking.EndUserIp = "203.192.219.93";
    this.BookingRequest.objFlightBooking.TokenIded = "";
    this.BookingRequest.objFlightBooking.TraceId = "";
  }

  Book() {
    console.log(JSON.stringify(this.BookingRequest.objFlightBooking));
    const controls = this.bookingform.controls;
    this.flightService.BookingFlights(this.BookingRequest.objFlightBooking, controls.gstnumber.value,
      controls.gstcompanyname.value, controls.gstcompanycontactno.value, controls.gstcompanyaddress.value,
      controls.gstcompanyemail.value, this.TokenID, [],
    ).subscribe((res: any) => {
      console.log(res);
    });
  }

  onEdit() {
    this.isloaded = true
    this.finalbooking = false;
  }

}


export class Baggages {
  from: string;
  to: string;
  baggage: any[];
}

export class MealDynamic {
  from: string;
  to: string;
  mealdynamic: any[];
}

export class Meal {
  from: string;
  to: string;
  meal: any[];
}

