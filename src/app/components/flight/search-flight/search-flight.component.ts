import { Component, OnInit, ChangeDetectorRef, Input, ViewChild } from '@angular/core';
import { Journey, Seat, Destinations, FlightAutocomplete, FlightSearch } from 'src/app/modals/flight/flight-search.modal';
import * as moment from 'moment';
import { tap, switchMap, finalize } from 'rxjs/operators';
import { FlightService } from 'src/app/services/flight/flight.service';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { objSearch } from "src/app/modals/flight/flight-search.modal";
import { Router } from '@angular/router';

declare function dropPanel(): any;

@Component({
  selector: 'app-search-flight',
  templateUrl: './search-flight.component.html',
  styleUrls: ['./search-flight.component.css']
})
export class SearchFlightComponent implements OnInit {
  Journey: Journey[] = [{ name: 'One Way', id: '1' }, { name: 'Round trip', id: '2' }, { name: 'Multi City', id: '3' }];
  Seats: Seat[] = [{ name: 'All', id: '1' }, { name: 'Economy', id: '2' }, { name: 'Premium Economy', id: '3' }, { name: 'Business', id: '4' }, { name: 'Premium Business', id: '5' }, { name: 'First', id: '6' }];
  @Input() JourneyType: string;
  @Input() Class: string;
  @Input() Destinations: Destinations[];
  @Input() singleDatePicker: boolean;
  @Input() OriginCode: any;
  @Input() DestinationCode: any;
  @Input() Search: objSearch;
  @Input() TotelGuest: number;
  @Input() Direct: boolean;

  autoApply: boolean;
  closeOnAutoApply: boolean;
  showDropdowns: boolean;
  lockStartDate: boolean;
  opens: string;
  drops: string;
  frmAirports = new FormControl();
  isLoading = false;
  errorMsg: string;

  constructor(private cd: ChangeDetectorRef,
    private flightService: FlightService,
    private fb: FormBuilder,
    private alert: AlertService,
    private router: Router
  ) {
    this.opens = 'left';
    this.drops = 'down';
  }

  ngOnInit() {
    //this.getAirports();

  }

  ngAfterViewInit() {
    let chk: any = document.getElementsByClassName('ltr');
    chk[0].style.left = '-320px';
    dropPanel();
  }

  onJourneyType(JourneyType: any) {
    debugger;
    if (JourneyType === '1') {
      var start = new Date();
      this.singleDatePicker = true;
      if (this.Destinations.length > 1) {
        let data = this.Destinations[0];
        this.Destinations = [];
        this.Destinations.push(
          {
            Origin: data.Origin,
            OriginCode: '',
            Destination: data.Destination,
            DestinationCode: '',
            JourneyDate: moment(start),
            MinDate: moment(start), OriginAirports: [],
            DestinationAirports: []
          });
      }
      else
        this.Destinations[0].JourneyDate = moment(start)

    }

    else if (JourneyType === '2') {
      this.singleDatePicker = false;
      setTimeout(() => {
        let chk: any = document.getElementsByClassName('ltr');
        chk[0].style.left = '-320px';
      }, 50);
      var start = new Date();
      var end = new Date();
      end.setDate(end.getDate() + 1);
      if (this.Destinations.length > 1) {
        let data = this.Destinations[0];
        this.Destinations = [];
        this.Destinations.push({
          Origin: data.Origin,
          OriginCode: data.OriginCode,
          Destination: data.Destination,
          DestinationCode: data.DestinationCode,
          JourneyDate: {
            start: moment(start),
            end: moment(end),
          },
          MinDate: moment(start),
          OriginAirports: [],
          DestinationAirports: []
        });
      }
      else {
        this.Destinations[0].JourneyDate = {
          start: moment(start),
          end: moment(end),
        }
      }
    }

    else if (JourneyType === '3') {
      this.singleDatePicker = true;
      let i = this.Destinations.length - 1;
      var start = new Date();
      this.Destinations[0].JourneyDate = {
        start: moment(start),
        end: moment(end),
      };
      this.Destinations.push({
        Origin: this.Destinations[i].Destination,
        OriginCode: this.Destinations[i].DestinationCode,
        Destination: '',
        DestinationCode: '',
        JourneyDate: {
          start: moment(start),
          end: moment(start),
        }, MinDate: moment(start),
        OriginAirports: [],
        DestinationAirports: []
      })
    }
    this.cd.detectChanges();
  }

  onDirct(event: any) {
    console.log(this.Direct);
  }

  onSwap(i: any, a: string, b: string) {
    debugger;
    this.Destinations[i].Origin = b;
    this.Destinations[i].Destination = a;
    console.log(this.Destinations);
  }


  onAddcity() {
    debugger
    let i = this.Destinations.length - 1;
    let date = this.Destinations[i].JourneyDate;
    this.Destinations.push({
      Origin: this.Destinations[i].Destination,
      OriginCode: this.Destinations[i].DestinationCode,
      Destination: '',
      DestinationCode: '',
      JourneyDate: {
        start: moment(date.start),
        end: moment(date.start)
      }, MinDate: moment(date.start),
      OriginAirports: [],
      DestinationAirports: []
    });
    this.cd.detectChanges();
  }

  choosedDate(event: any, index: any) {
    if (event.start !== null && this.JourneyType === '3') {
      this.Destinations.forEach((d, i) => {
        if (index < i) {
          d.MinDate = moment(event.start);
          d.JourneyDate = {
            start: moment(event.start),
            end: moment(event.end),
          }
        }
      });
      this.cd.detectChanges();
    }
  }

  onRemovecity() {
    this.Destinations.pop();
    this.cd.detectChanges();
  }

  // getAirports() {
  //   this.frmAirports.valueChanges
  //     .pipe(
  //       tap(() => {
  //         this.errorMsg = "";
  //         this.Airports = [];
  //         this.isLoading = true;
  //       }),
  //       switchMap(value => this.flightService.getAirports(value)
  //         .pipe(
  //           finalize(() => {
  //             this.isLoading = false
  //           }),
  //         )
  //       )
  //     )
  //     .subscribe((res: any) => {
  //       if (res.retCode === 1)
  //         this.Airports = res.arrResult;
  //     });
  // }

  getAirports(event: any, i: any, airport: string) {
    debugger;
    let value = event.target.value;
    if (value.length < 3)
      return;
    this.flightService.getAirports(value).subscribe((res: any) => {
      if (res.retCode === 1) {
        switch (airport) {
          case 'origin':
            this.Destinations[i].OriginAirports = res.arrResult;
            break;

          case 'destination':
            this.Destinations[i].DestinationAirports = res.arrResult;
            break;
        }
      }
    })
  }

  onAirportselect(value: any, i: any, airport: string) {
    debugger;
    switch (airport) {
      case 'origin':
        this.Destinations[i].OriginCode = value;
        this.OriginCode = this.Destinations[i].OriginCode;
        break;

      case 'destination':
        this.Destinations[i].DestinationCode = value;
        this.DestinationCode = this.Destinations[i].DestinationCode;
        break;
    }
    this.cd.detectChanges();
  }

  onTraveller(paxType: any, action: any) {
    debugger;
    switch (action) {
      case 'plus':
        // * add pax * //
        this.add(paxType);
        break;
      case 'minus':
        // * remove pax * //
        this.remove(paxType);
        break;
    }
    this.TotelGuest = this.Search.AdultCount + this.Search.ChildCount + this.Search.InfantCount;
  }

  add(paxType: any) {
    if ((this.TotelGuest) >= 9) {
      this.alert.confirm('Maximum of 9 travellers allowed', 'error')
      return;
    }
    else {
      switch (paxType) {
        case 'ad':
          this.Search.AdultCount = this.Search.AdultCount + 1;
          break;
        case 'ch':
          this.Search.ChildCount = this.Search.ChildCount + 1;
          break;
        case 'in':
          if (this.Search.InfantCount >= this.Search.AdultCount)
            this.alert.confirm('Number of infants cannot be more than adults', 'error')
          else
            this.Search.InfantCount = this.Search.InfantCount + 1;
          break
      }
    }
  }

  remove(paxType: any) {
    switch (paxType) {
      case 'ad':
        if (this.Search.AdultCount > 1)
          this.Search.AdultCount = this.Search.AdultCount - 1;
        break;
      case 'ch':
        if (this.Search.ChildCount > 0)
          this.Search.ChildCount = this.Search.ChildCount - 1;
        break;
      case 'in':
        if (this.Search.InfantCount > 0)
          this.Search.InfantCount = this.Search.InfantCount - 1;
        break
    }
  }

  onSearch() {
    debugger;
    this.Search.username = "holidays@clickurtrip.com";
    this.Search.Password = "India2007";
    this.Search.EndUserIp = "203.192.219.93";
    this.Search.OneStopFlight = "false";
    if (this.Direct)
      this.Search.DirectFlight = "true";
    else
      this.Search.DirectFlight = "false";
    this.Search.PreferredAirlines = null;
    this.Search.Sources = null;
    this.Search.TokenId = "";
    this.Search.JourneyType = this.JourneyType;
    this.Search.Segments = [];
    if (this.JourneyType !== '2') {
      this.Destinations.forEach(d => {
        this.Search.Segments.push(
          {
            Origin: d.OriginCode,
            OriginCity: d.Origin,
            Destination: d.DestinationCode,
            DestinationCity: d.Destination,
            FlightCabinClass: this.Seats.find(s => s.name === this.Class).id,
            PreferredDepartureTime: moment(d.JourneyDate.start).format('DD-MM-YYYY'),
            PreferredArrivalTime: moment(d.JourneyDate.start).format('DD-MM-YYYY'),
          }
        );
      });
    }
    else {
      for (let i = 0; i < 2; i++) {
        let Origin = this.Destinations[0].OriginCode;
        let OriginCity = this.Destinations[0].Origin;
        let Destination = this.Destinations[0].DestinationCode;
        let DestinationCity = this.Destinations[0].Destination;
        let start = moment(this.Destinations[0].JourneyDate.start).format('DD-MM-YYYY');
        let end = moment(this.Destinations[0].JourneyDate.start).format('DD-MM-YYYY');
        let city = "";
        let cityname = "";
        if (i !== 0) {
          city = Origin;
          Origin = Destination;
          Destination = city;
          cityname = OriginCity;
          OriginCity = DestinationCity;
          DestinationCity = cityname;
          start = moment(this.Destinations[0].JourneyDate.end).format('DD-MM-YYYY');
          end = moment(this.Destinations[0].JourneyDate.end).format('DD-MM-YYYY');
        }
        this.Search.Segments.push(
          {
            Origin: Origin,
            OriginCity: OriginCity,
            Destination: Destination,
            DestinationCity: DestinationCity,
            FlightCabinClass: this.Seats.find(s => s.name === this.Class).id,
            PreferredDepartureTime: start,
            PreferredArrivalTime: end,
          }
        )
      }
    }
    ///Flight Work 
    this.flightService.SetFligthSearchParam(this.Search);
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url;

    if (currentUrl == '/flight-list') {
      this.router.navigateByUrl(currentUrl)
        .then(() => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        });
    } else
      this.router.navigate(['/flight-list']);
    console.log(JSON.stringify(this.Search));
  }
}
