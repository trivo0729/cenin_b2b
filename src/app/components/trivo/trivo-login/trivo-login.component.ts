import { Component, OnInit } from '@angular/core';
import { ForgetPasswordComponent } from '../../user/forget-password/forget-password.component';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { MatDialog } from '@angular/material';
import { HomeComponent } from '../../home/home.component';
import { AppComponent } from 'src/app/app.component';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-trivo-login',
  templateUrl: './trivo-login.component.html',
  styleUrls: ['./trivo-login.component.css']
})
export class TrivoLoginComponent implements OnInit {

  AdminID: number;
  UserType: string[];
  loading: boolean;
  submitted: boolean;
  signin: FormGroup;
  login: Userdetails;
  sUserName: string;
  sPassword: string;

  constructor(private userService: UserService,
    private router: Router,
    private toastr: ToastrService,
    private cookie: CommonCookieService,
    public dialog: MatDialog,
    public UrlService: ApiUrlService,
    private formBuilder: FormBuilder,
    private app: AppComponent,
    private home: HomeComponent
  ) {
    this.AdminID = UrlService.AdminID;
    this.UserType = ["B2B"];
  }

  ngOnInit() {
    this.loading = false;
    this.submitted = false;
    this.signin = this.formBuilder.group({
      UserName: ['', Validators.required],
      Password: ['', [Validators.required]],
    }
    );
  }
  get f() { return this.signin.controls; }


  getUserlogin() {
    this.loading = true;
    this.submitted = true;
    if (this.signin.invalid) {
      this.loading = false;
      return;
    }
    this.login = new Userdetails();
    this.userService.UserLogin(this.sUserName, this.sPassword, this.AdminID, this.UserType).subscribe((res: any) => {
      this.loading = false;
      debugger;
      if (res.retCode == 1) {
        this.login = res;
        this.userService.setLogedIn(JSON.stringify(res));
        this.app.SessionTimerStart();
        if (this.cookie.checkcookie('login'))
          this.cookie.deletecookie('login');
        this.toastr.success("Login Success");
        var current = new Date();
        current.setTime(current.getTime() + (120 * 60 * 1000));
        this.cookie.setcookie('login', JSON.stringify(res), current);
        // this.app.GetLoginDetails();
        // this.home.ngOnInit();
        window.location.reload();
      }
      else {
        this.toastr.error(res.Message.toString());
        this.login.retCode = res.retCode;
      }
    }, error => {
      console.log(error);
      this.loading = false;
    });
  }

  ForgotDialog() {
    this.dialog.closeAll();
    this.dialog.open(ForgetPasswordComponent, {
      height: 'auto',
      width: '600px',
      data: {
        userType: "B2C"
      }
    });
  }

}
