import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { MenuService } from 'src/app/services/commons/menu.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { ExchangeRate } from 'src/app/modals/dbhelper.model';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { AccountService } from 'src/app/services/agent/account.service';
declare function Menu(): any;

@Component({
  selector: 'app-trivo-header',
  templateUrl: './trivo-header.component.html',
  styles: ['./trivo-header.component.css']
})
export class TrivoHeaderComponent implements OnInit {
  @Output() loginDialog = new EventEmitter();
  @Output() RegtrDialog = new EventEmitter();
  @Output() _logout = new EventEmitter();
  UsetId: number;
  @Input() login: any
  logo: string = "";
  SalesPerson: SalesPerson[];
  arrBalance: arrBalance;
  public Currency: string = "AED";
  public exchange: ExchangeRate[];
  menu: Menulist[];
  _mainMenu: Menulist[];
  _userMenu: Menulist[];
  CustomerName: string = null;
  CustomerEmail: string = null;
  constructor(
    private menuService: MenuService,
    public ServiceUrl: ApiUrlService,
    private accountService: AccountService,
    private cookie: CommonCookieService
  ) {
    this.logo = ServiceUrl.logo;
  }

  ngOnInit() {
    debugger
    this.SalesPerson = [];
    this.arrBalance = new arrBalance();
    this.login = this.login
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.getManu();
        this.GetBalanceInformation(data.LoginDetail.sid);
        this.getSalesPerson(data.LoginDetail.sid);
      }
    }
    Menu();
  }

  GetBalanceInformation(uid: number) {
    this.accountService.GetBalanceInformation(uid).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.arrBalance = res.arrBalance;
      }
      //console.log(res);
    })
  }

  getSalesPerson(uid: any) {
    this.accountService.getSalesPerson(uid,"","").subscribe((res: any) => {
      if (res.retCode === 1) {
        this.SalesPerson = res.arrResult;
      }
      //console.log(res);
    })
  }

  getManu() {
    this.menu = [];
    if (this.cookie.checkcookie('menu')) {
      let s = this.cookie.checkcookie('menu');
      let data = JSON.parse(this.cookie.getcookie('menu'));
      if (data.UserId === this.getUserId()) {
        this.menu = data.Menu;
        this.generateMenu();
      }
      if (data.UserId !== this.getUserId()) {
        this.cookie.deletecookie('menu');
      }
    }
    if (!(this.cookie.checkcookie('menu'))) {
      this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
        this.menu = res;
        if (this.menu.length > 0) {
          const menudetails = { UserId: this.getUserId(), Menu: this.menu }
          this.cookie.setcookie('menu', JSON.stringify(menudetails), 1);
          this.generateMenu();
        }
      }, error => {
        console.log(error);
      });
    }
  }

  getUserId() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.ServiceUrl.UserId;
        }
      }
    }
    else {
      this.UsetId = this.ServiceUrl.UserId;
    }
    return this.UsetId;
  }

  generateMenu() {
    this._mainMenu = [];
    this._userMenu = []
    this.menu.forEach(menu => {
      if (menu.Name === 'Home' || menu.Name === 'Hotel' || menu.Name === 'Flights' || menu.Name === 'Sightseeing' || menu.Name === 'Visa' || menu.Name === 'OTB' || menu.Name === 'Package')
        this._mainMenu.push(menu);
      if (menu.Name === 'Login')
        this._userMenu.push(menu)
    });
  }

  onSigninDialog() {
    this.loginDialog.emit();
  }

  onRegisterDialog() {
    this.RegtrDialog.emit();
  }

  onSignout() {
    this._logout.emit();
  }

}

export class arrBalance {
  Currency: string;
  Available: string;
  Payable: string;
  LimitExpire: string;
}

export class SalesPerson {
  Mobile: string;
  ContactPerson: string;
  Last_Name: string;
}
