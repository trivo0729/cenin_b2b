import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Tours, TicketType } from 'src/app/modals/tours.modals';
import { SessionService } from 'src/app/services/session.service';
import { ToursService } from 'src/app/services/tours.service';
import { MatDialog } from '@angular/material';
import { ReviewsComponent } from '../../reviews/reviews.component';
import { AddReviewsComponent } from '../../add-reviews/add-reviews.component';
import { GoogleService } from 'src/app/services/google.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare function stricyBar(): any;
declare function setimage(img):any;
declare function  Carousel(): any;
declare function owl(): any;
declare function ChangeStyle(): any;

@Component({
  selector: 'app-tour-details',
  templateUrl: './tour-details.component.html',
  styleUrls: ['./tour-details.component.css']
})
export class TourDetailsComponent implements OnInit {
 tour:Tours = new  Tours();
 Date:string="";
 arrTicketType:TicketType = new TicketType();
 img:string="";
 tours = new Array();
 opened = false;
 Images:any[]
  constructor(private sessionService:SessionService,
    private googleService : GoogleService,
    private tourService: ToursService,
    private ngxService: NgxUiLoaderService,
    private cd: ChangeDetectorRef,
    ) { }
  ngOnInit() {
    debugger;
    this.ngxService.start();
    this.Images=[];
    var arrSearch = this.tourService.getSerachParams();
    this.Date = arrSearch.sDate;
    this.tour=  this.sessionService.get("Details");
    this.tours.push(this.tour);
    this.img = this.tour.ListImage.Url;
    
    try {
      this.GetTourImage();
      setTimeout(()=> {
        setimage(this.img)
      },1000);
      setInterval(()=> {
        setimage(this.img)
      },500);
    }catch(e){}
    this.ngxService.stop();
    this.tourService.SavePopularTour(this.tour.ActivityID,this.tour.TotalPrice).subscribe((res: any) => {
    
    });
     stricyBar();
     setInterval(()=> {
      ChangeStyle();
     },1000);

    
  }

  GetTourImage(){
    this.tourService.GetImage(this.tour.ActivityID).subscribe((res: any) => {
        this.Images = res;
        setTimeout(()=> {
          owl();
          setimage(this.img)
         },20);
    });

  }

  Getmap(event: any){
    debugger;
     if(event.tab.textLabel=="Map"){
       if(this.opened==false){
        this.googleService.loadsinglemap(this.tours);
        this.opened=true;
       }
     }
  }

  gettype(arrTicketType:TicketType){
    this.arrTicketType = arrTicketType;
    if( this.arrTicketType.arrCancellationPolicy.length !=0)
       this.arrTicketType.arrCancellationPolicy = this.tourService.getCancellations( this.arrTicketType.arrCancellationPolicy);
       this.cd.detectChanges();
       console.log(this.arrTicketType);
  }
  
}
