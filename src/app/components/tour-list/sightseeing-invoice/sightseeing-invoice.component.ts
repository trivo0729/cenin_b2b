import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { BookingDetails } from 'src/app/modals/tours.modals';
import { AgentDetails, AdminUser } from 'src/app/modals/hotel/report.modal';

@Component({
  selector: 'app-sightseeing-invoice',
  templateUrl: './sightseeing-invoice.component.html',
  styleUrls: ['./sightseeing-invoice.component.css']
})
export class SightseeingInvoiceComponent implements OnInit {
  @Input() AgentDetails: AgentDetails;
  @Input() AdminUser: AdminUser;
  @Input() InvoiceDetails: BookingDetails;
  @Input() userdetails: Userdetails;
  @Output() bookingAction = new EventEmitter();

  public scale = 0.6;
  isb2c: boolean;

  constructor() { }

  ngOnInit() {
  }

}
