import { Component, OnInit, NgZone, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { Tours, resultTours, TourTypes, sPriceRange, objTour, arrSearch, arrUserDetails } from 'src/app/modals/tours.modals';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { GoogleService } from 'src/app/services/google.service';
import { ToastrService } from 'ngx-toastr';
import 'src/assets/js/map.js';
import { OrderPipe } from 'ngx-order-pipe';
import * as moment from 'moment';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $: JQueryStatic;
declare function Range_Slider(min: number, max: number, currency: string): any;
declare function Showtimepicker(): any;
declare function openNav(num:number): any;
declare function closeNav(num:number): any;
@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.css']
})
export class TourListComponent implements OnInit {
  objTour: objTour;
  arrSearch: arrSearch;
  arrUserDetails: arrUserDetails;
  constructor(private tourService: ToursService, private router: Router,
    private sessionService: SessionService,
    private googleService: GoogleService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
    private zone: NgZone,
    private orderPipe: OrderPipe,
    private ServiceUrl: ApiUrlService,
    private ngxService: NgxUiLoaderService,
  ) {
    console.log(this.orderPipe.transform(this.arrFilter, this.order)); // both this.array and this.order are from above example AppComponent
  }
  arrResult: resultTours = new resultTours();
  price: sPriceRange = new sPriceRange();
  tours: Tours[] = [];
  Temptours: Tours[] = [];
  arrFilter: Tours[] = [];
  Date: string = "";
  SlotDate: string;
  page: number = 1;
  sighseeing: Tours = new Tours();
  arrTourType: TourTypes[] = [];
  TempPrice: string;
  order: string = 'tour.TotalPrice';
  reverse: boolean = false;
  wizard:string = "Type";
  ngOnInit() {
    debugger
    this.ngxService.start();
    Showtimepicker();
    const Data = this.tourService.getSerachParams();
    this.arrUserDetails = new arrUserDetails();
    this.arrSearch = new arrSearch();
    this.arrSearch.date = Data.sDate;
    this.arrSearch.locationID = Data.LocationID;
    this.arrSearch.tourType = Data.TourType;
    this.arrUserDetails.userName = this.ServiceUrl.UserName;
    this.arrUserDetails.password = this.ServiceUrl.Password;
    this.arrUserDetails.parentID = this.ServiceUrl.AdminID;
    this.arrSearch.arrUserDetails = this.arrUserDetails;
    this.setSearchParm(Data);
    this.tourService.getTours(this.arrSearch).subscribe((res: resultTours) => {
      this.ngxService.stop();
      this.Date = Data.sDate;
      this.SlotDate = Data.sDate;
      this.arrResult = res;
      if (this.arrResult.retCode == 1) {
        this.arrFilter = this.arrResult.Activities;
        this.Temptours = this.arrResult.Activities;
        this.tours = this.arrResult.Activities;
        this.arrTourType = this.arrResult.arrTouType;
        this.price = this.arrResult.PriceRange;
        Range_Slider(this.price[0].MinPrice, this.price[0].MaxPrice, this.tours[0].CurrencyClass);
        console.log(this.arrResult.PriceRange);
        this.googleService.loadmap(this.tours);
        this.arrTourType.forEach(r => { r.checked = false });
        const self = this;
        this.zone.run(() => {
          $('.price_range').on('change', function (data) {
            self.PriceFilter(data);
          });
        });
        this.cd.detectChanges();
      }
      else {
        this.toastr.error(this.arrResult.Massage, '');
      }
    });
  }


  setSearchParm(data: any) {
    debugger;
    this.objTour = new objTour();
    this.objTour.Location = this.arrSearch.locationID;
    this.objTour.LocationName = data.LocationName;
    this.objTour.Date = moment(this.arrSearch.date, 'DD-MM-YYYY');
    this.objTour.TourType = this.arrSearch.tourType;
  }
  getTourType(ID: Number, name: string,i:number) {
    debugger;
    this.ngxService.start();
    this.sighseeing = undefined;
    this.tours.forEach(arrtour => {
      if (arrtour.ActivityID == ID)
        this.sighseeing = arrtour;
        this.wizard ="Type";
        this.ngxService.stop();
        openNav(i);
    });
  }
  showTourDetails(tour: Tours) {
    this.sessionService.clear("Details");
    this.sessionService.set("Details", JSON.stringify(tour));
    window.location.href = "/tour-details";
    //this.router.navigateByUrl("/tour-details")
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }
    if (value >= 1000) {
      return value + 'k';
    }
    return value;
  }

  getIngettourdetailsfoBox(item: any) {
    debugger
    console.log(item)
  }

  ActivityFilter(value: string, Name: string) {
    debugger;
    this.arrFilter = [];
    if (value === 'T') {
      this.arrTourType.filter(d => d.Name === Name).forEach(r => {
        const isChecked = this.arrTourType.filter(d => d.Name === Name)[0].checked;
        if (isChecked) {
          r.checked = false;
        } else {
          r.checked = true;
        }
      });
    }
    this.Temptours.forEach(arrtours => {
      this.arrTourType.forEach(r => {
        if (arrtours.List_Type.filter(d => d.Name == r.Name).length != 0 && r.checked)
          if (this.arrFilter.indexOf(arrtours) == -1)
            this.arrFilter.push(arrtours)
      });
    });
    if (this.arrFilter.length == 0) {
      this.arrFilter = this.Temptours;
    }
    this.cd.detectChanges();
  }

  PriceFilter(data: any) {
    this.TempPrice = data.currentTarget.value.split(';');
    this.arrResult.PriceRange.MinPrice = parseInt(this.TempPrice[0]);
    this.arrResult.PriceRange.MaxPrice = parseInt(this.TempPrice[1]);
    this.arrFilter = [];
    this.Temptours.forEach(arrtours => {
      if (this.arrResult.PriceRange.MinPrice <= arrtours.TotalPrice && this.arrResult.PriceRange.MaxPrice >= arrtours.TotalPrice) {
        this.arrFilter.push(arrtours);
      }
    });
    this.cd.detectChanges();
  }

  setOrder(value: string) {
    debugger
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  closenav(i:any){
    debugger
    try{
      this.sighseeing = new Tours();
      closeNav(i)
      this.cd.detectChanges();
    }catch(e){}
  }
}
