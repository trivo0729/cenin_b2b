import { Component, OnInit } from '@angular/core';
import { Tours, resultTours, Search_Params, arrSearch } from 'src/app/modals/tours.modals';
import { SessionService } from 'src/app/services/session.service';
import { ToursService } from 'src/app/services/tours.service';
import { Router } from '@angular/router';
import { GoogleService } from 'src/app/services/google.service';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  arrSearch: arrSearch;
  Search_Params:Search_Params = new Search_Params();
  arrResult : resultTours = new resultTours();
  tours: Tours[] = [];
  constructor(
    private tourService: ToursService, private router: Router,
    private sessionService:SessionService,
    private googleService: GoogleService,
    ) { }
  ngOnInit() {
    this.Search_Params.LocationID = "17";
    this.tourService.setSerachParams(this.Search_Params);
    this.tourService.getTours(this.arrSearch).subscribe((res: resultTours) => {
    this.arrResult = res;
    if( this.arrResult.retCode ==1)
      {
        this.tours =  this.arrResult.Activities;
        this.googleService.loadmap(this.tours);
      }
      else
      {

      }
    });
  }

showTourDetails(tour:Tours)
{
 this.sessionService.clear("Details");
 this.sessionService.set("Details",JSON.stringify(tour));
 this.router.navigateByUrl("/tour-details")
}

}
