import { Component, OnInit } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';
import { Search_Params, resultTours, sPriceRange, Tours, arrSearch, arrUserDetails } from 'src/app/modals/tours.modals';
import { GoogleService } from 'src/app/services/google.service';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  arrSearch: arrSearch;
  userdata: any;
  count :number;
  arrUserDetails: arrUserDetails;
  Search_Params: Search_Params = new Search_Params();
  arrResult: resultTours = new resultTours();
  price: sPriceRange = new sPriceRange();
  tours: Tours[] = [];
  constructor(
    private tourService: ToursService, private router: Router,
    private sessionService: SessionService,
    private googleService: GoogleService,
    private ServiceUrl: ApiUrlService,
    private cookie: CommonCookieService
  ) { }
  ngOnInit() {
    debugger;
    this.sessionService.clear("SearchParams");
    this.Search_Params.LocationID = "Dubai";
    this.tourService.setSerachParams(this.Search_Params);
    this.arrSearch = new arrSearch();
    this.arrUserDetails = new arrUserDetails();
    this.arrUserDetails = this.getUserdata();
    this.arrSearch.arrUserDetails = this.arrUserDetails;
    this.arrSearch.date = "";
    this.arrSearch.tourType = "";
    this.arrSearch.locationID = "Dubai";
    this.tourService.getTours(this.arrSearch).subscribe((res: resultTours) => {
      this.arrResult = res;
      if (this.arrResult.retCode == 1) {
        this.tours = this.arrResult.Activities;
        this.count = this.tours.length;
        this.tours = this.tours.sort((n1, n2) => {
          return this.naturalCompare(n1.TotalPrice, n2.TotalPrice)
        })
        this.tours = this.tours.slice(0,9);
        console.log(this.tours);
        this.price = this.arrResult.PriceRange;
        this.googleService.loadmap(this.tours);
      }
      else {

      }
    });
  }
   naturalCompare(a:any, b:any) {
    var ax = [], bx = [];
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
 }
  getUserdata() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.userdata = {
            userName: data.LoginDetail.Email,
            password: data.LoginDetail.Password,
            parentID: this.ServiceUrl.AdminID
          }
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.userdata = {
            userName: this.ServiceUrl.UserName,
            password: this.ServiceUrl.Password,
            parentID: this.ServiceUrl.AdminID
          }
        }
      }
    }
    else {
      this.userdata = {
        userName: this.ServiceUrl.UserName,
        password: this.ServiceUrl.Password,
        parentID: this.ServiceUrl.AdminID
      }
    }
    return this.userdata;
  }

  showTourDetails(tour: Tours) {
    this.sessionService.clear("Details");
    this.sessionService.set("Details", JSON.stringify(tour));
    window.location.href = "/tour-details";
  }
  showAllTour() {
    try {
      window.location.href = "/tours-list";
    } catch (ex) {
      console.log(ex);
    }
  }
}
