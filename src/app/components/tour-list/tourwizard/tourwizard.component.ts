import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Tours, DateRange, Slots, TicketType, PaxTyp, TourType, TourTypes, Dates } from 'src/app/modals/tours.modals';
import { ToursService } from 'src/app/services/tours.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { ToastrService } from 'ngx-toastr';
import { GoogleService } from 'src/app/services/google.service';
import { GlobalDefaultService } from 'src/app/services/global-variable.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-tourwizard',
  templateUrl: './tourwizard.component.html',
  styleUrls: ['./tourwizard.component.css']
})
export class TourwizardComponent implements OnInit {
  @Input() sighseeing: Tours = new Tours();
  @Input() Date: string;
  @Input() sequence: any;
  @Output() valueChange = new EventEmitter();
  sTicketType: TicketType = new TicketType();
  RateType: string;
  ID: Number;
  Name: string;
  Location: string = "";
  TourType: string = "";
  arrDates: DateRange[] = [];
  arrDate: Dates = new Dates();
  arrSlots: Slots[] = [];
  arrTicketType: TicketType[] = [];
  objGlobal: GlobalDefaultService;
  Total: number = 0.00;
  arrSelected: TicketType = new TicketType();
  arrPax: PaxTyp[] = [];
  PaxType: string[];
  /* Show Hide Wizard*/
  selRateType: boolean = true;
  selDate: boolean = false;
  selSlot: boolean = false;
  selBook: boolean = false;
  showRate: boolean = false;

  SlotID: number;
  SlotName: string;
  TicketID: number;
  TicketType: number;
  tourType: TourType[] = [];
  arrTourType: TourTypes[] = [];
  SlotDate: string;
  Cart: any = [];
  InventoryType: String;
  next: boolean = true;
  nextDate: string;
  LastDate: string;
  TotalPax: number;
  LastAct: string = ""
  isFullDay: boolean = true;
  arrPickupLocation: any[];
  arrDropLocation: any[];
  PickupID: string = "";
  PickupName: string = "";
  DropID: string = "";
  DropName: string = "";
  constructor(
    private tourService: ToursService,
    private router: Router,
    private sessionService: SessionService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
    private ngxService: NgxUiLoaderService,
  ) {

  }
  ngOnInit() {
    this.arrPickupLocation = [];
    this.arrDropLocation = [];
    debugger;
    if (this.sighseeing != undefined) {
      this.SlotDate = this.Date;
      this.tourService.getTourtype(this.sighseeing.ActivityID, this.Date).subscribe(
        (res: TourType[]) => {
          this.tourType = res;
        });
    }
    else {
      this.setWizard("Type");
    }
    this.cd.detectChanges();
    this.ID = this.sighseeing.ActivityID;
    this.Name = name;
  }

  getRateDate(Type: string) {
    this.ngxService.start();
    this.RateType = Type;
    this.tourService.getDateRange({ Ratetype: this.RateType, ID: this.sighseeing.ActivityID, Date: this.Date, next: this.next }).subscribe(
      (res: Dates) => {
        this.ngxService.stop();
        this.setWizard("Date");
        this.arrDate = res;
        this.arrDates = this.arrDate.arrDates;
        this.nextDate = this.arrDates[0].fDate;
        this.Date = this.nextDate;
        this.LastDate = this.arrDates[this.arrDate.arrDates.length - 1].fDate;
      });
  }

  changeDate(Date: string, nxt: boolean) {
    debugger
    this.next = nxt;
    if (nxt)
      this.Date = this.LastDate
    else
      this.Date = this.nextDate;
    
         this.getRateDate(this.RateType);
  }


  getPickupDrop(event: any, travelType: string) {
    debugger
    let value = event.target.value;
    if (value.length < 3 && value == "" && travelType == "") {
      this.PickupID == ""
      return;
    }
    if (travelType == "D" && this.PickupID == "") {
      this.toastr.error("Please select Pickup location First");
      return;
    }
    this.tourService.getAllPickupDrop(value, this.ID, this.RateType, travelType, this.Date, this.PickupID).subscribe((res: any) => {
      if (res.retCode == undefined) {
        switch (travelType) {
          case 'P':
            this.arrPickupLocation = res;
            break;
          case 'D':
            this.arrDropLocation = res;
            break;
        }
        this.cd.detectChanges();
      }
    })
  }

  onchangePickup(value: string, type: string) {
    try {
      if (value != "" && type == "P")
        this.PickupID = this.arrPickupLocation.find(d => d.value == value).id;
      if (value != "" && type == "D")
        this.DropID = this.arrDropLocation.find(d => d.value == value).id;
    } catch (e) { }
  }

  setSlotView() {
    try {
      if (this.PickupID == "") {
        this.toastr.warning("Please select Pickup Location");
        return;
      }
      else if (this.DropID == "") {
        this.toastr.warning("Please select Drop Location");
        return;
      }
      this.getSlot(this.Date);
    } catch (e) { console.log(e) }
  }

  setDate(Date:string){
    try{
        this.Date = Date;
        if (this.RateType =="TKT") {
          this.getSlot(this.Date)
        }
    }catch(e){}
  }

  getSlot(Date: string) {
    debugger;
    var PickupDrop = new Array();
    if (this.RateType != "TKT") {
      PickupDrop.push(this.PickupID);
      PickupDrop.push(this.DropID);
    }
    else {
      PickupDrop = [];
    }
  
    this.SlotDate = Date;
    this.SlotName = "";
    this.ngxService.start();
    this.tourService.getSlots({ Type: this.RateType, ID: this.sighseeing.ActivityID, Date: Date, PickupDrop: PickupDrop }).subscribe(
      (res: any) => {
        this.ngxService.stop();
        if (res.retCode == undefined) {
          this.setWizard("Slot");
          this.arrSlots = res;
          console.log(res);
          this.arrSlots.forEach(slot => {
            if (slot.SlotsStartTime == "00:00") {
              this.showRate = true;
              this.getSlotRate(slot.ID, slot.SlotsStartTime);
            }
            else if (slot.SlotsStartTime == "") {
              this.showRate = true;
              this.getSlotRate(slot.ID, slot.SlotsStartTime);
            }
            else {
              this.isFullDay = false;
            }
          })
        }
        else {
          if (res.retCode == 0)
            this.toastr.error("tickets not available on this dates", " We are Sorry!!")
        }
      });
  }

  getSlotRate(SlotID: number, slotName: string) {
    debugger;
    this.SlotID = SlotID;
    this.SlotName = slotName
    this.arrPax = [];
    this.PaxType = [];
    this.ngxService.start();
    this.tourService.getSlotRate({ Type: this.RateType, ID: this.sighseeing.ActivityID, SlotID: SlotID, Date: this.Date }).subscribe(
      (res: TicketType[]) => {
        this.ngxService.stop();
        console.log(res);
        this.showRate = true;
        this.arrTicketType = res;
        this.arrTicketType.forEach(arrTicket => {
          if(arrTicket.nonRefundable != true)
             arrTicket.arrCancellationPolicy=this.tourService.getCancellations(arrTicket.arrCancellationPolicy);
          arrTicket.arrInclusion = arrTicket.Inclusions.substring(0, arrTicket.Inclusions.length - 1).split(";");
          arrTicket.arrExclusion = arrTicket.Exclusions.substring(0, arrTicket.Exclusions.length - 1).split(";");
          arrTicket.arrayPax.forEach(Pax => {
            if (this.arrPax.filter(p=>p.Pax_Type== Pax.Pax_Type).length ==0)
                this.arrPax.push(Pax);
            if (this.PaxType.indexOf(Pax.Pax_Type) == -1)
              this.PaxType.push(Pax.Pax_Type)
          });
        });
      });
      this.cd.detectChanges();
  }
  GetInfo(ticket:string,info:string){
    try{
       var arrTicket=   this.arrTicketType.find(d=>d.Ticket ==ticket );
       if(info =="Cancel"){
        arrTicket.showInfo = false;
        if(arrTicket.showCancel){
          arrTicket.showCancel = false;
         }else{
          arrTicket.showCancel = true;
         }
       }else{
        arrTicket.showCancel = false;
        if(arrTicket.showInfo){
          arrTicket.showInfo = false;
         }else{
          arrTicket.showInfo = true;
         }
       }
      
       this.cd.detectChanges();
    }catch(ex){}
  }


  setTicketRate(Ticket: string) {
    try {

      this.Total = 0.00;
      this.ngxService.start();
      this.arrTicketType.forEach(arrTicket => {
        if (arrTicket.Ticket == Ticket) {
          arrTicket.showCancel = false;
          arrTicket.showInfo = false;
          this.sTicketType = arrTicket;
          this.valueChange.emit(this.sTicketType)
          if (arrTicket.arrCancellationPolicy.length != 0)
            arrTicket.arrCancellationPolicy = this.tourService.getCancellations(arrTicket.arrCancellationPolicy);
          this.arrSelected = arrTicket;
          this.arrSelected.Total = 0.00;
          this.InventoryType = arrTicket.InventoryType
          arrTicket.arrayPax.forEach(Pax => {
            if (Pax.Pax_Type == "adult") {
              Pax.Count = 1;
              Pax.Total = parseFloat(Pax.Rate);
              this.arrSelected.Total += Pax.Total;
            }
            else {
              Pax.Count = 0;
            }
          });
          this.setCart();
          this.ngxService.stop();
        }
      });
      this.setWizard("Book");
    } catch (ex) { }

  }
  GetToal(PaxType: string): void {
    debugger
    this.TotalPax = 0;
    this.arrSelected.Total = 0.00;
    //this.ngxService.start();
    this.arrSelected.arrayPax.forEach(Pax => {
      // this.ngxService.stop();
      this.TotalPax += Pax.Count;
      if (PaxType == Pax.Pax_Type && this.arrSelected.available >= this.TotalPax) {
        Pax.Total = parseFloat(Pax.Rate) * Pax.Count;

      }
      else if (PaxType == Pax.Pax_Type) {
        Pax.Count = Pax.Count - 1;
        Pax.Total = parseFloat(Pax.Rate) * Pax.Count;
        this.toastr.error("tickets not available", " We are Sorry!!")
      }
      if (Pax.Total != 0 && Pax.Total != undefined)
        this.arrSelected.Total += Pax.Total;
    });
    this.setCart();
  }

  setCart(): void {
    debugger;
    this.Cart = {
      ID: this.sighseeing.ActivityID,
      SlotID: this.SlotID,
      Slot: this.SlotName,
      sDate: this.SlotDate,
      TicketID: this.arrSelected.TicketID,
      TicketType: this.RateType,
      Type: 'Tour',
      ProductName: this.sighseeing.Name,
      image: this.sighseeing.ListImage.Url,
      PaxRate: this.arrSelected,
      Inventory: this.InventoryType,
    };

  }


  setWizard(Type: string): void {
    switch (Type) {
      case "Type":
        this.selDate = false;
        this.selSlot = false;
        this.selBook = false;
        this.selRateType = true;
        this.showRate = false;
        this.arrTicketType = [];
        this.arrPax = [];
        this.Total = 0.00;
        break;
      case "Date":
        this.selDate = true;
        this.selSlot = false;
        this.selBook = false;
        this.selRateType = false;
        this.showRate = false;
        break;
      case "Slot":
        if (this.RateType != "SIC" || this.PickupID != "") {
          this.selDate = false;
          this.selSlot = true;
          this.selBook = false;
          this.selRateType = false;
        }
        break;
      case "Book":
        this.selDate = false;
        this.selSlot = false;
        this.selBook = true;
        this.selRateType = false;
        break;
    }
  }
}
