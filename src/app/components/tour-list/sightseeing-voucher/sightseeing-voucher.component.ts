import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';

@Component({
  selector: 'app-sightseeing-voucher',
  templateUrl: './sightseeing-voucher.component.html',
  styleUrls: ['./sightseeing-voucher.component.css']
})
export class SightseeingVoucherComponent implements OnInit {
  @Input() ReservationID: string;
  @Input() Individual: boolean;
  @Output() bookingAction = new EventEmitter();
  arrTicket: any[] = [];
  constructor(
    public tourService: ToursService,
  ) { }
  public scale = 0.6;
  ngOnInit() {
    this.arrTicket = [];
    //this.ResrvationID = "321192";
    this.tourService.getTickets(this.ReservationID, this.Individual).subscribe((res: any) => {
      debugger;
      if (res.retCode == "1") {
        this.arrTicket = res.arrTickets;
        console.log(this.arrTicket);
        this.bookingAction.emit("Voucher");
      }
    });
  }

}
