import { Component, OnInit, Input } from '@angular/core';
import { ReviewService } from 'src/app/services/review.service';
import { MatDialog } from '@angular/material';
import { Review } from 'src/app/modals/reviews/review.model';

@Component({
  selector: 'app-reviewratings',
  templateUrl: './reviewratings.component.html',
  styleUrls: ['./reviewratings.component.css']
})
export class ReviewratingsComponent implements OnInit {
  @Input()  ProductType:string;
  @Input()  ProdcutID : number;
  arrReview:Review[];
  constructor(private reviewservice: ReviewService,
    ) { }

  ngOnInit() {
   this.arrReview=[];
    this.reviewservice.GetReviews(this.ProdcutID,this.ProductType).subscribe((res: any) => {
      if( res.retCode ==1)
      {
         this.arrReview =  res.arrReviews;
      }
    });
  }

}
