import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewratingsComponent } from './reviewratings.component';

describe('ReviewratingsComponent', () => {
  let component: ReviewratingsComponent;
  let fixture: ComponentFixture<ReviewratingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewratingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewratingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
