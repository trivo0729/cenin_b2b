import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Review } from 'src/app/modals/reviews/review.model';
import { ReviewService } from 'src/app/services/review.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddReviewsComponent } from '../add-reviews/add-reviews.component';
declare function Modal(): any
@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsComponent implements OnInit {
  @Input() ProductType: string;
  @Input() ProdcutID: number
  arrReview: Review[];
  constructor(private reviewservice: ReviewService, public dialog: MatDialog,
    private cd: ChangeDetectorRef, 
    ) {
  }
  ngOnInit() {
    //Modal();
    debugger
    this.arrReview = [];
    this.loadreview();
    // setInterval(()=>{
    //   this.loadreview();
    // },2000)

  }

  openRiviewDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      ProdcutID: this.ProdcutID,ProductType: this.ProductType
  };

    const dialogRef = this.dialog.open(
      AddReviewsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.ProdcutID = dialogConfig.data.ProdcutID;
        this.ProductType = dialogConfig.data.ProductType;
        this.loadreview;
      });
  }

  loadreview() {
    debugger
    this.reviewservice.GetReviews(this.ProdcutID, this.ProductType).subscribe((res: any) => {
      console.log(res)
      if (res.retCode == 1) {
        this.arrReview = res.arrReviews;
        this.cd.detectChanges();
      }
    });
  }
}
