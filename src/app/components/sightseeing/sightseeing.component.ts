import { Component, OnInit } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { Observable } from 'rxjs';
import { TourTypes } from 'src/app/modals/tours.modals';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { login } from 'src/app/modals/user.model';
import { AlertService } from 'src/app/services/alert.service';
import { objTour } from 'src/app/modals/tours.modals';
import * as moment from 'moment';
@Component({
  selector: 'app-sightseeing',
  templateUrl: './sightseeing.component.html',
  styleUrls: ['./sightseeing.component.css']
})
export class SightseeingComponent implements OnInit {
  objTour: objTour;
  arrPopulars: TourTypes[];
  cityCtrl = new FormControl();

  constructor(private tourService: ToursService,
    private router: Router, private toastr: ToastrService,
    public alert: AlertService) { }

  ngOnInit() {
    this.tourService.getPopularTours().subscribe((res: any) => {
      this.arrPopulars = res.arrTourTypes;
    });
    this.objTour = new objTour();
    this.objTour.Location = '';
    this.objTour.LocationName = "";
    this.objTour.TourType = "";
    var date = new Date();
    this.objTour.Date = moment(date, 'DD-MM-YYYY');
    this.objTour.TourType = '';
  }
  SignIn() {
    debugger;
    login();
  }
}
