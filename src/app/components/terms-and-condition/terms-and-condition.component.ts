import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.component.html',
  styleUrls: ['./terms-and-condition.component.css']
})
export class TermsAndConditionComponent implements OnInit {
  AdminCode:string;
  constructor(
    public objGlobal: ApiUrlService,
  ) { 

  }
  ngOnInit() {
    this.AdminCode = this.objGlobal.Code;
  }

}
