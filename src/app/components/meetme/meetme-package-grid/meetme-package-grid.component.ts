import { Component, OnInit } from '@angular/core';
import { Package, Themes, PackageTheme } from 'src/app/modals/packages/package.modal';
import { PackageService } from 'src/app/services/packages/package.service';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { stringify } from 'querystring';
import { arrUserDetails } from 'src/app/modals/tours.modals';

@Component({
  selector: 'app-meetme-package-grid',
  templateUrl: './meetme-package-grid.component.html',
  styleUrls: ['./meetme-package-grid.component.css']
})
export class MeetmePackageGridComponent implements OnInit {
  Package: Package[];
  TempPackage: Package[];
  minimum: Array<number>;
  count: number = 0;
  userdata: any;
  PackageTheme: PackageTheme[]
  Themes: Themes[];
  constructor(private packageService: PackageService,
    private userService: UserService,
    private ServiceUrl: ApiUrlService ,
    private cookie : CommonCookieService) { 
  }

  ngOnInit() {
    this.Package = [];
    this.Themes = [];
    this.TempPackage = [];
    this.PackageTheme = [];
    this.packageService.getPackage(this.getUserdata()).subscribe((res: any) => {
      this.TempPackage = res;
      this.Package = res;
      this.count = this.Package.length;
      this.SetMinimumPrice();
      this.getThemes(this.TempPackage);
    });
  }

  SetMinimumPrice() {
    try {
      this.Package = [];
      this.TempPackage.forEach(arrPackage => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (this.minimum.length !== 0)
            arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
          else
            arrPackage.MinPrice = 0;
        }
        else {
          arrPackage.MinPrice = 0;
        }
        this.Package.push(arrPackage);
      });
    } catch (e) { console.log(e.message) }


  }
  
  getUserdata() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.userdata = {
            userName: data.LoginDetail.Email,
            password: data.LoginDetail.Password,
            parentID: this.ServiceUrl.AdminID
          }
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.userdata = {
            userName: this.ServiceUrl.UserName,
            password: this.ServiceUrl.Password,
            parentID: this.ServiceUrl.AdminID
          }
        }
      }
    }
    else {
      this.userdata = {
        userName: this.ServiceUrl.UserName,
        password: this.ServiceUrl.Password,
        parentID: this.ServiceUrl.AdminID
      }
    }
    return this.userdata;
  }

  getThemes(Packages: any[]) {
    Packages.forEach(arrPackage => {
      arrPackage.Themes.forEach(theme => {
        if (this.Themes.filter(d => d.Name == theme).length == 0)
          this.Themes.push({ Name: theme, Count: 1, checked: false });
        else {
          this.Themes.filter(d => d.Name == theme).forEach(r => {
            r.Count = this.Themes.filter(d => d.Name == theme).length + 1;
          });
        }
      });
    });
    this.setPackageWithTheme();
  }

  setPackageWithTheme() {
    this.PackageTheme = [];
    this.Themes.forEach(theme => {
      this.Package = [];
      this.TempPackage.forEach(arrPackage => {
        if (arrPackage.Themes.filter(d => d == theme.Name).length != 0)
          if (this.Package.indexOf(arrPackage) == -1)
            this.Package.push(arrPackage)
      });
      this.PackageTheme.push({
        Count: theme.Count,
        Name: theme.Name,
        checked: theme.checked,
        PackageCount: this.Package.length,
        Package: this.Package = this.Package.slice(0, 6),
      })
    });
  }

  showTabs(theme: string) {
    this.Package = [];
    /* Theme */
    this.TempPackage.forEach(arrPackage => {
      if (arrPackage.Themes.filter(d => d == theme).length != 0)
        if (this.Package.indexOf(arrPackage) == -1)
          this.Package.push(arrPackage)
    });
    this.count = this.Package.length;
    this.Package = this.Package.slice(0, 6)
  }



}
