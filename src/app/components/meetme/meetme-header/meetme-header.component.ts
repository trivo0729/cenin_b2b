import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { loginDetails } from 'src/app/modals/user.model';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { ApiUrlService } from 'src/app/services/api-url.service';
declare function allScript(): any;
@Component({
  selector: 'app-meetme-header',
  templateUrl: './meetme-header.component.html',
  styleUrls: ['./meetme-header.component.css']
})
export class MeetmeHeaderComponent implements OnInit {
  @Output() loginDialog = new EventEmitter();
  @Output() RegtrDialog = new EventEmitter();
  @Output() _logout = new EventEmitter();
  @Input() login : any;
  CustomerName: string = null;
  CustomerEmail: string = null;
  constructor( private userService: UserService,
    public objGlobal : ApiUrlService) { 

    }

  ngOnInit() {
    this.login = this.login
    debugger;
    console.log(this.login)
  }
  onSigninDialog() {
    this.loginDialog.emit();
  }

  onRegisterDialog() {
    this.RegtrDialog.emit();
  }

  onSignout() {
    this._logout.emit();
  }



}
