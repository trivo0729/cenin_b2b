import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetmeHeaderComponent } from './meetme-header.component';

describe('MeetmeHeaderComponent', () => {
  let component: MeetmeHeaderComponent;
  let fixture: ComponentFixture<MeetmeHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetmeHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetmeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
