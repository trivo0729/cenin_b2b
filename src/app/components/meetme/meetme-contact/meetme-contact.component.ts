import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Contact } from 'src/app/modals/contact.modal';

declare function MapDesign(lat: any, lng: any, logo: string, address: string): any;
@Component({
  selector: 'app-meetme-contact',
  templateUrl: './meetme-contact.component.html',
  styleUrls: ['./meetme-contact.component.css']
})
export class MeetmeContactComponent implements OnInit {
 
  contactfrm : FormGroup;
  submitted = false;
  contact : Contact;

  constructor(
    private formBuilder : FormBuilder,
    private ContactService : ContactService ,
    public objGlobalService : ApiUrlService,
    
  ) {


   }

  ngOnInit() {
MapDesign(this.objGlobalService.lat ,this.objGlobalService.lng , this.objGlobalService.logo , this.objGlobalService.address);
 this.contact = new Contact();
 this.contactfrm = this.formBuilder.group({
   Firstname : ['', Validators.required],
   Lastname : ['',Validators.required],
   Email : ['',Validators.required],
   Phone : ['' , Validators.required],
   Message : ['',Validators.required]
 });
  } 

  get f() { return this.contactfrm.controls ;}

  public CheckValidation(): void {
   this.submitted = true ; 
   // stop here if form is invalid 
 if(this.contactfrm.invalid){
  return ;
 } else
 {
   this.SendContactinfo();
 }

  }

  
  public SendContactinfo(){
    debugger;
   this.ContactService.sendContactinfo(this.contact).subscribe((res : any) => {
   if(res.retCode == 1){
    alert("Your request sent.We will get in touch with you soon.");
   this.submitted = false ;
   this.contactfrm.reset();
   }
   })
  }


}
