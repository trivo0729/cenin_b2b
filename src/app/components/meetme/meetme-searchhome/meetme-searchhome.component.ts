import { Component, OnInit } from '@angular/core';
import { PackageService } from 'src/app/services/packages/package.service';
import { ToursService } from 'src/app/services/tours.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuService } from 'src/app/services/commons/menu.service';
import { AlertService } from 'src/app/services/alert.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Traveller, objTraveller, Childs } from 'src/app/modals/hotel/search.modal';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { objTour, TourTypes } from 'src/app/modals/tours.modals';
import * as moment from 'moment';

@Component({
  selector: 'app-meetme-searchhome',
  templateUrl: './meetme-searchhome.component.html',
  styleUrls: ['./meetme-searchhome.component.css']
})
export class MeetmeSearchhomeComponent implements OnInit {
  UsetId : number ;
  Traveller: Traveller[];
  arrPopulars: TourTypes[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];
  menu: Menulist[];
  _homeTabs: Menulist[];
  objTour: objTour;
  constructor(
    private packageService : PackageService ,
    private userService: UserService,
    private router: Router, private toastr: ToastrService,
    private menuService: MenuService,
    public alert: AlertService,
    private cookie: CommonCookieService,
    public objGlobalService: ApiUrlService,
   

  ) {


   }

  ngOnInit() {
    // this.objTour = new objTour;
    // this.objTour.Location = '';
    // this.objTour.Date = {
    //   Date: moment(this.objTour.Date, 'DD-MM-YYYY')
    // }
    // this.objTour.TourType = '';
    // this.tourService.getPopularTours().subscribe((res: any) => {
    //   this.arrPopulars = res.arrTourTypes;
    //   console.log(res);
    // });
    // For Tabs 
    this.getTabs();

  } 

  getTabs() {
    this._homeTabs = [];
    if (this.cookie.checkcookie('menu')) {
      let s = this.cookie.checkcookie('menu');
      let data = JSON.parse(this.cookie.getcookie('menu'));
      if (data.UserId === this.getUserId()) {
        this.menu = data.Menu;
        this.generateTabs();
      }
      if (data.UserId !== this.getUserId()) {
        this.cookie.deletecookie('menu');
      }
    }
    if (!(this.cookie.checkcookie('menu'))) {
      this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
        this.menu = res;
        const menudetails = { UserId: this.getUserId(), Menu: this.menu }
        this.cookie.setcookie('menu', JSON.stringify(menudetails), 1);
        this.generateTabs();
      }, error => {
        console.log(error);
      });
    }
  }

  generateTabs() {
    this.menu.forEach(menu => {
      if (menu.Name === 'Hotel' || menu.Name === 'Sightseeing' || menu.Name === 'Package')
        this._homeTabs.push(menu);
    });
  } 
 
  getUserId() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.objGlobalService.UserId;
        }
      }
    }
    else {
      this.UsetId = this.objGlobalService.UserId;
    }
    return this.UsetId;
  }




}
