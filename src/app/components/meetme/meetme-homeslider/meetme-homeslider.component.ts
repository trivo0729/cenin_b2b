import { Component, OnInit } from '@angular/core';

declare function allScript(): any;
declare function revslidermeetme(): any;
@Component({
  selector: 'app-meetme-homeslider',
  templateUrl: './meetme-homeslider.component.html',
  styleUrls: ['./meetme-homeslider.component.css']
})
export class MeetmeHomesliderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  
    revslidermeetme();
    allScript();
  }

}
