import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { DatePipe } from '@angular/common';
import { Product, Cart } from 'src/app/modals/common/cart.model';
import { SessionService } from 'src/app/services/session.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';
import { MatDialog } from '@angular/material';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { UserComponent } from '../user/user.component';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { Router } from '@angular/router';
import { DialogService } from 'src/app/services/commons/dialog.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']

})
export class ShoppingCartComponent implements OnInit {
  today: any = new Date();
  constructor(private shopCartService: ShoppingCartService,
    private datePipe: DatePipe,//= new DatePipe("en-US"),
    private sessionService: SessionService,
    private cd: ChangeDetectorRef,
    private toster: ToastrService,
    private userService: UserService,
    public dialog: MatDialog,
    private cookie: CommonCookieService,
    public router: Router,
    private dialogService: DialogService,
  ) { }
  products: any[];
  arrProducts: Product[];
  arrCart: Cart = new Cart();
  Total: any;
  Currency: string
  ngOnInit() {
    try {
      setInterval(() => {
        this.take();
      }, 1000);
    } catch (e) { }
  }
  remove(ProductID: number): void {
    debugger
    this.dialogService.openConfirmDialog('Are you sure want to Remove this Sightseeing ?')
      .afterClosed().subscribe(response => {
        if (response !== undefined) {
          if (response === true) {
            this.shopCartService.remove(this.arrCart.CartID, ProductID).subscribe((res: any) => {
              if (res.retCode == 1) {
                this.take();
                this.toster.success("Removed From Cart");
                this.cd.detectChanges();
                var url = window.location.href;
                if( this.products.length ==0 && url.includes("checkout"))
                  this.router.navigate["home"];
                  if(url.includes("checkout")){
                    window.location.reload();
                  }
              } else {
                this.toster.error("Please try later");
              }
            });
          }
        }
      });
  }
  take(): void {
    try {
      var arrDetails = this.sessionService.get("Cart");
      if (arrDetails != null)
        this.arrCart = arrDetails;
      if (this.arrCart.CartID != undefined) {
        this.shopCartService.get(this.arrCart.CartID).subscribe((res: any) => {
          if (res.retCode == 1) {
            this.Total = '0';
            this.products = [];
            this.Currency = '';
            this.arrProducts = res.CartProdDetailList;
            var i = 0, total = 0;
            this.arrProducts.forEach(arrProduct => {
              this.today = new Date();
              if (arrProduct.CartStatus)/*Check Expire Time */ {
                this.products.push(JSON.parse(arrProduct.ProductDetails));
                try {
                  var sExp = this.changeDate(arrProduct.ExpiryTime)//.replace(" PM",'').replace(" AM","");
                  // var sExp ="07/15/2019 11:58 AM" ;
                  var Expire = this.datePipe.transform(sExp, 'MM/dd/yyyy h:mm a')
                  var arrTimeDetails = this.getDataDiff(new Date(this.today), new Date(Expire));
                  this.products[i].ExpiryTime = arrTimeDetails.minute + ":" + arrTimeDetails.second
                } catch (e) { console.log(e.message) }
                this.products[i].ProductID = arrProduct.ProductID;
                total += parseFloat(this.products[i].PaxRate.Total);
                this.Currency = this.products[i].PaxRate.Currency;
                i++;
              }
            });
            this.Total = total;
          } else {
            this.sessionService.clear("Cart")
          }
        });
      }
    } catch (e) { }
  }
  getDataDiff(startDate: Date, endDate: Date) {
    try {
      var diff = endDate.getTime() - startDate.getTime();
      var days = Math.floor(diff / (60 * 60 * 24 * 1000));
      var hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
      var minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
      var seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
      return { day: days, hour: hours, minute: minutes, second: seconds };
    } catch (e) { console.log(e.message) }
  }
  changeDate(date: string) {
    var arrDate = date.split('-');
    date = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2]
    return date
  }

  CheckOut() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = this.cookie.getcookie('login');
      if (data !== null) {
        window.location.href = "/checkout";
      }
    }
    else {
      this.openDialog();
    }
  }                    


  public login: Userdetails = new Userdetails();

  openDialog() {
    debugger;
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.GetLoginDetails();
    });
  }

  GetLoginDetails() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie('login')) {
      let userdata = JSON.parse(this.cookie.getcookie('login'));
      if (userdata !== null) {
        this.login = userdata;
      }
    else
        this.login.retCode = 0;
      
    }
  }

}
