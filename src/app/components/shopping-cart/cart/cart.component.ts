import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { UserService } from 'src/app/services/user.service';
import { MatDialog } from '@angular/material';
import { UserComponent } from '../../user/user.component';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  Userdetails: Userdetails = new Userdetails();
  constructor(private shopCartService:ShoppingCartService ,
    private userService : UserService ,
    public dialog : MatDialog,
    private cookie : CommonCookieService
    ) { 


  }
  products:any[];
  Total:string;
  ngOnInit() {
    //this.products = this.shopCartService.get();
    this.Total= this.shopCartService.setTotal();
  }
  remove(index:number):void{
  //  this.products =  this.shopCartService.remove(index);
    this.Total= this.shopCartService.setTotal();
  }

  CheckOut(){
  debugger;
  if (this.cookie.checkcookie('login')) {
    let data = this.cookie.getcookie('login');
    if (data ! == null) {
      window.location.href = "/checkout";
    }
  }  else 
   {
     this.openDialog();
   }

  }
 
  public login: Userdetails = new Userdetails();

  openDialog() {
    debugger;
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.GetLoginDetails();
    });
  }

  GetLoginDetails(){
    debugger;
     this.login = new Userdetails();
     if (this.cookie.getcookie('login')) {
       let userdata = JSON.parse(this.cookie.getcookie('login')) ;
      if (userdata !== null) {
        this.login = userdata;
      } 
      if (userdata === null) {
        this.login.retCode = 0 ;
      }
     }
  }

}
