import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from 'src/app/services/session.service';
import { Cart, Product } from 'src/app/modals/common/cart.model';
import { UserService } from 'src/app/services/user.service';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { UserComponent } from '../../user/user.component';
import { MatDialog } from '@angular/material';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AlertService } from 'src/app/services/alert.service';
declare function closeNav(i:any): any;
@Component({
  selector: 'app-add-cart',
  templateUrl: './add-cart.component.html',
  styleUrls: ['./add-cart.component.css']
  // template:`<button (click)="addToCart(product)">ADD TO    CART</button>`,
  //   inputs: ['product']
})
export class AddCartComponent implements OnInit {
  Userdetails: Userdetails = new Userdetails();
  @Input() Product: any;
  @Input() sequence :any
  arrCart:Cart = new Cart();
  CartProduct: Product = new Product();
  arrProduct: Product[];
  logedin : boolean;
  constructor(
    private sessionService:SessionService,
    private _shoppingCartService: ShoppingCartService,
    private router:Router,
    private toastr: ToastrService,
    private userService : UserService,
    public dialog : MatDialog,
    private cookie :  CommonCookieService,
    private ngxService: NgxUiLoaderService,
    public alert: AlertService,
 ) {}
 ngOnInit() { 
   this.logedin = false;
  }
addToCart() {
  debugger
  this.ngxService.start();
  this.arrCart = new Cart();
  this.arrProduct=[];
  var arrDetails = this.sessionService.get("Cart");
  if(arrDetails!= null)
      this.arrCart  = arrDetails;
  this.CartProduct = new Product();
  if(this.arrCart.CartID != undefined)
  this.CartProduct.CartID = this.arrCart.CartID;
  this.CartProduct.ProductDetails = JSON.stringify(this.Product);
  this.CartProduct.ProductID =  this.Product.ID;
  this.CartProduct.ProductType = this.Product.Type;
  this.arrProduct.push(this.CartProduct);
  this._shoppingCartService.addCart(this.arrCart,this.arrProduct).subscribe((res: any) => { 
    if(res.retCode == 1)
    {
      this.ngxService.stop();
      this.alert.confirm('Added In your Cart ' + this.Product.ProductName,"success")
      this.arrCart.CartID = res.CartID;
      this.sessionService.set("Cart", JSON.stringify(this.arrCart) )
    }
    else
         this.alert.confirm('Please Try Later' + this.Product.ProductName,"error")
    closeNav(this.sequence);
  });
}
CheckOut()
{
  debugger;
  if (this.cookie.checkcookie('login')) {
    let data = this.cookie.getcookie('login');
    if (data !== null) {
      this.addToCart(); 
      setTimeout(() => {
        window.location.href ="/checkout";
      }, 1000);
      
     }
  } else
 { 
  this.openDialog();
 }
 
}
public login: Userdetails = new Userdetails();
openDialog() {
  debugger;
  this.dialog.closeAll();
  const dialogRef = this.dialog.open(UserComponent);
  dialogRef.afterClosed().subscribe(result => {
    this.GetLoginDetails();
  });
}

GetLoginDetails() {
  this.login = new Userdetails();
  if (this.cookie.checkcookie('login')) {
    let userData = JSON.parse(this.cookie.getcookie('login'));
    if (userData !== null) {
      this.login = userData;
    }
    if (userData === null) {
      this.login.retCode == 0
    }
  } 
}

}
