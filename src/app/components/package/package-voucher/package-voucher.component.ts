import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PackageService } from 'src/app/services/packages/package.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-package-voucher',
  templateUrl: './package-voucher.component.html',
  styleUrls: ['./package-voucher.component.css']
})
export class PackageVoucherComponent implements OnInit {
  Reservation: any;
  AgentDetails: any;
  PackageDetails: any;
  userdetails: any;
  isb2c: boolean;
  BookingId: any;
  public scale = 0.6;
  logo: string;
  Hotel: any;
  Transfer: any[];
  Meal: any[];
  constructor(private packageservice: PackageService,
    private cookie: CommonCookieService,
    public objcommon: ApiUrlService,
    private route: ActivatedRoute,
    private userService: UserService,
    private ngxService: NgxUiLoaderService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.BookingId = this.route.snapshot.queryParamMap.get("ID");
    this.Reservation = new Object();
    this.AgentDetails = new Object();
    this.PackageDetails = new Object();
    this.Hotel = [];
    this.Transfer = [];
    this.getUserdata();
    if (!this.isb2c)
      this.getlogo(this.userdetails.LoginDetail.sid);
    this.ngxService.start();
    this.packageservice.getVoucher(this.BookingId).subscribe((res: any) => {
      this.ngxService.stop();
      debugger
      console.log(JSON.stringify(res));
      if (res.retCode === 1) {
        this.Reservation = res.arrResrvation;
        this.AgentDetails = res.AgentDetails;
        this.PackageDetails = res.PackageDetails;
        if (this.PackageDetails.Category.length > 0) {
          this.getHotels();
          this.getTransfers();
          this.getMeal();
        }
      }
    });
  }

  getUserdata() {
    debugger
    if (this.cookie.checkcookie('login')) {
      let user = JSON.parse(this.cookie.getcookie('login'));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B")
        this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
    this.cd.detectChanges();
  }

  getHotels() {
    debugger
    this.Hotel = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].PackageHotels;
    console.log(JSON.stringify(this.Hotel))
  }

  getTransfers() {
    let Inclusions = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].PackageInclusion.PackageVehicle;
    let Exclusions = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].PackageExclusion.PackageVehicle;

    Inclusions.forEach(inclusion => {
      inclusion.bIncluded = 'Included'
      this.Transfer.push(inclusion)
    });

    Exclusions.forEach(exclusion => {
      exclusion.bIncluded = 'Excluded'
      this.Transfer.push(exclusion)
    });
  }

  getMeal() {
    let Inclusions = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].PackageInclusion.PackageMeal;
    let Exclusions = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].PackageExclusion.PackageMeal;
    Inclusions.forEach(inclusion => {
      inclusion.bIncluded = 'Included'
      this.Meal.push(inclusion)
    });

    Exclusions.forEach(exclusion => {
      exclusion.bIncluded = 'Excluded'
      this.Meal.push(exclusion)
    });
  }

  getlogo(id: any) {
    this.userService.AgentInfo(id).subscribe((res: any) => {
      console.log(res);
      if (res.retCode === 1) {
        this.logo = res.arrUser.CompanyDetails.LogoLink;
        this.cd.detectChanges();
      }
    })
  }

  Download() {
    $("#btn_Voucher_pdf").click();
  }

  Print() {
    window.print();
  }

}
