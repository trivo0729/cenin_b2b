import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {
  AdminCode: any;
  constructor(public objGlobalService: ApiUrlService) { }

  ngOnInit() {
    this.AdminCode = this.objGlobalService.Code;
  }

}
