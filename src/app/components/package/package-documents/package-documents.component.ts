import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Package } from 'src/app/modals/packages/package.modal';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-package-documents',
  templateUrl: './package-documents.component.html',
  styleUrls: ['./package-documents.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PackageDocumentsComponent implements OnInit {
  @Input() Packages: Package;
  @Input() currency: any;
  @Input() validityshow: boolean;
  @Output() exportElement = new EventEmitter();
  minimum: Array<number>;
  AdminCode: any;
  public scale = 0.6;
  login: any;
  logo: any;
  AgentName: any;
  constructor(
    public objGlobalDefault: ApiUrlService,
    private userService: UserService,
    private cookie: CommonCookieService,
    private cd: ChangeDetectorRef,

  ) { }
  ngOnInit() {
    debugger
    this.Packages = new Package();
    this.Packages.Category = [];
    this.Packages.Images = [];
    this.AdminCode = this.objGlobalDefault.Code;
    if (this.cookie.checkcookie("login")) {
      this.login = JSON.parse(this.cookie.getcookie("login"));
      try {
        if (this.login.LoginDetail.UserType === "B2B") {
          this.AgentName = this.login.LoginDetail.AgencyDetail.AgencyName;
          this.getlogo(this.login.LoginDetail.sid);
        }
      } catch (error) {
      }
    }
  }

  getlogo(id: any) {
    this.userService.AgentInfo(id).subscribe((res: any) => {
      debugger
      if (res.retCode === 1) {
        this.logo = res.arrUser.CompanyDetails.LogoLink;
        this.cd.detectChanges();
      }
    })
  }

  _exportImage(element) {
    debugger
    this.exportElement.emit(element);
  }
}
