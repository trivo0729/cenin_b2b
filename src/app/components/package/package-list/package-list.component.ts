import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, NgZone } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PackageService } from 'src/app/services/packages/package.service';
import { Package, Themes, Categories } from 'src/app/modals/packages/package.modal';
import { ActivatedRoute } from '@angular/router';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { UserService } from 'src/app/services/user.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { CurrencyService } from 'src/app/services/commons/currency.service';
declare function setimage(img: string): any;
declare function singleBanner(): any;
declare function Range_Slider(min: number, max: number, currency: string): any;
declare var $: JQueryStatic;
@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.component.html',
  styleUrls: ['./package-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PackageListComponent implements OnInit {
  AdminID: number;
  Type: string;
  City: string;
  img: string;
  ID: any;
  PackageName: string;
  TempPackages: Package[];
  Packages: Package[];
  page: number = 1;
  Themes: Themes[];
  Categories: Categories[];
  PackagePrice: Array<number>;
  min: any;
  max: any;
  selectedValue: string;
  TempPrice: string;
  minimum: Array<number>;
  userdata: any;
  response: boolean;
  currency: string;
  constructor(private packageService: PackageService,
    private router: ActivatedRoute,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
    private zone: NgZone,
    private ServiceUrl: ApiUrlService,
    private userService: UserService,
    private cookie: CommonCookieService,
    private currencyService: CurrencyService,
  ) { this.AdminID = this.ServiceUrl.AdminID; }

  ngOnInit() {
    this.response = false;
    this.Categories = [];
    this.Themes = [];
    this.PackagePrice = [];
    this.Packages = [];
    debugger;
    this.currency = this.currencyService.getCurrency();
    if (this.currency === null)
      this.currency = this.ServiceUrl.Currency;
    this.Type = this.router.snapshot.queryParamMap.get('Type');
    this.City = this.router.snapshot.queryParamMap.get('City');
    this.ID = this.router.snapshot.queryParamMap.get('PackageID');
    if ((this.Type == "" || this.Type == null) && (this.City == "" || this.City == null) && (this.ID == "" || this.ID == null)) {
      this.packageService.getPackage(this.userService.getUserdata()).subscribe((res: Package[]) => {
        this.response = true;
        console.log(res);
        res.forEach(arrPackage => {
          let p = arrPackage.Themes.filter(d => d == "Wildlife")
          if (p.length !== 0)
            this.Packages.push(arrPackage)
        });
        this.TempPackages = this.Packages;
        this.SetMinimumPrice();
        this.getfilterparms();
        this.SetPriceRange();
        this.cd.detectChanges();
        if (this.Packages[0].Images.length != 0)
          this.img = this.Packages[0].Images[0].Url;
        this.SetImages();
      });
    }
    if (this.Type != "" && this.Type != null) {
      this.packageService.SearchPackagesByType(this.userService.getUserdata(), this.Type).subscribe((res: Package[]) => {
        this.response = true;
        this.Packages = res;
        this.TempPackages = res;
        this.SetMinimumPrice();
        this.getfilterparms();
        this.SetPriceRange();
        console.log(this.Packages);
        this.cd.detectChanges();
        if (this.Packages[0].Images.length != 0)
          this.img = this.Packages[0].Images[0].Url;
        this.SetImages();
      });
    }
    if (this.City != "" && this.City != null) {
      this.packageService.SearchPackagesByCity(this.City, this.userService.getUserdata()).subscribe((res: Package[]) => {
        this.response = true;
        this.Packages = res;
        this.TempPackages = res;
        this.SetMinimumPrice();
        this.getfilterparms();
        this.SetPriceRange()
        console.log(this.Packages);
        this.cd.detectChanges();
        if (this.Packages[0].Images.length != 0)
          this.img = this.Packages[0].Images[0].Url;
        this.SetImages();
      });
    }
    if ((this.ID != "" && this.ID != null)) {
      debugger
      this.packageService.getPackageDetail(this.ID).subscribe((res: any) => {
        this.response = true;
        this.Packages = res;
        this.TempPackages = res;
        this.SetMinimumPrice();
        this.getfilterparms();
        this.SetPriceRange();
        console.log(this.Packages);
        this.cd.detectChanges();
        if (this.Packages[0].Images.length != 0)
          this.img = this.Packages[0].Images[0].Url;
        this.SetImages();
      });
    }
  }



  SetImages() {
    setTimeout(() => {
      setimage(this.img);
    }, 20)
  }

  //* Package filterparms Start *//
  getfilterparms() {
    /* Category */
    this.Packages.forEach(arrPackage => {
      arrPackage.Category.forEach(Catg => {
        if (this.Categories.filter(d => d.Name == Catg.Categoryname).length == 0)
          this.Categories.push({ Name: Catg.Categoryname, Count: 1, checked: false });
        else {
          this.Categories.filter(d => d.Name == Catg.Categoryname).forEach(r => {
            r.Count = r.Count + 1;
          });
        }
      });
    });
    console.log(this.Categories);

    /* Theme */
    this.Packages.forEach(arrPackage => {
      arrPackage.Themes.forEach(theme => {
        if (this.Themes.filter(d => d.Name == theme).length == 0)
          this.Themes.push({ Name: theme, Count: 1, checked: false });
        else {
          this.Themes.filter(d => d.Name == theme).forEach(r => {
            r.Count = this.Themes.filter(d => d.Name == theme).length + 1;
          });
        }
      });
    });
  }
  setSinglebanner() {
    singleBanner();
  }

  /* Package filter Start  */
  PackageFilter(value: string, Name: string) {
    debugger;
    this.Packages = [];
    if (value === 'C') {
      this.Categories.filter(d => d.Name === Name).forEach(r => {
        const isChecked = this.Categories.filter(d => d.Name === Name)[0].checked;
        if (isChecked) {
          r.checked = false;
        } else {
          r.checked = true;
        }
      });
    }
    if (value == 'T') {
      this.Themes.filter(d => d.Name == Name).forEach(r => {
        var isChecked = this.Themes.filter(d => d.Name == Name)[0].checked;
        if (isChecked)
          r.checked = false;
        else
          r.checked = true;
      });
    }
    this.SetFilteredData(this.TempPackages)
    if (this.Packages.length == 0) {
      this.Packages = this.TempPackages;
    }
  }
  /* Package filter End */

  SetFilteredData(arrData: any) {
    debugger;
    this.Packages = [];
    arrData.forEach(arrPackage => {
      this.Categories.forEach(r => {
        if (arrPackage.Category.filter(d => d.Categoryname == r.Name).length != 0 && r.checked)
          if (this.Packages.indexOf(arrPackage) == -1)
            this.Packages.push(arrPackage)
      });
    });

    arrData.forEach(arrPackage => {
      this.Themes.forEach(r => {
        if (arrPackage.Themes.filter(d => d == r.Name).length != 0 && r.checked)
          if (this.Packages.indexOf(arrPackage) == -1)
            this.Packages.push(arrPackage)
      });
    });
  }


  SetMinimumPrice() {
    try {
      debugger;
      this.Packages = [];
      this.TempPackages.forEach(arrPackage => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (this.minimum != undefined && this.minimum.length != 0)

            arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
        }
        else {
          arrPackage.MinPrice = 0;
        }

        //this.min = 
        this.Packages.push(arrPackage);
      });
      this.TempPackages = this.Packages;
    } catch (e) { console.log(e.message); this.Packages = this.TempPackages }

    //this.min = this.PackagePrice.reduce((a, b) => Math.min(a, b));
    //.minimum = this.min;
  }



  setPrice() {
    try {
      debugger;
      this.Packages.forEach(arrPackage => {
        if (arrPackage.MinPrice != 0) {
          this.PackagePrice.push(arrPackage.MinPrice);
        }

      });
      this.max = this.PackagePrice.reduce((a, b) => Math.max(a, b)).toFixed(2);
      this.min = this.PackagePrice.reduce((a, b) => Math.min(a, b)).toFixed(2);
      const self = this;
      this.zone.run(() => {
        $('.price_range').on('change', function (data) {
          self.PriceFilter(data);
        });
      });
    } catch (e) { }
  }

  PriceFilter(data: any) {
    try {
      debugger;
      this.TempPrice = data.currentTarget.value.split(';');
      this.min = parseInt(this.TempPrice[0]);
      this.max = parseInt(this.TempPrice[1]);
      this.Packages = [];
      this.TempPackages.forEach(arrPackage => {
        if (this.min <= arrPackage.MinPrice && this.max >= arrPackage.MinPrice) {
          this.Packages.push(arrPackage);
        }
      });
      this.cd.detectChanges();
    } catch (e) { }
  }

  SetPriceRange() {
    try {
      debugger;
      setTimeout(() => {
        this.setPrice();
        Range_Slider(this.min, this.max, this.TempPackages[0].Category[0].sCurrency);
      }, 100);
    } catch (e) { }
  }
}


