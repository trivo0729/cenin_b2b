import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Package } from 'src/app/modals/packages/package.modal';
import { MatDialog } from '@angular/material/dialog';
import { PackageService } from 'src/app/services/packages/package.service'
import { ActivatedRoute, Router } from "@angular/router";
import { DatePipe } from '@angular/common';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { UserComponent } from '../../user/user.component';
import { UserService } from 'src/app/services/user.service';
import { AlertService } from 'src/app/services/alert.service';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { CurrencyService } from 'src/app/services/commons/currency.service';
declare function HotelCarousel(): any;
declare function ChangeStyle(): any;
declare function StickySidebar(): any;
declare function setimage(img: string): any;
declare function set_class(): any;
declare function owl(): any;
@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PackageDetailsComponent implements OnInit {
  public login: Userdetails = new Userdetails();
  ID: string;
  AdminID: number;
  Packages: Package
  img: string = "";
  Images: any[];
  minimum: Array<number>;
  Share: any;
  Bit: any;
  currency: any;

  constructor(
    private packageService: PackageService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private cd: ChangeDetectorRef,
    public userService: UserService,
    public dialog: MatDialog,
    private ServiceUrl: ApiUrlService,
    public objGlobal: ApiUrlService,
    private alert: AlertService,
    private cookie: CommonCookieService,
    private router: Router,
    private currencyService: CurrencyService,
  ) { this.AdminID = this.ServiceUrl.AdminID; }

  ngOnInit() {
    this.Share = { url: "", details: "", img: "", title: "" };
    this.Packages = new Package();
    this.Packages.Category = [];
    this.Packages.arrInclusions = [];
    this.Packages.arrExclusions = [];
    this.Images = [];
    this.ID = this.route.snapshot.queryParamMap.get('ID');
    this.Bit = 1;
    this.currency = this.currencyService.getCurrency();
    if (this.currency === null)
      this.currency = this.ServiceUrl.Currency;
    this.packageService.getPackageDetail(this.ID).subscribe((res: any) => {
      this.Packages = res[0];
      this.Packages.City = this.Packages.City.split("|");
      this.Packages.ValidFrom = this.datePipe.transform(this.Packages.ValidFrom, "dd-MM-yy");
      this.Packages.ValidTo = this.datePipe.transform(this.Packages.ValidTo, "dd-MM-yy");
      console.log(this.Packages);
      if (this.Packages.Images.length != 0) {
        this.img = this.Packages.Images[0].Url;
        this.Share.img = this.img;
        this.Images = this.Packages.Images;
        this.SetImages();
      }
      this.Share.title = this.Packages.PackageName;
      this.Share.details = this.Packages.Description;
      this.Share.url = this.objGlobal.website + "/package-details?ID=" + this.ID;
      this.SetMinimumPrice();
      this.cd.detectChanges();
      StickySidebar();
      console.log(JSON.stringify(this.Packages));
    });
  }
  ngAfterViewInit() {
    var elem = document.getElementsByClassName('sb-icon')[0];
    elem.className = "sb-icon icon-facebook";
  }

  SetMinimumPrice() {
    debugger;
    this.minimum = [];
    if (this.Packages.Category.length != 0) {
      if (this.Packages.Category[0].Double != 0) {
        this.minimum.push(this.Packages.Category[0].Double);
      }
      if (this.Packages.Category[0].Single != 0) {
        this.minimum.push(this.Packages.Category[0].Single);
      }
      if (this.Packages.Category[0].Triple != 0) {
        this.minimum.push(this.Packages.Category[0].Triple);
      }
      if (this.Packages.Category[0].Quad != 0) {
        this.minimum.push(this.Packages.Category[0].Quad);
      }
      if (this.Packages.Category[0].Quint != 0) {
        this.minimum.push(this.Packages.Category[0].Quint);
      }
      this.Packages.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
    }
    else {
      this.Packages.MinPrice = 0;
    }
  }

  SetImages() {
    setTimeout(() => {
      owl();
      setimage(this.img);
      set_class();
    }, 20);
    setInterval(() => {
      set_class();
    }, 100);
    setInterval(() => {
      HotelCarousel();
    }, 5000);
    setInterval(() => {
      ChangeStyle();
    }, 1000);
  }

  public Booking(Id: number) {
    debugger;
    /*Check User logedIn */
    this.login = new Userdetails();
    if (this.cookie.checkcookie('login')) {
      let userData = JSON.parse(this.cookie.getcookie('login'));
      if (userData !== null) {
        window.location.href = 'package-booking?ID=' + Id;
      }
    }

    else {
      const dialogRef = this.dialog.open(UserComponent);
      dialogRef.afterClosed().subscribe(result => {
        console.log(result);

      });
    }

  }

  public SendEnquiry(Id: number) {
    debugger;
    //window.location.href = 'package-booking?ID=' + Id + '&bit='+this.Bit;
    this.router.navigate(['/package-booking'], { queryParams: { ID: Id, bit: this.Bit } });
  }

  GetLoginDetails() {
    debugger;
    this.login = new Userdetails();

    if (this.cookie.checkcookie('login')) {
      let userData = JSON.parse(this.cookie.getcookie('login'));
      if (userData !== null) {
        this.login = userData;
      }
    }
    else
      this.login.retCode == 0
  }

  downloadpdf() {
    $("#box_style_pdf").show();
    $("#btn_pdf").click();
    setTimeout(() => {
      $("#box_style_pdf").hide();
    }, 2000);
  }


  _export(element: HTMLElement) {

  }
}
