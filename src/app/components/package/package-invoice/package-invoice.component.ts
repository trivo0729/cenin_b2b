import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PackageService } from 'src/app/services/packages/package.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-package-invoice',
  templateUrl: './package-invoice.component.html',
  styleUrls: ['./package-invoice.component.css']
})
export class PackageInvoiceComponent implements OnInit {
  Reservation: any;
  AgentDetails: any;
  PackageDetails: any;
  TaxPerAmnt: any[];
  userdetails: any;
  isb2c: boolean;
  BookingId: any;
  public scale = 0.6;
  logo: string;
  Total: any;
  SubTotal: any;
  Currency: string;
  Extrabed: any;
  constructor(private packageservice: PackageService,
    private cookie: CommonCookieService,
    public objcommon: ApiUrlService,
    private route: ActivatedRoute,
    private userService: UserService,
    private ngxService: NgxUiLoaderService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.BookingId = this.route.snapshot.queryParamMap.get("ID");
    this.Reservation = new Object();
    this.AgentDetails = new Object();
    this.PackageDetails = new Object();
    this.TaxPerAmnt = [];
    this.getUserdata();
    if (!this.isb2c)
      this.getlogo(this.userdetails.LoginDetail.sid);
    this.ngxService.start();
    this.packageservice.getInvoice(this.BookingId).subscribe((res: any) => {
      this.ngxService.stop();
      debugger
      console.log(JSON.stringify(res));
      if (res.retCode === 1) {
        this.SubTotal = this.getTotal(res.arrResrvation)
        this.Reservation = res.arrResrvation;
        this.AgentDetails = res.AgentDetails;
        this.PackageDetails = res.PackageDetails;
        this.TaxPerAmnt = this.getTaxAmount(res.TaxPerAmnt);
        this.Extrabed = this.getExtrabedprice();
      }
    });
  }

  getExtrabedprice() {
    debugger
    let price;
    if (this.PackageDetails.Category.length > 0) {
      return price = this.PackageDetails.Category.filter(c => c.CategoryID == this.Reservation.CategoryId)[0].ExtraAdult;
    }
    else {
      return price = 0
    }
  }

  getTaxAmount(taxes: any) {
    this.Total = this.SubTotal;
    taxes.forEach(tax => {
      tax.amount = (tax.Value / 100) * this.SubTotal;
      this.Total += (+tax.amount);
    });
    return taxes
  }

  getTotal(arrResrvation: any) {
    let total = 0;
    total += +((arrResrvation.AdultPrice) * (arrResrvation.Adults)).toFixed(2);
    total += +((arrResrvation.ChildBedsPrice) * (arrResrvation.Childs)).toFixed(2);
    return total
  }

  getUserdata() {
    debugger
    if (this.cookie.checkcookie('login')) {
      let user = JSON.parse(this.cookie.getcookie('login'));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") {
        this.isb2c = false;
        this.Currency = this.userdetails.LoginDetail.AgencyDetail.OtherDetail.Currency
      }

      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
        this.Currency = this.objcommon.Currency
      }
    }
    this.cd.detectChanges();
  }

  getlogo(id: any) {
    this.userService.AgentInfo(id).subscribe((res: any) => {
      console.log(res);
      if (res.retCode === 1) {
        this.logo = res.arrUser.CompanyDetails.LogoLink;
        this.cd.detectChanges();
      }
    })
  }

  Download() {
    $("#btn_Invoice_pdf").click();
  }

  Print() {
    window.print();
  }

}
