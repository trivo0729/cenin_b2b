import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { Package, ArrBooking } from 'src/app/modals/packages/package.modal';
import { PackageService } from 'src/app/services/packages/package.service';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { AlertService } from 'src/app/services/alert.service';
import { UserComponent } from '../../user/user.component';
import { MatDialog } from '@angular/material/dialog';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import * as moment from 'moment';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CurrencyService } from 'src/app/services/commons/currency.service';
import { drawDOM, exportPDF, DrawOptions, Group, exportImage } from '@progress/kendo-drawing';
declare function StickySidebar(): any;
declare function Showtimepicker(): any;
declare function setimage(img: string): any;

@Component({
    selector: 'app-package-booking',
    templateUrl: './package-booking.component.html',
    styleUrls: ['./package-booking.component.css']
})
export class PackageBookingComponent implements OnInit {
    Booking: ArrBooking;
    login: Userdetails = new Userdetails();
    submitted = false;
    ID: string;
    AdminID: number;
    Packages: Package;
    img: string = "";
    packbookingForm: FormGroup;
    mindate: any;
    opens: string;
    drops: string;
    autoApply: boolean;
    singleDatePicker: boolean;
    bit: any;
    showprice: boolean;
    pricedetails: any;
    minimum: any[];
    currency: string;
    Package: any;
    ImageData: any;
    PDFData: any;
    options: DrawOptions;
    constructor(
        private packageService: PackageService,
        private route: ActivatedRoute,
        private cd: ChangeDetectorRef,
        private zone: NgZone,
        private datePipe: DatePipe,
        private ServiceUrl: ApiUrlService,
        private formBuilder: FormBuilder,
        private Alert: AlertService,
        private cookie: CommonCookieService,
        private currencyService: CurrencyService,
        private ngxService: NgxUiLoaderService
    ) {
        this.AdminID = this.ServiceUrl.AdminID;
        this.opens = 'down';
        this.singleDatePicker = true;
        this.drops = 'down';
    }

    ngOnInit() {
        debugger;
        this.currency = this.currencyService.getCurrency();
        if (this.currency === null)
            this.currency = this.ServiceUrl.Currency;
        this.minimum = [];
        this.showprice = false;
        this.pricedetails = new Object();
        this.Package = new Object();
        this.Packages = new Package();
        this.Packages.Category = [];
        this.login.retCode = 0;
        if (this.cookie.checkcookie('login')) {
            this.login = JSON.parse(this.cookie.getcookie('login'));
        }
        //this.onChange();
        this.Booking = new ArrBooking();
        this.Booking.Adults = 1;
        this.Booking.Childs = 0;
        this.Booking.Infant = 0;
        if (this.cookie.checkcookie('login')) {
            this.Booking.email = this.login.LoginDetail.Email;
            this.Booking.LeadingPax = this.login.LoginDetail.Name;
            this.Booking.uid = this.login.LoginDetail.sid;
        }
        console.log(this.Booking);
        this.packbookingForm = this.formBuilder.group({
            Category: ['', Validators.required],
            Name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phone: ['', Validators.required],
            date: ['', [Validators.required]],
            adult: ['', [Validators.required]],
            child: ['', [Validators.required]],
            infant: ['', [Validators.required]],
        });
        StickySidebar();
        this.ID = this.route.snapshot.queryParamMap.get('ID');
        this.bit = this.route.snapshot.queryParamMap.get('bit');
        this.packageService.getPackageDetail(this.ID).subscribe((res: Package) => {
            debugger
            this.Packages = res[0];
            this.Packages.City = this.Packages.City.split("|");
            this.Packages.ValidFrom = this.datePipe.transform(this.Packages.ValidFrom, "DD-MM-YYYY");
            this.Packages.ValidTo = this.datePipe.transform(this.Packages.ValidTo, "DD-MM-YYYY");
            if (this.Packages.Images.length > 0)
                this.img = this.Packages.Images[0].Url;
            debugger;
            //if (this.Packages.Category.length != 0 && this.Packages.Category[0].Single ==0 && this.Packages.Category[0]. Double== 0 && this.Packages.Category[0].Triple==0 && this.Packages.Category[0].Quad==0 && this.Packages.Category[0].Quint==0)
            if (this.Packages.Category.length > 0)
                this.Booking.CategoryId = this.Booking.CategoryId = this.Packages.Category[0].CategoryID;
            else
                this.Booking.CategoryId = 1;
            this.SetImages();
            this.getPrice(this.Booking.CategoryId);
            this.setPackageForDrowing(this.Booking.CategoryId);
        });
        console.log(this.Packages);
        this.singleDatePicker = true;
        var min = new Date();
        this.mindate = moment(min);
        this.Booking.TravelDate = moment(min).format("DD-MM-YYYY");
        console.log(this.Booking.TravelDate);

    }

    getPrice(Id: any) {
        debugger
        if (this.Packages.Category.length > 0) {
            this.showprice = true;
            let price = this.Packages.Category.filter(c => c.CategoryID === +Id)[0];
            this.SetMinimumPrice(price);
            if (this.Packages.MinPrice > 0)
                this.showprice = true;
            this.pricedetails = {
                Adult: this.Packages.MinPrice,
                Child: price.ChildPrice,
                ChildWithBed: price.ChildWithBedPrice,
                ChildNoBedPrice: price.ChildNoBedPrice
            }
        }
    }

    SetMinimumPrice(price: any) {
        debugger;
        this.minimum = [];
        if (this.Packages.Category.length != 0) {
            if (price.Double != 0) {
                this.minimum.push(price.Double);
            }
            if (price.Single != 0) {
                this.minimum.push(price.Single);
            }
            if (price.Triple != 0) {
                this.minimum.push(price.Triple);
            }
            if (price.Quad != 0) {
                this.minimum.push(price.Quad);
            }
            if (price.Quint != 0) {
                this.minimum.push(price.Quint);
            }
            this.Packages.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
        }
        else {
            this.Packages.MinPrice = 0;
        }
    }

    setPackageForDrowing(Id: any) {
        debugger
        let _package = this.Packages
        if (this.Packages.Category.length > 0) {
            let Category = this.Packages.Category.filter(c => c.CategoryID === +Id);
            _package.Category = Category
        }
        this.Package = _package
    }

    SetImages() {
        setTimeout(() => {
            setimage(this.img);
        }, 20);
    }
    // convenience getter for easy access to form fields
    get f() { return this.packbookingForm.controls; }

    public Book(date: any, catId: any): void {
        debugger;
        this.Booking.TravelDate = date;
        if (catId !== null || catId !== undefined || catId !== "")
            this.Booking.CategoryId = +catId
        debugger
        this.submitted = true;
        // stop here if form is invalid
        if (this.packbookingForm.invalid) {
            console.log(this.packbookingForm)
            return;
        }
        else {
            this.ngxService.start();
            //this.BookPackage();
            $("#box_style_pdf").show();
            this.onScroll();
            $("#btn_image").click();
            // setTimeout(() => {
            //     $("#box_style_pdf").hide();
            // }, 5000);
        }

    }

    onScroll() {
        $("html, body").animate({ scrollTop: 250 }, 600);
    }

    public BookPackage() {
        debugger;
        this.ngxService.start();
        this.Booking.nID = this.Packages.Packageid;
        this.Booking.PackageID = this.Packages.Packageid;
        this.Booking.CategoryId = + this.Booking.CategoryId;
        let params = [];
        params.push(this.ImageData.toString());
        params.push(this.PDFData.toString());
        console.log(this.Booking);
        console.log(JSON.stringify(params));
        this.packageService.BookPackage(this.Booking, params).subscribe((res: any) => {
            this.ngxService.stop();
            if (res.retCode == 1) {
                if (this.bit != null) {
                    this.Alert.succsess('Thanks For Your Enquiry! ', 'We will update you soon..', 'success', function () {
                        window.location.href = 'home'
                    });
                }
                if (this.bit === null) {
                    this.Alert.succsess('Thanks For Booking!', 'We Will Update you when it is Confirm by Administrator..', 'success', function () {
                        window.location.href = 'home'
                    });
                }

                setTimeout(() => {
                    window.location.href = 'home';
                }, 5000);
            }
            else {
                this.Alert.confirm(res.Meassge, 'warning');
            }
        },
            error => {
                this.ngxService.stop();
            }
        );
    }

    SetDateValidity(Id: string) {
        debugger;
        if (Id !== undefined) {
            const CategoryID = parseInt(Id);
            this.getPrice(CategoryID);
            this.setPackageForDrowing(CategoryID);
            var data = this.Packages.Category.filter(x => x.CategoryID == CategoryID);
            if (data.length != 0) {
                this.Booking.StartFrom = data[0].ValidFrom;
                this.Booking.EndDate = data[0].ValidUpto;
                this.Booking.Type = data[0].Categoryname;

                this.Booking.AdultPrice = data[0].Double;
                this.Booking.ChildBedsPrice = data[0].ChildWithBedPrice;
                this.Booking.ChildWBed = data[0].ChildNoBedPrice;
            }
            else {
                this.Booking.StartFrom = "";
                this.Booking.EndDate = "";
                this.Booking.Type = "Standard";
                this.Booking.AdultPrice = "0.00";
                this.Booking.ChildBedsPrice = "0.00";
                this.Booking.ChildWBed = "0.00";
            }
            this.Booking.noDays = this.Packages.noDays;

            this.Booking.ParentId = this.AdminID;
            this.Booking.BookingDate = this.datePipe.transform(new Date(), "dd-MM-yyyy");
        }
    }

    onTraveller(paxType: any, action: any) {
        debugger;
        switch (action) {
            case "plus":
                // * add pax * //
                this.add(paxType);
                break;
            case "minus":
                // * remove pax * //
                this.remove(paxType);
                break;
        }
        this.cd.detectChanges();
    }

    add(paxType: any) {
        switch (paxType) {
            case "ad":
                this.Booking.Adults = this.Booking.Adults + 1;
                break;
            case "ch":
                this.Booking.Childs = this.Booking.Childs + 1;
                break;
            case "in":
                this.Booking.Infant = this.Booking.Infant + 1;
                break;
        }
    }

    remove(paxType: any) {
        switch (paxType) {
            case "ad":
                if (this.Booking.Adults > 1)
                    this.Booking.Adults = this.Booking.Adults - 1;
                break;
            case "ch":
                if (this.Booking.Childs > 0)
                    this.Booking.Childs = this.Booking.Childs - 1;
                break;
            case "in":
                if (this.Booking.Infant > 0)
                    this.Booking.Infant = this.Booking.Infant - 1;
                break;
        }
    }


    onChange() {
        debugger;
        const self = this;
        /* Adult */
        $("#inc_Adult").append('<div class="inc button_inc Adult">+</div><div class="dec button_inc Adult">-</div>');
        $(".Adult").on("click", function () {
            debugger;
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue.toString()) + 1;
            } else {
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue.toString()) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find("input").val(newVal);
            self.Booking.Adults = newVal;
        });
        /* Child */
        $(".inc_Child").append('<div class="inc button_inc Child">+</div><div class="dec button_inc Child">-</div>');
        $(".Child").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue.toString()) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue.toString()) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find("input").val(newVal);
            self.Booking.Childs = newVal;
        });

        /* Infant */

        $(".inc_Infant").append('<div class="inc button_inc Infant">+</div><div class="dec button_inc Infant">-</div>');
        $(".Infant").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue.toString()) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue.toString()) - 1;
                } else {
                    newVal = 0;
                }
            }
            $button.parent().find("input").val(newVal);
            self.Booking.Infant = newVal;
        });
        // this.zone.run(() => {

        // });
    }



    public exportElement(element: HTMLElement) {
        this.options = {
            paperSize: "auto",
            margin: { left: "0cm", top: "0cm", right: "0cm", bottom: "0cm" }
        }
        let self = this;
        self.getImage(element, this.options);
    }

    getImage(element: HTMLElement, options?: DrawOptions) {
        let self = this;
        drawDOM(element, options).then((group: Group) => {
            return exportImage(group);
        }).then((dataUri) => {
            console.log(dataUri);
            this.ImageData = dataUri;
            self.getPDF(element, options)
        });
    }

    getPDF(element: HTMLElement, options?: DrawOptions) {
        let self = this;
        drawDOM(element, options).then((group: Group) => {
            return exportPDF(group);
        }).then((dataUri) => {
            console.log(dataUri);
            this.PDFData = dataUri;
            $("#box_style_pdf").hide();
            self.BookPackage()
        });
    }
}
