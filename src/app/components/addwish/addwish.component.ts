import { Component, OnInit, Input } from '@angular/core';
import { loginDetails, User } from 'src/app/modals/user.model';
import { UserService } from 'src/app/services/user.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { UserComponent } from '../user/user.component';
import { Wishlist } from 'src/app/modals/common/wishlist.model';
import { CommonService } from 'src/app/services/commons/common.service';
import { ToastrService } from 'ngx-toastr';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-addwish',
  templateUrl: './addwish.component.html',
  styleUrls: ['./addwish.component.css']
})
export class AddwishComponent implements OnInit {
  @Input() ProductType: number; /*Activity :1 , Package:2*/
  @Input() ProdcutDetails: any;
  arrWish: Wishlist = new Wishlist();
  public login: Userdetails = new Userdetails();
  public arrUser: User = new User();
  constructor(public userService: UserService,
    private cookie: CommonCookieService,
    public commonService: CommonService,
    public dialog: MatDialog,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
  }

  addtoWish() {
    debugger;
    /*Check User logedIn */
    if (this.cookie.checkcookie('login')) {
      this.login = JSON.parse(this.cookie.getcookie('login'));
      this.saveWish();
    }
    else {
      const dialogRef = this.dialog.open(UserComponent);
      dialogRef.afterClosed().subscribe(result => {
        //this.saveWish();
      });
    }
  }

  saveWish() {
    debugger;
    this.arrWish.Details = JSON.stringify(this.ProdcutDetails);
    this.arrWish.ProductType = this.ProductType;
    if (this.ProductType == 1)
      this.arrWish.ProductID = this.ProdcutDetails.ActivityID;
    this.arrWish.UserID = this.login.LoginDetail.Email;
    this.commonService.AddtoWishList(this.arrWish).subscribe(
      (res: any) => {
        if (res.retCode == 1) {
          this.toastr.success("Added on Wish list")
        }
        else { this.toastr.error(res.Message.toString()) }
      });

  }
}
