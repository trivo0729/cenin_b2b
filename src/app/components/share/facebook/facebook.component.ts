import { Component, OnInit, Input } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {
fbIcon = faFacebookSquare;
@Input() url: string;
@Input() details:string;
@Input() img:string;
@Input() title: string;
  constructor() { }

  ngOnInit() {
  }

}
