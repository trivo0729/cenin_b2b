import { Component, OnInit } from '@angular/core';
import { UserCred, loginDetails, UserEmail } from 'src/app/modals/user.model';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl, ValidatorFn } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  Passwordfrm: FormGroup;
  password: any;
  loading: boolean;
  ConfirmPassword: any;
  submitted = false;
  UserCred: UserCred;
  AdminID: number;
  isb2b: Boolean;
  login: Userdetails = new Userdetails();
  constructor(private userService: UserService, private service: ApiUrlService, private formBuilder: FormBuilder, private Alert: AlertService, private cookie: CommonCookieService) {
    this.AdminID = service.AdminID;
  }

  ngOnInit() {
    this.isb2b = false;
    this.loading = false;
    this.UserCred = new UserCred();
    this.Passwordfrm = this.formBuilder.group(
      {
        Password: ['', Validators.required],
        _Confirm: ['', [Validators.required]],
        ConfirmNew: ['', [Validators.required]],
      },
      {
        validator: MustMatch('_Confirm', 'ConfirmNew')
      });
    this.getUserDetails()
  }


  getUserDetails() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie('login')) {
      this.login = JSON.parse(this.cookie.getcookie('login'));
      if (this.login.LoginDetail.UserType === "B2B")
        this.isb2b = true
      if (this.login.LoginDetail.UserType === "B2C")
        this.isb2b = false
    }
  }


  get f() { return this.Passwordfrm.controls; }

  public CheckValidation(): void {

    this.submitted = true;
    // stop here if form is invalid
    if (this.Passwordfrm.invalid) {
      return;
    }
    else {
      this.ChangePassword();
    }
  }

  ChangePassword() {
    this.loading = true
    this.UserCred.sid = this.login.LoginDetail.sid;
    this.userService.ChangePassword(this.login.LoginDetail.sid, this.UserCred.Password, this.UserCred.ConfirmPassword, this.login.LoginDetail.UserType).subscribe(
      (res: any) => {
        this.loading = false;
        if (res.retCode == 1) {
          this.Passwordfrm.reset();
          this.submitted = false;
          this.Alert.confirm('User Password Updated Successfully', 'success');
          this.redirect();
        } else {
          this.Alert.confirm(res.Message, 'warning')
        }
      }, error => {
        this.loading = false;
      });
  }

  redirect() {
    setTimeout(() => {
      this.userService.clearSession('LoginDetails');
      if (this.cookie.checkcookie('login')) {
        this.cookie.deletecookie('login');
      }
      window.location.href = '/home';
    }, 1000);
  }

}

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }
    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
