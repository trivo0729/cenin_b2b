import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from 'src/app/services/commons/report.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { BookingDetails } from 'src/app/modals/tours.modals';
import { AdminUser, AgentDetails } from 'src/app/modals/hotel/report.modal';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { DialogService } from 'src/app/services/commons/dialog.service';

@Component({
  selector: 'app-sightseeing-booking-details',
  templateUrl: './sightseeing-booking-details.component.html',
  styleUrls: ['./sightseeing-booking-details.component.css']
})
export class SightseeingBookingDetailsComponent implements OnInit {
  UserId: any;
  ReservationID: any;
  action: string;
  _display: string;
  BookingDetails: BookingDetails[];
  Invoice: BookingDetails;
  AdminUser: AdminUser;
  AgentDetails: AgentDetails;
  login: Userdetails;
  Individual: boolean;

  constructor(private reportService: ReportService,
    private cookie: CommonCookieService,
    private ServiceUrl: ApiUrlService,
    private dialogService: DialogService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.BookingDetails = [];
    this.AdminUser = new AdminUser();
    this.AgentDetails = new AgentDetails();
    this.login = new Userdetails();
    this.Invoice = new BookingDetails();
    this._display = 'Detail';
    console.log(this.Individual);
    this.ReservationID = +this.route.snapshot.queryParamMap.get('parm_1');
    if (this.ReservationID !== null || this.ReservationID !== undefined)
      this.getBookingDetails(this.ReservationID);
  }

  getBookingDetails(ReservationID: number) {
    debugger
    this.reportService.getActivityBooking(ReservationID, this.getUserId()).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.BookingDetails = res.BookingByID;
        this.AdminUser = res.AdminUser;
        this.AgentDetails = res.AgentDetails;
      }
    });
  }

  getUserId() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      this.login = data;
      if (data.LoginDetail.UserType === "B2B") {
        this.UserId = data.LoginDetail.sid;
      }
    } else {
      this.UserId = this.ServiceUrl.UserId;
    }
    return this.UserId;
  }

  downloadPdf(service: string, index: any) {
    debugger;
    this.Invoice = this.BookingDetails[index];
    this.action = "download";
    if (service === "Invoice") {
      this._display = service;
      this.Action(service);
    }

    if (service === "Voucher") {
      if (+(this.Invoice.TotalPax) > 1) {
        this.dialogService.openConfirmDialog('Download Individual tickets for all paxes ?')
          .afterClosed().subscribe(res => {
            if (res !== undefined) {
              this._display = service;
              this.Individual = res;
            }
            console.log(res);
          });
      }
      else {
        this._display = service;
        this.Individual = false;
      }
    }

  }

  print(service: string, index: any) {
    debugger;
    this.action = "print";
    this.Invoice = this.BookingDetails[index];
    if (service === "Invoice") {
      this._display = service;
      this.Action(service);
    }
    if (service === "Voucher") {
      if (+(this.Invoice.TotalPax) > 1) {
        this.dialogService.openConfirmDialog('Print Individual tickets for all paxes ?')
          .afterClosed().subscribe(res => {
            if (res !== undefined) {
              this._display = service;
              this.Individual = res;
            }
            console.log(res);
          });
      }
      else {
        this._display = service;
        this.Individual = false;
      }
    }
  }

  Back() {
    debugger;
    this._display = 'Detail';
    this.action = "";
  }


  Action(service: any) {
    debugger;
    if (this.action === "download") {
      /* download invoice */
      if (service === 'Invoice') {
        setTimeout(() => {
          $("#btn_Invoice_pdf").click();
        }, 1000);
      }

      /* download voucher */
      if (service === 'Voucher') {
        setTimeout(() => {
          $("#btn_Voucher_pdf").click();
        }, 1000);
      }
    }

    if (this.action === "print") {
      setTimeout(() => {
        window.print();
      }, 1000);
    }

  }

}
