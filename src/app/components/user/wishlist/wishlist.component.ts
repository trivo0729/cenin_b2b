import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/commons/common.service';
import { Wishlist } from 'src/app/modals/common/wishlist.model';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { Userdetails } from 'src/app/modals/user/userdetails';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  arrWishes: Wishlist[];
  tours: any;
  Package: any;
  TempPackage: any;
  minimum: Array<number>;
  isb2b: Boolean;
  login: Userdetails = new Userdetails();
  constructor(private commonService: CommonService,
    private userService: UserService,
    private toastr: ToastrService,
    private cookie: CommonCookieService,
    private sessionService: SessionService,
    private router: Router
  ) { }

  ngOnInit() {
    this.isb2b = false;
    this.getUserDetails()
    this.CheckWish();
  }

  getUserDetails() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie('login')) {
      this.login = JSON.parse(this.cookie.getcookie('login'));
      if (this.login.LoginDetail.UserType === "B2B")
        this.isb2b = true
      if (this.login.LoginDetail.UserType === "B2C")
        this.isb2b = false
    }
  }

  showTourDetails(tour: any) {
    this.sessionService.clear("Details");
    this.sessionService.set("Details", JSON.stringify(tour));
    this.router.navigateByUrl("/tour-details")
  }

  CheckWish() {
    debugger;
    this.arrWishes = [];
    this.tours = [];
    this.Package = [];
    this.commonService.CheckWish(this.login.LoginDetail.Email).subscribe(
      (res: any) => {
        if (res.retCode == 1) {
          this.arrWishes = res.arrWish;
          var t = 0;
          var p = 0;
          this.arrWishes.forEach(arrWish => {
            if (arrWish.ProductType == 1) /* For Activity */ {
              this.tours.push(JSON.parse(arrWish.Details));
              this.tours[t].ID = arrWish.ID;
              t++;
            }
            if (arrWish.ProductType == 2) {
              console.log(JSON.parse(arrWish.Details))
              this.Package.push(JSON.parse(arrWish.Details));
              this.Package[p].ID = arrWish.ID;
              p++;
            }
          });
          this.TempPackage = this.Package;
          this.SetMinimumPrice();
        }
        else { this.toastr.error(res.Message) }
      });
  }

  RemoveWish(ProductType: number, ID: any) {
    this.commonService.RemoveWish(ProductType, ID, this.login.LoginDetail.Email).subscribe(
      (res: any) => {
        if (res.retCode == 1) {
          this.CheckWish();
        }
        else { this.toastr.error(res.Message) }
      }
    )
  }

  SetMinimumPrice() {
    try {
      debugger;
      this.Package = [];
      this.TempPackage.forEach(arrPackage => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (this.minimum.length !== 0)
            arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
          else
            arrPackage.MinPrice = 0;
        }
        else {
          arrPackage.MinPrice = 0;
        }
        this.Package.push(arrPackage);
      });
    } catch (e) { console.log(e.message) }


  }

}
