import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, Input } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { bookings } from 'src/app/modals/tours.modals';
import { Router } from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-tour-bookings',
  templateUrl: './tour-bookings.component.html',
  styleUrls: ['./tour-bookings.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TourBookingsComponent implements OnInit {
  arrBookings: bookings[];
  Bookings: bookings[];
  page: number = 1;
  autoApply: boolean;
  closeOnAutoApply: boolean;
  maxDate: any;
  showDropdowns: boolean;
  lockStartDate: boolean;
  singleDatePicker: boolean;
  opens: string;
  drops: string;
  response: boolean;
  allStatus: string[];
  Status: any;
  Checkin: any;
  Checkout: any;
  BookingDate: any;
  GuestName: string = '';
  ReferenceNo: string = '';
  SightseeingName: string = '';
  Location: string = '';
  @Input() service: string;
  @Input() userinfo: any;
  @Output() Services = new EventEmitter();

  constructor(private tourService: ToursService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    this.opens = 'right';
    this.drops = 'down';
    this.singleDatePicker = true;
  }

  ngOnInit() {
    this.arrBookings = [];
    this.Bookings = [];
    this.response = false;
    this.allStatus = ['All', 'Vouchered', 'Cancelled'];
    this.Status = 'All';
    var max = new Date();
    this.maxDate = moment(max);
    if (this.userinfo !== null) {
      var UserName = ""
      var UserType = this.userinfo.UserType
      if( UserType == "B2B")
           UserName= this.userinfo.UserId
      else
            UserName= this.userinfo.Email    
      this.tourService.getBooking(UserName,UserType).subscribe(
        (res: bookings[]) => {
          this.arrBookings = res;
          this.Bookings = res;
          this.response = true;
          this.Services.emit({
            name: "Tour",
            icon: "icon-suitcase font15"
          });
          this.cd.detectChanges();
          console.log(this.arrBookings)
        });
    }
  }

  _filter(BookingDate: string, Sightseeingdate: string) {
    this.arrBookings = this.Bookings;

    if (BookingDate !== '')
      this.arrBookings = this.arrBookings.filter(b => b.BookDate === BookingDate)

    if (Sightseeingdate !== '')
      this.arrBookings = this.arrBookings.filter(b => b.SightseeingDate.toLowerCase().includes(Sightseeingdate.toLowerCase()))

    // if (this.GuestName !== '')
    //   this.arrBookings = this.arrBookings.filter(b => b.bookingname.toLowerCase().includes(this.GuestName.toLowerCase()))

    if (this.SightseeingName !== '')
      this.arrBookings = this.arrBookings.filter(b => b.Name.toLowerCase().includes(this.SightseeingName.toLowerCase()))

    // if (this.Location !== '')
    //   this.arrBookings = this.arrBookings.filter(b => b.City.toLowerCase().includes(this.Location.toLowerCase()))

    if (this.Status !== "All")
      this.arrBookings = this.arrBookings.filter(b => b.Status === this.Status)
  }

  clear() {
    this.GuestName = '';
    this.ReferenceNo = '';
    this.SightseeingName = '';
    this.Location = '';
    this.Status = "All";
    this.Checkin = '';
    this.Checkout = '';
    this.BookingDate = '';
    this.arrBookings = this.Bookings;
  }

  showBookingDetails(ReservationID: any) {
    this.router.navigate(['/sightseeing-booking-details'], { queryParams: { parm_1: ReservationID } });
  }
}
