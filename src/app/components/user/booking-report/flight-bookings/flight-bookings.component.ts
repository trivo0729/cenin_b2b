import {
  Component,
  OnInit,
  Input,
  Output,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  EventEmitter,
} from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { ReportService } from "src/app/services/commons/report.service";
import { ArrResult } from "src/app/modals/flight/flight-booking-report.modal";
import * as moment from "moment";
import { Router } from "@angular/router";
@Component({
  selector: "app-flight-bookings",
  templateUrl: "./flight-bookings.component.html",
  styleUrls: ["./flight-bookings.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlightBookingsComponent implements OnInit {
  @Input() service: string;
  @Input() userdata: any;
  @Output() Services = new EventEmitter();
  FlightBookingList: ArrResult[];
  FlightBookings: ArrResult[];
  DepartureDate: any;
  Date: any;
  TravellerName: string = "";
  TransactionNo: string = "";
  allStatus: string[];
  Status: any;
  UserId: number;
  page: number = 1;
  constructor(
    public userService: UserService,
    private apiurl: ApiUrlService,
    private cd: ChangeDetectorRef,
    private reportService: ReportService,
    private router: Router
  ) {
    this.UserId = this.apiurl.UserId;
  }

  ngOnInit() {
    //this.getFlightBooking();
    this.FlightBookingList = [];
    this.allStatus = ["All", "Ticketed", "Cancelled", "Hold"];
    this.Status = "All";
  }

  _flightfilter(TravellerName: string, TransactionNo: string) {
    this.FlightBookingList = this.FlightBookings;

    if (this.TravellerName != "") {
      this.FlightBookingList = this.FlightBookingList.filter((f) =>
        f.LeadingPaxName.toLowerCase().includes(
          this.TravellerName.toLowerCase()
        )
      );

      if (this.TransactionNo != "") {
        this.FlightBookingList = this.FlightBookingList.filter(
          (f) => f.InvoiceNo === TransactionNo
        );
      }

      if (this.Status != "All") {
        this.FlightBookingList = this.FlightBookingList.filter(
          (f) => f.Status === this.Status
        );
      }
    }
  }

  clear() {
    this.TravellerName = "";
    this.TransactionNo = "";
    this.Status = "All";

    this.FlightBookingList = this.FlightBookings;
  }

  // getFlightBooking() {
  //   if (this.userdata != null) {
  //     this.reportService
  //       .getflightBooking(this.userdata.UserId, this.userdata.Email)
  //       .subscribe((res: any) => {
  //         if (res.retCode === 1) {
  //           console.log(res);
  //           this.Services.emit({
  //             name: "Flight",
  //             icon: "pe-7s-plane font15",
  //           });
  //           this.FlightBookingList = res.arrResult;
  //           this.FlightBookings = res.arrResult;
  //           this.FlightBookingList.forEach((flight) => {
  //             this.Date = moment(flight.Date, "DD-MM-YYYY");
  //             this.DepartureDate = moment(flight.DepartureDate, "DD-MM-YYYY");
  //           });
  //           //  this.response = true;
  //           this.cd.detectChanges();
  //         }
  //         console.log(JSON.stringify(res));
  //       });
  //   }
  // }

  showflightBookingDetails(UserId: any, BookingID: any) {
    this.router.navigate(["/flight-booking-details"], {
      queryParams: { parm_1: UserId, parm_2: BookingID },
    });
  }
}
