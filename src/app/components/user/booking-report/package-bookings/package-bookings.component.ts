import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { ReportService } from "src/app/services/commons/report.service";
import { PakageReport } from "src/app/modals/packages/pakage-report.modal";
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as moment from 'moment';

@Component({
  selector: "app-package-bookings",
  templateUrl: "./package-bookings.component.html",
  styleUrls: ["./package-bookings.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PackageBookingsComponent implements OnInit {
  UserId: number;
  PakageBookingList: PakageReport[];
  page: number = 1;
  response: boolean;
  BookingDate: any;
  GuestName: string = '';
  PackageName: string = '';
  Location: string = '';
  PackageBookings: PakageReport[];
  autoApply: boolean;
  closeOnAutoApply: boolean;
  maxDate: any;
  showDropdowns: boolean;
  lockStartDate: boolean;
  singleDatePicker: boolean;
  opens: string;
  drops: string;
  @Input() service: string;
  @Output() Services = new EventEmitter();
  @Input() userinfo: any;
  constructor(
    private reportService: ReportService,
    private router: Router,
    private cd: ChangeDetectorRef,
    private ngxService: NgxUiLoaderService,
  ) { }

  ngOnInit() {
    this.response = false;
    var max = new Date();
    this.maxDate = moment(max);
    this.singleDatePicker = false;
    this.PakageBookingList = [];
    this.getPackageBooking();
  }

  /* package booking */
  getPackageBooking() {
    debugger
    this.ngxService.start();
    if (this.userinfo !== null) {
      this.reportService.getPackageBooking(this.userinfo.UserId).subscribe(
        (res: any) => {
          this.ngxService.stop();
          this.response = true;
          console.log(JSON.stringify(res));
          if (res.retCode === 1) {
            this.setBookingList(res.arrBookingList);
            // this.PakageBookingList = res.arrBookingList;
            // this.PackageBookings = res.arrBookingList;
            this.Services.emit({
              name: "Package",
              icon: "icon_set_1_icon-8 font15",
            });
          }
          this.cd.detectChanges();
        },
        (error: any) => {
          this.response = true;
          console.log(error);
        }
      );
    }
  }

  setBookingList(arrBookingList: any) {
    debugger
    let bookinglist = [];
    arrBookingList.forEach(list => {
      let themes = [];
      themes = list.Theme.split(",");
      let p = themes.filter(d => d == "10")
      if (p.length !== 0)
        bookinglist.push(list)
    });
    this.PakageBookingList = bookinglist;
    this.PackageBookings = bookinglist;
  }

  _filter(BookingDate: string) {
    debugger
    const _Date = BookingDate.split(' - ');
    const start = _Date[0];
    const end = _Date[1];
    this.PakageBookingList = this.PackageBookings;
    if (BookingDate !== '')
      // this.PakageBookingList = this.PakageBookingList.filter(b => b.BookingDate === BookingDate)
      this.PakageBookingList = this.PakageBookingList.filter(b => b.BookingDate >= start && b.BookingDate <= end)

    if (this.GuestName !== '')
      this.PakageBookingList = this.PakageBookingList.filter(b => b.PaxName.toLowerCase().includes(this.GuestName.toLowerCase()))


    if (this.PackageName !== '')
      this.PakageBookingList = this.PakageBookingList.filter(b => b.PackageName.toLowerCase().includes(this.PackageName.toLowerCase()))

    if (this.Location !== '')
      this.PakageBookingList = this.PakageBookingList.filter(b => b.Location.toLowerCase().includes(this.Location.toLowerCase()))

    // if (this.Status !== "All")
    //   this.BookingList = this.BookingList.filter(b => b.Status === this.Status)
  }

  clear() {
    this.GuestName = '';
    this.PackageName = '';
    this.Location = '';
    this.BookingDate = '';
    this.PakageBookingList = this.PackageBookings;
  }

  onVoucher(ID) {
    this.router.navigate(["/package-voucher"], {
      queryParams: { ID: ID },
    });
  }
  onInvoice(ID) {
    this.router.navigate(["/package-invoice"], {
      queryParams: { ID: ID },
    });
  }
}
