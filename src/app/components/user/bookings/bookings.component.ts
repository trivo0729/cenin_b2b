import { Component, OnInit } from '@angular/core';
import { ToursService } from 'src/app/services/tours.service';
import { loginDetails, User } from 'src/app/modals/user.model';
import { UserService } from 'src/app/services/user.service';
import { bookings } from 'src/app/modals/tours.modals';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  constructor(private tourService: ToursService,
    private cookie : CommonCookieService
  ) { }
  public arrDetails: loginDetails = new loginDetails();
  public LoginDetails: User = new User();
  arrBookings: bookings[];
  ngOnInit() {
   if (this.cookie.checkcookie('login')) {
       this.arrDetails = JSON.parse(this.cookie.getcookie('login'));
   }
    this.LoginDetails = this.arrDetails.LoginDetail;
    var Email = 'sufiyankhalid01@gmail.com';
    this.tourService.getBooking(Email,"").subscribe(
      (res: bookings[]) => {
        this.arrBookings = res;
        debugger
        console.log(this.arrBookings)
      });
  }
}
