import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AccountService } from 'src/app/services/agent/account.service';
import { Transaction } from 'src/app/modals/agent/transaction.modal';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { process } from '@progress/kendo-data-query';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';


@Component({
  selector: 'app-agency-statement',
  templateUrl: './agency-statement.component.html',
  styleUrls: ['./agency-statement.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AgencyStatementComponent implements OnInit {
  userdetails: any;
  public Transaction: Transaction[];

  constructor(private accountService: AccountService,
     private cookie: CommonCookieService,
     private commonService: ApiUrlService,
    ) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.Transaction = [];
    this.getUserdata();
  }

  getUserdata() {
    if (this.cookie.checkcookie('login')) {
      this.userdetails = JSON.parse(this.cookie.getcookie('login'));
    }
    this.getTransactions();
  }

  getTransactions() {
    if (this.userdetails !== null || this.userdetails !== undefined) {
      let userId = this.userdetails.LoginDetail.sid;
      let UserType = this.userdetails.LoginDetail.UserType;
      this.accountService.GetTransactions(userId, UserType).subscribe((response) => {
        this.Transaction = response;
        console.log(response);
      }, error => {
        console.log(error);
      })
    }
  }


  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.Transaction, { sort: [{ field: 'TransactionID', dir: 'desc' }] }).data,
    };
    return result;
  }
}

