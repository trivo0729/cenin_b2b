import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { GenralService } from 'src/app/services/genral.service';
import * as moment from 'moment';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { ExchangeRate } from 'src/app/modals/dbhelper.model';
import { arrLogin, CompanyDetails, Contact, Address, TaxationDetails, ContactOrOwner, Affilation } from 'src/app/modals/agent/register';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { RegisterService } from 'src/app/services/agent/register.service';
import { AlertService } from 'src/app/services/alert.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { AccountService } from 'src/app/services/agent/account.service';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { UserService } from 'src/app/services/user.service';
export class arrTaxes { Code: string; Name: string }


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
  arrCountry: any[];
  base64textString = [];
  arrStates: any[];
  arrCity: any[];
  showPan: boolean;
  showState: boolean;
  Registered: any[];
  Affilation: any[];
  opens: string;
  drops: string;
  autoApply: boolean;
  singleDatePicker: boolean;
  otherRegister: boolean;
  otherAffilation: boolean;
  submitted: boolean;
  maxDate: any;
  AssSelected: any[];
  public AgentRegform: FormGroup;
  public AssociationList: FormArray;
  public exchange: ExchangeRate[];
  Registration: arrLogin;
  compPhone: string;
  panFile: any;
  compFilename: any;
  taxFilename: any;
  affFilename: any[];
  City: string;
  userDetails: Userdetails;
  logedin: Boolean;
  arrTaxes: arrTaxes[];

  constructor(private genralService: GenralService,
    private objGlobalService: ApiUrlService,
    private registerService: RegisterService,
    private userService: UserService,
    private alert: AlertService,
    private fb: FormBuilder,
    private accountService: AccountService,
    private ngxService: NgxUiLoaderService,
    private cookie: CommonCookieService,
    private cd: ChangeDetectorRef) {
    this.opens = 'down';
    this.singleDatePicker = true;
    this.drops = 'down';
  }

  ngOnInit() {
    this.logedin = false;
    this.City = "";
    this.arrTaxes = [];
    this.Registration = new arrLogin();
    this.Registration.CompanyDetails = new CompanyDetails();
    this.Registration.CompanyDetails.Contact = new Contact();
    this.Registration.CompanyDetails.Address = new Address();
    this.Registration.TaxationDetails = new TaxationDetails();
    this.Registration.Owner = new ContactOrOwner();
    this.Registration.ParentId = this.objGlobalService.AdminID.toString();
    this.Registration.Affilation = [];
    this.AssSelected = [];
    this.affFilename = [];
    this.Registration.Affilation.push({ Name: '', Number: '', Documentlink: '' });
    this.AssSelected.push("");
    this.affFilename.push("");
    this.compPhone = '';
    var max = new Date();
    this.maxDate = moment(max);
    this.Registration.CompanyDetails.RegistrationDate = moment(max).format('DD-MM-YYYY');
    this.Registration.TaxationDetails.TaxRegistrationDate = moment(max).format('DD-MM-YYYY');
    this.panFile = "";
    this.otherRegister = false;
    this.showState = false;
    this.showPan = false;
    this.otherAffilation = false;
    this.exchange = [];
    this.arrCountry = [];
    this.arrStates = [];
    this.arrCity = [];
    this.Registered = [];
    this.Affilation = [];
    this.getUserData();
    this.GetTaxes();
    this.setRegistered();
    this.getCountry();
    this.getCurrency();
    this.setAffilation();
    this.formValidation();
    this.cd.detectChanges();

  }

  getUserData() {
    if (this.cookie.checkcookie('login')) {
      console.log(JSON.parse(this.cookie.getcookie('login')))
      this.userDetails = JSON.parse(this.cookie.getcookie('login'));
      this.logedin = true;
      //this.AgentInfo();
    }
  }

  GetTaxes() {
    this.accountService.GetTaxes(this.objGlobalService.AdminID).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.arrTaxes = res.arrTaxes;
      }
    })
  }

  AgentInfo() {
    this.userService.AgentInfo(this.userDetails.LoginDetail.sid).subscribe((res: any) => {
      console.log(res);
      if (res.retCode === 1) {
        this.Registration = res.arrUser;
        this.compPhone = this.Registration.CompanyDetails.Contact.Phone[0];
        this.cd.detectChanges();
      }
    })
  }

  formValidation() {

    this.AgentRegform = this.fb.group({
      /* company basic details validation */
      tradingName: ['', Validators.compose([Validators.required])],
      legalName: [],
      companyMail: ['', Validators.compose([Validators.email, Validators.required])],
      buildingshop: ['', Validators.compose([Validators.required])],
      compTelephone: ['', Validators.compose([Validators.required])],
      compWeb: [],
      compLandmark: [],
      street: ['', Validators.compose([Validators.required])],
      companycountry: ['', Validators.compose([Validators.required])],
      companycity: ['', Validators.compose([Validators.required])],
      companystate: [],
      zipCode: ['', Validators.compose([Validators.required])],
      panNo: [],

      /* owner details validation */
      ownerFirstname: ['', Validators.compose([Validators.required])],
      ownerLastname: ['', Validators.compose([Validators.required])],
      ownerDesignation: ['', Validators.compose([Validators.required])],
      ownerEmail: ['', Validators.compose([Validators.email, Validators.required])],
      ownerMobile: ['', Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required])],
      ownerPhone: [],

      /* company registration details validation */
      compRegisteredas: ['', Validators.compose([Validators.required])],
      compRegisteredno: ['', Validators.compose([Validators.required])],
      compRegistereddate: ['', Validators.compose([Validators.required])],
      compRegisteredname: ['', Validators.compose([Validators.required])],

      /* tax details validation */
      taxName: ['', Validators.compose([Validators.required])],
      //taxCode: ['', Validators.compose([Validators.required])],
      taxRegNo: ['', Validators.compose([Validators.required])],
      taxRegDate: ['', Validators.compose([Validators.required])],

      /* tax details validation */
      curruncy: ['', Validators.compose([Validators.required])],
      associate: this.fb.array([this.createAssociate()])
    });
    this.AssociationList = this.AgentRegform.get('associate') as FormArray;
  }

  addAssociate() {
    this.AssociationList.push(this.createAssociate());
  }

  createAssociate(): FormGroup {
    return this.fb.group({
      travelAssociation: [],
      travelAssociationNo: [],
      travelAssociationName: [],
      // travelAssociation: ['', Validators.compose([Validators.required])],
      // travelAssociationNo: ['', Validators.compose([Validators.required])],
      // travelAssociationName: [],
    });
  }

  getassociateFormGroup(index: any): FormGroup {
    this.AssociationList = this.AgentRegform.get('associate') as FormArray;
    const formGroup = this.AssociationList.controls[index] as FormGroup;
    return formGroup;
  }

  get associateFormGroup() {
    return this.AgentRegform.get('associate') as FormArray;
  }

  setRegistered() {
    this.Registered = [
      { name: 'Proprietorship' },
      { name: 'Partnership' },
      { name: 'Pvt. Ltd.' },
      { name: 'Limited' },
      { name: 'LLC' },
      { name: 'LLP' },
      { name: 'Other' }
    ]
  }

  setAffilation() {
    this.Affilation = [
      { Name: 'IATA' },
      { Name: 'TAAI' },
      { Name: 'PATA' },
      { Name: 'TAFI' },
      { Name: 'Other' }
    ]
  }

  setStates() {
    this.arrStates = [{ "ID": 1, "StateID": "35", "SateName": "Andaman and Nicobar Islands" }, { "ID": 2, "StateID": "37", "SateName": "Andhra Pradesh" }, { "ID": 3, "StateID": "12", "SateName": "Arunachal Pradesh" }, { "ID": 4, "StateID": "18", "SateName": "Assam" }, { "ID": 5, "StateID": "10", "SateName": "Bihar" }, { "ID": 6, "StateID": "04", "SateName": "Chandigarh" }, { "ID": 7, "StateID": "22", "SateName": "Chhattisgarh" }, { "ID": 8, "StateID": "26", "SateName": "Dadra and Nagar Haveli" }, { "ID": 9, "StateID": "25", "SateName": "Daman and Diu" }, { "ID": 10, "StateID": "07", "SateName": "Delhi" }, { "ID": 11, "StateID": "30", "SateName": "Goa" }, { "ID": 12, "StateID": "24", "SateName": "Gujarat" }, { "ID": 13, "StateID": "06", "SateName": "Haryana" }, { "ID": 14, "StateID": "02", "SateName": "Himachal Pradesh" }, { "ID": 15, "StateID": "01", "SateName": "Jammu and Kashmir" }, { "ID": 16, "StateID": "20", "SateName": "Jharkhand" }, { "ID": 17, "StateID": "29", "SateName": "Karnataka" }, { "ID": 18, "StateID": "32", "SateName": "Kerala" }, { "ID": 19, "StateID": "31", "SateName": "Lakshadweep Islands" }, { "ID": 20, "StateID": "23", "SateName": "Madhya Pradesh" }, { "ID": 21, "StateID": "27", "SateName": "Maharashtra" }, { "ID": 22, "StateID": "14", "SateName": "Manipur" }, { "ID": 23, "StateID": "17", "SateName": "Meghalaya" }, { "ID": 24, "StateID": "15", "SateName": "Mizoram" }, { "ID": 25, "StateID": "13", "SateName": "Nagaland" }, { "ID": 26, "StateID": "21", "SateName": "Odisha" }, { "ID": 27, "StateID": "34", "SateName": "Puducherry" }, { "ID": 28, "StateID": "03", "SateName": "Punjab" }, { "ID": 29, "StateID": "08", "SateName": "Rajasthan" }, { "ID": 30, "StateID": "11", "SateName": "Sikkim" }, { "ID": 31, "StateID": "33", "SateName": "Tamil Nadu" }, { "ID": 32, "StateID": "36", "SateName": "Telangana" }, { "ID": 33, "StateID": "16", "SateName": "Tripura" }, { "ID": 34, "StateID": "09", "SateName": "Uttar Pradesh" }, { "ID": 35, "StateID": "05", "SateName": "Uttarakhand" }, { "ID": 36, "StateID": "19", "SateName": "West Bengal" }, { "ID": 37, "StateID": "37", "SateName": "Andhra Pradesh (New)" }, { "ID": 38, "StateID": "97", "SateName": "Other Territory" }]
  }

  onAffilationAdd(i: any) {
    debugger
    let a = this.Registration.Affilation[i];
    if (a.Name !== "" && a.Number !== "") {
      this.Registration.Affilation.push({ Name: '', Number: '', Documentlink: '' });
      this.AssSelected.push("");
      this.affFilename.push("");
      this.addAssociate();
    }
    else {
      this.alert.confirm("fields can not be balank", "warning")
    }

    // this.submitted = true;
    // if (this.AgentRegform.invalid) {
    //   return;
    // }
    // else {

    // }
  }

  onAffilationRmv(i: any) {
    this.Registration.Affilation.splice(i, 1);
    this.affFilename.splice(i, 1);
    this.AssSelected.splice(i, 1);
    this.AssociationList.removeAt(i);
  }

  getCountry() {
    this.genralService.getCountry().subscribe(
      (res: any) => {
        this.arrCountry = res;
        if (this.cookie.checkcookie('login')) {
          this.AgentInfo();
        }
      }, error => {
        console.log(error);
      });
  }

  getCurrency() {
    this.genralService.getExchange(this.objGlobalService.AdminID).subscribe((res: any) => {
      if (res.retCode == 1) {
        this.exchange = res.arrCurrency;
      }
    }, error => {
      console.log(error)
    });
  }

  getStates() {
    this.genralService.getState().subscribe(
      (res: any) => {
        this.arrStates = res;
      }, error => {
        console.log(error);;
      });
  }

  selectedCountry(event: any) {
    debugger;
    if (event) {
      if (event === 'INDIA') {
        this.showPan = true;
        this.showState = true;
        let validators = Validators.compose([Validators.pattern("[A-Z]{3}[ABCFGHLJPTF]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}"), Validators.required]);
        this.AgentRegform.controls["panNo"].setValidators(validators);
        this.AgentRegform.controls["companystate"].setValidators(Validators.compose([Validators.required]));
        this.AgentRegform.controls["panNo"].updateValueAndValidity();
        this.setStates();
      }
      else {
        this.showPan = false;
        this.showState = false;
        this.AgentRegform.controls["panNo"].clearValidators();
        this.AgentRegform.controls["companystate"].clearValidators();
        this.AgentRegform.controls["panNo"].updateValueAndValidity();
      }
      let c = this.arrCountry.filter(c => c.Countryname === event).find(c => c.Country);
      this.getCity(c.Country);
    }
  }

  onRegisteredSelect(name: any) {
    debugger
    if (name === "Other") {
      this.otherRegister = true;
      let validators = Validators.compose([Validators.required]);
      this.AgentRegform.controls["compRegisteredname"].setValidators(validators);
      this.AgentRegform.controls["compRegisteredname"].updateValueAndValidity();
    }
    else {
      this.otherRegister = false;
      this.AgentRegform.controls["compRegisteredname"].clearValidators();
      this.AgentRegform.controls["compRegisteredname"].updateValueAndValidity();
    }

  }

  onTaxSelect(value: any) {
    debugger;
    try {
      let selected = this.arrTaxes.filter(t => t.Name === value)[0];
      this.Registration.TaxationDetails.TaxCode = selected.Code;
      console.log(selected);
    } catch (error) {

    }

  }

  onAffilationSelect(name: any, i: any) {
    this.AssSelected[i] = name;
    // if (name !== "Other") {
    //   this.AssSelected[i] = name;
    //   // let validators = Validators.compose([Validators.required]);
    //   // this.getassociateFormGroup(i).controls["travelAssociation"].setValidators(validators);
    //   // this.getassociateFormGroup(i).controls["travelAssociationName"].clearValidators();
    //   // this.getassociateFormGroup(i).controls["travelAssociationName"].updateValueAndValidity();
    // }
    // else {
    //   // let validators = Validators.compose([Validators.required]);
    //   // this.getassociateFormGroup(i).controls["travelAssociationName"].setValidators(validators);
    //   // this.getassociateFormGroup(i).controls["travelAssociation"].clearValidators();
    //   // this.getassociateFormGroup(i).controls["travelAssociationName"].updateValueAndValidity();
    // }
  }


  getCity(country: any) {
    this.genralService.getCity(country).subscribe((response: any) => {
      this.arrCity = response;
      if (this.cookie.checkcookie('login')) {
        this.City = this.arrCity.filter(c => c.Code === this.Registration.CompanyDetails.Address.City)[0].Description;
        console.log(this.City);
      }
      this.cd.detectChanges();
    }, error => {
      console.log(error);
    })
  }

  changeRegDate(event: any) {
    debugger;
    if (event.endDate !== null && event.startDate !== null) {
      this.Registration.CompanyDetails.RegistrationDate = moment(event.startDate).format('DD-MM-YYYY');
    }
  }

  changeTaxRegDate(event: any) {
    debugger;
    if (event.endDate !== null && event.startDate !== null) {
      this.Registration.TaxationDetails.TaxRegistrationDate = moment(event.startDate).format('DD-MM-YYYY');
    }
  }


  Save() {
    debugger
    this.submitted = true;
    console.log(JSON.stringify(this.Registration));
    console.log(this.AgentRegform);
    if (this.AgentRegform.invalid) {
      return
    }
    else {
      this.updateParams();
      this.ngxService.start()
    }
  }

  updateParams() {
    debugger;
    let c = this.arrCity.filter(c => c.Description === this.City).find(c => c.Code);
    if (c)
      this.Registration.CompanyDetails.Address.City = c.Code
    let Phone = this.compPhone;
    this.Registration.CompanyDetails.Contact = new Contact();
    this.Registration.CompanyDetails.Contact.Phone = [];
    this.Registration.CompanyDetails.Contact.Email = [];
    if (Phone !== '') {
      this.Registration.CompanyDetails.Contact.Phone.push('' + Phone + '');
    }
    this.Registration.CompanyDetails.Contact.Email.push(this.Registration.UserName);
    this.Registration.CompanyDetails.Contact.Email.push(this.Registration.Owner.Email);
    if (this.Registration.Owner.Phone !== "" || this.Registration.Owner.Phone !== undefined) {
      this.Registration.CompanyDetails.Contact.Phone.push(this.Registration.Owner.Phone)
    }
    console.log(JSON.stringify(this.Registration));
    if (this.logedin)
      this.UpdateProfile();
    else
      this.agentRegistration();
  }

  agentRegistration() {
    this.registerService.OnagentRegistration(this.Registration).subscribe((response) => {
      console.log(response);
      this.ngxService.stop();
      if (response.retCode === 1) {
        this.alert.confirm('Registration done succesfully', 'success');
        setTimeout(() => {
          window.location.href = "/home";
        }, 4500);
      }
      else if (response.retCode === 0 && response.ex === "User Name already exist") {
        this.alert.confirm('Company Email Or Owner Mobile No already exist', 'warning');
      }
      else {
        this.alert.confirm(response.ex, 'error');
      }
    }, error => {
      console.log(error);
      this.ngxService.stop();
    })
  }

  UpdateProfile() {
    this.registerService.UpdateProfile(JSON.stringify(this.Registration)).subscribe((response) => {
      console.log(response);
      this.ngxService.stop();
      if (response.retCode === 1) {
        this.alert.confirm('Profile updated  succesfully', 'success');
        setTimeout(() => {
          window.location.href = "/home";
        }, 4500);
      }
      else {
        this.alert.confirm(response.Message, 'error');
      }
    }, error => {
      console.log(error);
      this.ngxService.stop();
    })
  }


  get f() { return this.AgentRegform.controls; }

  onFileSelected(evt: any, doc: any, index: any) {
    debugger
    var file = evt.target.files[0];
    if (file !== undefined) {
      if (file.name.includes('.jpg') || file.name.includes('.png') || file.name.includes('.jpeg') || file.name.includes('.pdf')) {
        this.getBase64(file).then(data => {
          if (doc === "Pancard") {
            this.Registration.TaxationDetails.PanDocumentLink = 'data:image/png;base64,' + btoa("" + data + "");
            if (file.name.includes('.pdf'))
              this.panFile = file.name;
            else
              this.panFile = "";
          }
          if (doc === "Tax") {
            this.Registration.TaxationDetails.DocumentLink = 'data:image/png;base64,' + btoa("" + data + "")
            if (file.name.includes('.pdf'))
              this.taxFilename = file.name;
            else
              this.taxFilename = "";
          }
          if (doc === "Company") {
            this.Registration.CompanyDetails.DocumentLink = 'data:image/png;base64,' + btoa("" + data + "");
            if (file.name.includes('.pdf'))
              this.compFilename = file.name;
            else
              this.compFilename = "";
          }

          if (doc === "Association") {
            this.Registration.Affilation[index].Documentlink = 'data:image/png;base64,' + btoa("" + data + "");
            if (file.name.includes('.pdf'))
              this.affFilename[index] = file.name;
            else
              this.affFilename[index] = "";
          }
          if (doc === "Logo") {
            this.Registration.CompanyDetails.LogoLink = 'data:image/png;base64,' + btoa("" + data + "");
          }
          this.cd.detectChanges();
        }
        );
      }
      else {
        alert(".jpg, .jpeg, .png, .pdf support only")
      }
    }
  }
  getBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsBinaryString(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  RemoveImage(img: any, index: any) {
    if (img === "Pancard") {
      this.Registration.TaxationDetails.PanDocumentLink = "";
      this.panFile = "";
    }
    if (img === "Tax") {
      this.Registration.TaxationDetails.DocumentLink = "";
      this.taxFilename = "";
    }
    if (img === "Company") {
      this.Registration.CompanyDetails.DocumentLink = "";
      this.compFilename = "";
    }
    if (img === "Association") {
      this.Registration.Affilation[index].Documentlink = "";
      this.affFilename[index] = "";
    }
    if (img === "Logo") {
      this.Registration.CompanyDetails.LogoLink = "";
    }

    this.cd.detectChanges();
  }
}
