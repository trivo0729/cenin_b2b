import { Component, OnInit } from '@angular/core';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-agent-profile',
  templateUrl: './agent-profile.component.html',
  styleUrls: ['./agent-profile.component.css']
})
export class AgentProfileComponent implements OnInit {
  AdminID: number;
  isb2b: Boolean;
  login: Userdetails = new Userdetails();
  constructor(private cookie: CommonCookieService) { }

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie('login')) {
      this.login = JSON.parse(this.cookie.getcookie('login'));
      if (this.login.LoginDetail.UserType === "B2B")
        this.isb2b = true
      if (this.login.LoginDetail.UserType === "B2C")
        this.isb2b = false
    }
  }

  ngAfterViewInit() {
    debugger
    let position: HTMLElement = document.getElementById('register') as HTMLElement;
    position.style.marginTop = '0px';
    position.style.paddingRight = '0px';
    position.style.paddingLeft = '0px';
    let card: HTMLElement = document.getElementById('register_card') as HTMLElement;
    card.style.marginBottom = '0px';
  }

}
