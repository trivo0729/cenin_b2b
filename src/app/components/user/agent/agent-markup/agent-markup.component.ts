import { Component, OnInit } from '@angular/core';
import { MarkupService } from 'src/app/services/agent/markup.service';
import { RouteAuthorizationService } from 'src/app/services/commons/route-authorization.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-agent-markup',
  templateUrl: './agent-markup.component.html',
  styleUrls: ['./agent-markup.component.css']
})
export class AgentMarkupComponent implements OnInit {

  constructor(private markupService: MarkupService,
    private authservice: RouteAuthorizationService,
    private alert: AlertService
  ) { }
  arrMakup: any[];
  loading: boolean;
  // [
  //   { Service: "Flight", markup: "0.00", commision: 0.00 },
  //   { Service: "Hotel", markup: "0.00", commision: 0.00 },
  //   { Service: "Package", markup: "0.00", commision: 0.00 },
  // ]
  ngOnInit() {
    this.loading = false;
    this.arrMakup = [];
    this.getMarkups();
  }

  getMarkups() {
    debugger
    this.markupService.getMarkupByAgent(this.authservice.getUserId()).subscribe((response: any) => {
      if (response.retCode === 1) {
        this.arrMakup = response.arrMarkup
      }
    })
  }

  updateMarkup() {
    debugger
    this.loading = true;
    this.markupService.setMarkupByAgent(this.authservice.getUserId(), this.arrMakup).subscribe((response: any) => {
      this.loading = false;
      if (response.retCode === 1) {
        this.alert.confirm('Markup updated succesfully', 'success');
      }
      else
        this.alert.confirm('something went wrong', 'error');
    });
  }
}
