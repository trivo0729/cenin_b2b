import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AccountService } from 'src/app/services/agent/account.service';
import { Credits } from 'src/app/modals/agent/transaction.modal';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { process } from '@progress/kendo-data-query';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-credit-information',
  templateUrl: './credit-information.component.html',
  styleUrls: ['./credit-information.component.css']
})
export class CreditInformationComponent implements OnInit {
  userdetails: any;
  Credits: Credits[];
  constructor(private userService: UserService, private accountService: AccountService , private cookie : CommonCookieService) {
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.Credits = [];
    this.getUserdata();
  }

  getUserdata() {
    if (this.cookie.checkcookie('login')) {
      this.userdetails = JSON.parse(this.cookie.getcookie('login'));
    }
    this.getCreditInformation();
  }

  getCreditInformation() {
    if (this.userdetails !== null || this.userdetails !== undefined) {
      let userId = this.userdetails.LoginDetail.sid;
      this.accountService.GetCreditInformation(userId).subscribe((response: any) => {
        this.Credits = response;
        console.log(response);
      }, error => {
        console.log(error);
      })
    }
  }

  public allData(): ExcelExportData {
    const result: ExcelExportData = {
      data: process(this.Credits, { sort: [{ field: 'DepositDate', dir: 'desc' }] }).data,
    };
    return result;
  }

}
