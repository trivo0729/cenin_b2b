import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Userdetails, LoginDetail, ContacDetails } from 'src/app/modals/user/userdetails';
import { GenralService } from 'src/app/services/genral.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { AlertService } from 'src/app/services/alert.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  AdminID: number;
  public Userdetails: Userdetails = new Userdetails();
  LoginDetails: LoginDetail;
  loading: boolean;
  Country: any[] = [];
  arrCountry: Observable<any[]>;
  nationalityCtrl = new FormControl();

  constructor(private userService: UserService, private genralService: GenralService, private service: ApiUrlService, private Alert: AlertService, private cookie: CommonCookieService) {
    this.AdminID = service.AdminID;
  }

  ngOnInit() {
    this.loading = false;
    this.Userdetails.sParentID = this.service.AdminID;
    if (this.cookie.checkcookie('login')) {
      let userdetails = JSON.parse(this.cookie.getcookie('login'));
      if (userdetails !== null) {
        this.LoginDetails = userdetails.LoginDetail;
      }
    }
    this.getCountry();
  }

  UpdateProfile() {
    this.loading = true;
    this.userService.UpdateProfile(this.LoginDetails).subscribe(
      (res: any) => {
        this.loading = false;
        if (res.retCode == 1) {
          this.Userdetails.LoginDetail = this.LoginDetails;
          this.Userdetails.Message = "";
          this.Userdetails.retCode = 1;
          this.Userdetails.sParentID = this.service.AdminID;
          this.userService.setLogedIn(JSON.stringify(this.Userdetails));
          this.Alert.confirm('User Profile Updated Successfully', 'success');
        } else {
          this.Alert.confirm(res.Meassge, 'warning')
        }
      }, error => {
        console.log(error);
        this.loading = false;
      });
  }

  getCountry() {
    this.genralService.getCountry().subscribe((res: any) => {
      ;
      this.Country = res;
      this.arrCountry = this.nationalityCtrl.valueChanges
        .pipe(
          startWith(''),
          map(Country => Country ? this._filterCountry(Country) : this.Country.slice())
        );
      console.log(res);
    });
  }

  private _filterCountry(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.Country.filter(Country => Country.Countryname.toLowerCase().indexOf(filterValue) === 0);
  }
}
