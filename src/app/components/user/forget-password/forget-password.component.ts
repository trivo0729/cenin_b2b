import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { AlertService } from 'src/app/services/alert.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  fpassword: FormGroup;
  Email: string;
  submitted: boolean;
  loading: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ForgetPasswordComponent>,
    private Alert: AlertService,
    private service: ApiUrlService,
  ) { }

  ngOnInit() {
    this.submitted = false;
    this.loading = false;
    this.fpassword = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  get f() { return this.fpassword.controls; }

  onForgot() {
    this.submitted = true;
    this.loading = true;
    // stop here if form is invalid
    if (this.fpassword.invalid) {
      this.loading = false;
      return;
    }
    else {
      this.sendPassword();
    }
  }

  sendPassword() {
    this.userService.forgetPassword(this.Email, this.service.AdminID, this.data.userType).subscribe((response: any) => {
      this.loading = false;
      if (response.retCode === 1) {
        this.dialogRef.close();
        this.Alert.confirm('Password sent your registered mail id ', 'success');
      } else {
        this.Alert.confirm(response.Message, 'warning')
      }
      console.log(response);
    }, error => {
      this.loading = false;
      console.log(error);
    })
  }

}
