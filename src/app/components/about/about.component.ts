import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  AdminCode : any;
  constructor(
    public objGlobalService :ApiUrlService
  ) { }

  ngOnInit() {
    this.AdminCode = this.objGlobalService.Code;
  }

}
