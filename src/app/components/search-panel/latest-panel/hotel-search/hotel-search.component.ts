import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { HotelSearch } from 'src/app/modals/hotel/hotel.modal';
import { objSearch, Destination, Occupancy, Childs, objTraveller } from 'src/app/modals/hotel/search.modal';
import { debounceTime, tap, switchMap, finalize, startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GenralService } from 'src/app/services/genral.service';
import { FormControl } from '@angular/forms';
import { HotelService } from 'src/app/services/hotel/hotel.service';
import { AlertService } from 'src/app/services/alert.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
declare function dropPanel(): any;

@Component({
  selector: 'app-hotel-search',
  templateUrl: './hotel-search.component.html',
  styleUrls: ['./hotel-search.component.css']
})
export class HotelSearchComponent implements OnInit {
  @Input() objTraveller: objTraveller;
  @Input() Traveller: any;
  @Input() Code: any;
  @Output() setGuests = new EventEmitter();

  selected = '';
  UserName: String;
  Password: string;
  Valid = false;
  arrCountry: Observable<any[]>;
  nationalityCtrl = new FormControl();
  searchMoviesCtrl = new FormControl();
  hotelcity: HotelSearch[];
  isLoading = false;
  errorMsg: string;
  Country: any[] = [];
  objSearch: objSearch;
  Destination: Destination;
  Occupancy: Occupancy[];
  autoApply: boolean;
  closeOnAutoApply: boolean;
  minDate: any;
  showDropdowns: boolean;
  lockStartDate: boolean;
  opens: string;
  drops: string;
  Guests: string;
  userdata: any;


  TotalGuest: number;
  public Ages: Array<any>;
  Childs: Childs[];
  plus: string = 'plus';
  minus: string = 'minus';

  constructor(private genralService: GenralService,
    private router: Router,
    public alert: AlertService,
    private cookie: CommonCookieService,
    private hotelService: HotelService,
    private cd: ChangeDetectorRef,
    public ServiceUrl: ApiUrlService) {
    this.UserName = this.ServiceUrl.UserName;
    this.Password = this.ServiceUrl.Password;
    this.opens = 'left';
    this.drops = 'down';
    this.TotalGuest = 0;
  }

  ngOnInit() {
    this.objSearch = new objSearch();
    this.Guests = '1 Room, ' + '2 Guests';
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.getTravellers();
    var min = new Date();
    this.minDate = moment(min);
    this.getHotelCity();
    this.getCountry();
    this.getUserdata();
  }

  ngAfterViewInit() {
    let chk: any = document.getElementsByClassName('ltr');
    chk[0].style.left = '-320px';
    dropPanel();
  }


  Addrooms() {
    this.Childs = [];
    this.Traveller.push({ adults: 2, childs: this.Childs });
    this.getTravellers();
  }

  RemoveRoom() {
    this.Traveller.pop();
    this.getTravellers();
  }

  public getTravellers() {
    this.TotalGuest = 0;
    this.Traveller.forEach(element => {
      this.TotalGuest += element.adults;
      this.TotalGuest += element.childs.length;
    });

    this.Guests = this.Traveller.length + ' Room, ' + this.TotalGuest + ' Guests'
    // this.setGuests.emit(this.Traveller.length + ' Room, ' + this.TotalGuest + ' Guests')
  }

  public changeTravellers(count: any, action: any, paxtype: string) {
    debugger;
    for (let i = 0; i < this.Traveller.length; i++) {
      if (i == count) {
        if (paxtype == 'ad') {
          if (action == 'plus') {
            if (this.Traveller[i].adults < 4) {
              this.Traveller[i].adults = this.Traveller[i].adults + 1
            }
          }
          if (action == 'minus') {
            if (this.Traveller[i].adults > 1) {
              this.Traveller[i].adults = this.Traveller[i].adults - 1
            }
          }
        }
        if (paxtype == 'ch') {
          if (action == 'plus') {
            if (this.Traveller[i].childs.length < 4) {
              this.Traveller[i].childs.push({ count: 1, childAge: 2, ages: this.Ages });
            }
          }
          if (action == 'minus') {
            if (this.Traveller[i].childs.length != 0) {
              this.Traveller[i].childs.pop();
            }
          }
        }
      }
    }
    this.cd.detectChanges();
    this.getTravellers();
  }


  getUserdata() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.userdata = {
          userName: data.LoginDetail.Email,
          password: data.LoginDetail.Password,
          parentID: this.ServiceUrl.AdminID
        }
      }
      if (data.LoginDetail.UserType === "B2C") {
        this.userdata = {
          userName: this.ServiceUrl.UserName,
          password: this.ServiceUrl.Password,
          parentID: this.ServiceUrl.AdminID
        }
      }
    }
    else {
      this.userdata = {
        userName: this.ServiceUrl.UserName,
        password: this.ServiceUrl.Password,
        parentID: this.ServiceUrl.AdminID
      }
    }
  }

  Guest(Guests: string) {
    this.Guests = Guests;
  }

  getCountry() {
    this.genralService.getCountry().subscribe((res: any) => {
      debugger;
      this.Country = res;
      this.selected = this.objTraveller.Nationality;
      this.cd.detectChanges();
      this.arrCountry = this.nationalityCtrl.valueChanges
        .pipe(
          startWith(''),
          map(Country => Country ? this._filterCountry(Country) : this.Country.slice())
        );
    });
  }

  private _filterCountry(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.Country.filter(Country => Country.Countryname.toLowerCase().indexOf(filterValue) === 0);
  }


  getHotelCity() {
    this.searchMoviesCtrl.valueChanges
      .pipe(
        tap(() => {
          this.errorMsg = "";
          this.hotelcity = [];
          this.isLoading = true;
        }),
        switchMap(value => this.hotelService.getHotelLocation(value)
          .pipe(
            finalize(() => {
              this.isLoading = false
            }),
          )
        )
      )
      .subscribe((res: HotelSearch[]) => {
        this.hotelcity = res;
      });
  }

  selectedCity(value: any) {
    debugger
    this.Code = value.id;
  }



  change(event: any) {
    debugger;
    var days = this.Getdays(event.start, event.end);
    if (days > 30)
      this.alert.confirm('you can not stay more then 30 nights', 'warning');
    this.objTraveller.Date = {
      start: moment(event.start),
      end: moment(event.end),
    }
  }

  SearchHotelParams() {
    debugger;
    this.Valid = this.validate()
    if (this.Valid) {
      this.SearchHotel();
      // this.hotelService.getHotelLocation(this.objTraveller.Location.split(',')[0]).subscribe((res: any) => {
      //   this.SearchHotel(res[0].id)
      // })
    }
  }

  SearchHotel() {
    this.objSearch.HotelCode = [];
    this.Destination = new Destination();
    this.Destination.City = this.objTraveller.Location.split(',')[0];
    this.Destination.Country = this.objTraveller.Location.split(',')[1].trim();
    this.Destination.Code = this.Code;
    this.objSearch.Destination = this.Destination;
    this.objSearch.Checkin = moment(this.objTraveller.Date.start).format('DD-MM-YYYY');
    this.objSearch.Checkout = moment(this.objTraveller.Date.end).format('DD-MM-YYYY');
    this.objSearch.nationality = this.objTraveller.Nationality;
    this.objSearch.Nights = this.Getdays(this.objTraveller.Date.start, this.objTraveller.Date.end);
    this.objSearch.Adults = 0;
    this.objSearch.Childs = 0;
    this.objSearch.Supplier = "";
    this.objSearch.MealPlan = "";
    this.objSearch.CurrencyCode = "";
    this.objSearch.SearchType = "";
    this.objSearch.username = this.userdata.userName;
    this.objSearch.Password = this.userdata.password;
    this.objSearch.TockenID = '';
    this.objSearch.UserIP = '';
    this.objSearch.Rooms = [];
    this.Occupancy = [];
    for (let i = 0; i < this.Traveller.length; i++) {
      var ChildAges = []
      if (this.Traveller[i].childs.length != 0) {
        for (let j = 0; j < this.Traveller[i].childs.length; j++) {
          ChildAges.push(parseInt(this.Traveller[i].childs[j].childAge))
        }
      }
      // else {
      //   ChildAges.push(0);
      // }
      this.Occupancy.push({ RoomCount: i + 1, AdultCount: this.Traveller[i].adults, ChildCount: this.Traveller[i].childs.length, ChildAges: ChildAges });
      this.objSearch.Adults += this.Traveller[i].adults;
      this.objSearch.Childs += this.Traveller[i].childs.length;
    }

    this.objSearch.Rooms = this.Occupancy;

    this.hotelService.setHotelSerachParams(this.objSearch);

    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url;

    if (currentUrl === '/hotel-list') {
      this.router.navigateByUrl(currentUrl)
        .then(() => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        });
    }
    else
      this.router.navigate(['/hotel-list']);
  }

  validate() {
    if (this.objTraveller.Location == "" || this.objTraveller.Location == undefined) {
      this.alert.confirm('Please Enter City Name', 'warning')
      return false;
    }
    else
      this.Valid = true;
    if (this.objTraveller.Date == "" || this.objTraveller.Date == undefined) {
      this.alert.confirm('Please pick a Date', 'warning')
      return false;
    }
    else
      this.Valid = true;

    var days = this.Getdays(this.objTraveller.Date.start, this.objTraveller.Date.end);
    if (days > 30) {
      this.alert.confirm('you can not stay more then 30 nights', 'warning')
      return false;
    }
    if (this.objTraveller.Nationality == "" || this.objTraveller.Nationality == undefined) {
      this.alert.confirm('Please Select Nationality', 'warning')
      return false;
    }
    else
      this.Valid = true;

    return this.Valid
  }

  Getdays(start: any, end: any) {
    var startdate = moment(start).toDate();
    var enddate = moment(end).toDate();
    const days = (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24);
    return parseInt(days.toString());
  }

  click() {

  }

}
