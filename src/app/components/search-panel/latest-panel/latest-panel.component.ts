import { Component, OnInit } from '@angular/core';
import { Destinations, objSearch } from 'src/app/modals/flight/flight-search.modal';
import { objTraveller, Traveller, Childs } from 'src/app/modals/hotel/search.modal';
import { TourTypes, PackageCities, objTour } from 'src/app/modals/tours.modals';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { MenuService } from 'src/app/services/commons/menu.service';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { ApiUrlService } from 'src/app/services/api-url.service';

import * as moment from 'moment';

@Component({
  selector: 'app-latest-panel',
  templateUrl: './latest-panel.component.html',
  styleUrls: ['./latest-panel.component.css']
})
export class LatestPanelComponent implements OnInit {

  menu: Menulist[];
  _homeTabs: Menulist[];
  UsetId: number;

  // Flight variables
  JourneyType: string;
  Class: string;
  JourneyDate: any;
  singleDatePicker: boolean;
  Destinations: Destinations[];
  Search: objSearch;
  TotelGuest: number;
  Direct: boolean;

  // Hotel variables
  Traveller: Traveller[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];

  // Sightseeing variables

  objTour: objTour;

  constructor(private menuService: MenuService,
    private cookie: CommonCookieService,
    public objGlobalService: ApiUrlService,
  ) { }

  ngOnInit() {

    // For Flight

    this.Destinations = [];
    this.JourneyType = "1";
    this.Class = "All";
    this.singleDatePicker = true;
    var start = new Date();
    this.JourneyDate = moment(start);
    this.Destinations.push(
      {
        Origin: '',
        OriginCode: '',
        Destination: '',
        DestinationCode: '',
        JourneyDate: this.JourneyDate,
        MinDate: moment(start),
        OriginAirports: [],
        DestinationAirports: []
      }
    );
    this.Search = new objSearch();
    this.Search.AdultCount = 1;
    this.Search.ChildCount = 0;
    this.Search.InfantCount = 0;
    this.TotelGuest = 1;
    this.Direct = false;


    // For Hotel
    this.Traveller = [];
    this.Childs = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objTraveller = new objTraveller();
    this.objTraveller.Location = '';
    this.objTraveller.Guests = "";
    var start = new Date();
    var end = new Date();
    end.setDate(end.getDate() + 1);
    this.objTraveller.Date = {
      start: moment(start),
      end: moment(end),
    }
    this.Traveller.push({ adults: 2, childs: this.Childs });

    // for sightseeing

    this.objTour = new objTour();
    this.objTour.Location = '';
    this.objTour.LocationName = "";
    this.objTour.TourType = "";
    var date = new Date();
    this.objTour.Date = moment(date, 'DD-MM-YYYY');
    this.objTour.TourType = '';
    this.getTabs();
  }



  getTabs() {
    debugger
    this._homeTabs = [];
    this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
      this.menu = res;
      this.generateTabs();
    }, error => {
      console.log(error);
    });
  }

  generateTabs() {
    this.menu.forEach(menu => {
      if (menu.Name === 'Flight' || menu.Name === 'Hotel' || menu.Name === 'Sightseeing' || menu.Name === 'Package')
        this._homeTabs.push(menu);
    });
    this._homeTabs = this._homeTabs.sort((a, b) => a.Name > b.Name ? 1 : -1);
  }

  getUserId() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.objGlobalService.UserId;
        }
      }
    }
    else {
      this.UsetId = this.objGlobalService.UserId;
    }
    return this.UsetId;
  }

}
