import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { arrLocation, TourTypes, Search_Params, objTour } from 'src/app/modals/tours.modals';
import { ToursService } from 'src/app/services/tours.service';
import { Router } from '@angular/router';
import { map, startWith } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';
import { AlertService } from 'src/app/services/alert.service';
import * as moment from 'moment';

@Component({
  selector: 'app-sightseeing-search',
  templateUrl: './sightseeing-search.component.html',
  styleUrls: ['./sightseeing-search.component.css']
})
export class SightseeingSearchComponent implements OnInit {
  @Input() objTour: objTour;
  stateCtrl = new FormControl();
  filteredStates: Observable<arrLocation[]>;
  Locations: any[];
  Adults: number = 1;
  Childs: number = 0;
  value: string = "";
  TourTypes: TourTypes[];
  opens: string;
  drops: string;
  autoApply: boolean;
  singleDatePicker: boolean;
  minDate: any;

  constructor(private tourService: ToursService,
    private router: Router,
    private sessionService: SessionService,
    public alert: AlertService
  ) {
    this.Adults = 1;
    this.Childs = 0;
    this.opens = 'down';
    this.singleDatePicker = true;
    this.drops = 'down';
  }
  ngOnInit() {
    debugger;
    this.Search_Params.LocationID = this.objTour.Location;
    this.Search_Params.LocationName = this.objTour.LocationName;
    this.Search_Params.TourType = this.objTour.TourType;

    this.Locations = [];
    this.tourService.getLocation('').subscribe((res: arrLocation[]) => {
      this.Locations = res;
      this.filteredStates = this.stateCtrl.valueChanges
        .pipe(
          startWith(''),
          map(Location => Location ? this._filterStates(Location) : this.Locations.slice())
        );
    });
    var min = new Date();
    this.minDate = moment(min);
    
  }
  onchangeLocation(sLocation: string) {
    debugger;
    var location = this.Locations.find(l => l.value === sLocation);
    console.log(sLocation);
    this.Search_Params.LocationName = location.value;
    this.Search_Params.LocationID = location.id.toString();
    this.tourService.getAllTourtype(location.id.toString()).subscribe((res: TourTypes[]) => {
      this.TourTypes = res;
      console.log(res);
    });
  }
  private _filterStates(value: string): arrLocation[] {
    const filterValue = value.toLowerCase();
    return this.Locations.filter(Location => Location.value.toLowerCase().indexOf(filterValue) === 0);
  }
  myForm = new FormGroup({})
  Search_Params: Search_Params = new Search_Params();

  change(event: any) {
    debugger;
    if (event.startDate !== null && event.endDate !== null)
      this.objTour.Date = moment(event.startDate);
  }

  SearchActivity() {
    debugger;
    this.sessionService.clear("SearchParams");
    /* LocationID */
    this.Search_Params.sDate = moment(this.objTour.Date).format('DD-MM-YYYY');
    //this.Search_Params.TourType =  "";
    if (this.Search_Params.LocationID == "") {
      this.alert.confirm('Please Enter Location', 'warning')
      return false;
    }
    else if (this.objTour.Date == "" || this.objTour.Date == undefined) {
      this.alert.confirm('Please Enter Sighseeing Date', 'warning')
      return false;
    }
    // if (this.Locations != undefined) {
    //  this.Locations.forEach(Location =>{
    //     if (Location.LocationName == this.Search_Params.LocationID) {
    //       this.Search_Params.TourType = this.TourTypes.toString();
    //     }
    //  });  
    // }
    if (this.Locations != undefined) {
      this.Locations.forEach(Location => {
        if (Location.LocationName == this.Search_Params.LocationID) {
          this.Search_Params.LocationID = Location.Lid.toString();
        }
        this.Search_Params.LocationName = Location.LocationName;

      });
    } else
      this.Search_Params.LocationID = "";
    this.tourService.setSerachParams(this.Search_Params);
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url;

    if (currentUrl === '/tours-list') {
      this.router.navigateByUrl(currentUrl)
        .then(() => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        });
    }
    else
      this.router.navigate(['/tours-list']);
  }
  AddPax(Type: string) {
    debugger
    if (Type == "Adults") {
      this.Search_Params.Adults += this.Search_Params.Adults + 1;
    }
    else {
      this.Search_Params.Child += this.Search_Params.Child + 1;
    }
  }

  checklocation() {
    debugger;

    if (this.objTour.LocationName == "") {
      this.alert.confirm('Please Enter Location', 'warning')
      return false;
    }

    // if(this.Search_Params.LocationID =="")
    // {
    //   this.alert.confirm('Please Enter Location','warning')
    //   return false;
    // }


  }

  onselected(Name: string) {
    debugger;
    if (Name != undefined || Name != "") {
      this.Search_Params.TourType = Name;
      this.objTour.TourType = Name;
    }
  }

  update(date: string) {
    debugger
    console.log(date)
    this.Search_Params.sDate = date;
  }

}
