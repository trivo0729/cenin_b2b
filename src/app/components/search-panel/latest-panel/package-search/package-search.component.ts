import { Component, OnInit } from '@angular/core';
import { PackageCities } from 'src/app/modals/tours.modals';
import { FormControl } from '@angular/forms';
import { PackageService } from 'src/app/services/packages/package.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-package-search',
  templateUrl: './package-search.component.html',
  styleUrls: ['./package-search.component.css']
})
export class PackageSearchComponent implements OnInit {

  PackageCity: PackageCities[];
  filterCities: Observable<PackageCities[]>;
  cityCtrl = new FormControl();
  constructor(private router: Router, private packageService: PackageService, 
    public alert: AlertService,
    
    ) { }

  ngOnInit() {
    this.packageService.getPackageLocation().subscribe((res: PackageCities[]) => {
      debugger;
      this.PackageCity = res;
      this.filterCities = this.cityCtrl.valueChanges
        .pipe(
          startWith(''),
          map(CityName => CityName ? this._filterCities(CityName) : this.PackageCity.slice())
        );
    });
  }


  private _filterCities(value: string): PackageCities[] {
    const filterValue = value.toLowerCase();
    return this.PackageCity.filter(CityName => CityName.City.toLowerCase().indexOf(filterValue) === 0);
  }

  getPackages(City: string) {
    debugger;
    if (City != "") {
      this.router.navigate(['/package-list'], { queryParams: { City: City } });
    }
    else {
      debugger;
      this.alert.confirm('Please Enter City Name', 'warning')
    }
  }

}
