import { Component, OnInit } from '@angular/core';
import { Package, Themes } from 'src/app/modals/packages/package.modal';
import { PackageService } from 'src/app/services/packages/package.service';
import { UserService } from 'src/app/services/user.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
declare function allTabs(): any;
declare function owl_Carousel(): any;
declare function allScript(): any;
declare function showTab(i: any): any;

@Component({
  selector: 'app-idealjungle-packagegrid',
  templateUrl: './idealjungle-packagegrid.component.html',
  styleUrls: ['./idealjungle-packagegrid.component.css']
})
export class IdealjunglePackagegridComponent implements OnInit {
  Package: Package[];
  TempPackage: Package[];
  minimum: Array<number>;
  count: number = 0;
  userdata: any;
  Themes: Themes[];
  constructor(private packageService: PackageService,
    private userService: UserService,
    private cookie: CommonCookieService,
    private ServiceUrl: ApiUrlService) { }
  ngOnInit() {
    debugger;

    this.Package = [];
    this.Themes = [];
    this.TempPackage = [];
    this.packageService.getPackage(this.userService.getUserdata()).subscribe((res: Package[]) => {
      this.TempPackage = res;
      this.Package = res;
      //console.log(this.Package);
      this.count = this.Package.length;
      this.Package = this.Package.slice(0, 6)
      this.SetMinimumPrice();
      this.getThemes(this.TempPackage);
      if (this.Themes.length != 0)
        this.showTabs(this.Themes[0].Name);
      setTimeout(() => {
        try {
          allTabs();
        } catch (e) { }
      }, 1000);

    });
  }
  SetMinimumPrice() {
    try {
      debugger;
      this.Package = [];
      this.TempPackage.forEach(arrPackage => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (this.minimum.length !== 0)
            arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
          else
            arrPackage.MinPrice = 0;
        }
        else {
          arrPackage.MinPrice = 0;
        }
        this.Package.push(arrPackage);
        this.Package = this.Package.slice(0, 6)
      });
    } catch (e) { console.log(e.message) }


  }
  
  

  getThemes(Packages: any[]) {
    debugger;
    Packages.forEach(arrPackage => {
      arrPackage.Themes.forEach(theme => {
        if (theme === "Wildlife") {
          if (this.Themes.filter(d => d.Name == theme).length == 0)
          this.Themes.push({ Name: theme, Count: 1, checked: false });
        else {
          this.Themes.filter(d => d.Name == theme).forEach(r => {
            r.Count = this.Themes.filter(d => d.Name == theme).length + 1;
          });
        }
        }
      });
    });
  }

  showTabs(theme: string) {
    this.Package = [];
    /* Theme */
    this.TempPackage.forEach(arrPackage => {
      if (arrPackage.Themes.filter(d => d == theme).length != 0)
        if (this.Package.indexOf(arrPackage) == -1)
          this.Package.push(arrPackage)
    });
    this.count = this.Package.length;
    this.Package = this.Package.slice(0, 6)
  }
  }

