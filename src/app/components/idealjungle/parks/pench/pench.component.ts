import { Component, OnInit } from '@angular/core';


declare function allScript(): any;
declare function owl_Carousel(): any;
declare function Carousel(): any;
declare function owl(): any;

@Component({
  selector: 'app-pench',
  templateUrl: './pench.component.html',
  styleUrls: ['./pench.component.css']
})
export class PenchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    allScript();
    owl_Carousel();
    Carousel();
    owl();
  }

}
