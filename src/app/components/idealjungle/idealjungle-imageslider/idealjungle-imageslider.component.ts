import { Component, OnInit } from '@angular/core';

declare function allScript() : any;
declare function RevSliderIdeal() : any; 
declare function Carousel(): any;
declare function owl(): any;

@Component({
  selector: 'app-idealjungle-imageslider',
  templateUrl: './idealjungle-imageslider.component.html',
  styleUrls: ['./idealjungle-imageslider.component.css']
})
export class IdealjungleImagesliderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    allScript();
    RevSliderIdeal();
    Carousel();
    owl();
  }

}
