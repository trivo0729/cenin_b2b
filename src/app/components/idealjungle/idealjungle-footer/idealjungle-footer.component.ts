import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-idealjungle-footer',
  templateUrl: './idealjungle-footer.component.html',
  styleUrls: ['./idealjungle-footer.component.css']
})
export class IdealjungleFooterComponent implements OnInit {
 
 
  constructor(public objGlobal : ApiUrlService) {

   }

  ngOnInit() {
  }

}
