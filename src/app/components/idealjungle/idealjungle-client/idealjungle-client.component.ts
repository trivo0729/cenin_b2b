import { Component, OnInit } from '@angular/core';

declare function allScript(): any;
declare function setimage(): any;

declare function Carousel(): any;
declare function RevSliderIdeal(): any;
declare function owl(): any;

@Component({
  selector: 'app-idealjungle-client',
  templateUrl: './idealjungle-client.component.html',
  styleUrls: ['./idealjungle-client.component.css']
})
export class IdealjungleClientComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    allScript();
    setimage();
    Carousel();
    RevSliderIdeal();
    owl();
  }

}
