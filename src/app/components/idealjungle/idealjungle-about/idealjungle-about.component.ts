import { Component, OnInit } from '@angular/core';

declare function allscript(): any;
declare function owl(): any;
declare function Carousel(): any;
declare function owalCarousel(): any;

@Component({
  selector: 'app-idealjungle-about',
  templateUrl: './idealjungle-about.component.html',
  styleUrls: ['./idealjungle-about.component.css']
})
export class IdealjungleAboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    allscript();
    owl();
    Carousel();
    owalCarousel();
  }

}
