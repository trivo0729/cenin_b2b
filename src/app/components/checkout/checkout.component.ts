import { Component, OnInit, Self } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { UserService } from 'src/app/services/user.service';
import { GenralService } from 'src/app/services/genral.service';
declare var Razorpay: any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Booking_Tour } from 'src/app/modals/tours.modals';
import { ToursService } from 'src/app/services/tours.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { Product, Cart } from 'src/app/modals/common/cart.model';
import { AlertService } from 'src/app/services/alert.service';
import { Userdetails, LoginDetail } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare function stricyBar(): any
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  isB2B: boolean = false;
  arrCountry: any[] = [];
  arrCity: any[] = [];
  products: any[] = [];
  Total: any;
  userdetails: Userdetails;
  User: LoginDetail;
  rzp1: any;
  options: any;
  registerForm: FormGroup;
  submitted = false;
  arrSightseeing: Booking_Tour[];
  Product: Booking_Tour = new Booking_Tour();
  arrProducts: Product[];
  arrCart: Cart = new Cart();
  Currency: string
  termsncondition: boolean;

  constructor(
    private shopCartService: ShoppingCartService,
    private userService: UserService,
    private genralService: GenralService,
    private tourService: ToursService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sessionService: SessionService,
    private alertService: AlertService,
    private cookie: CommonCookieService,
    private apiUrl: ApiUrlService,
    private ngxService: NgxUiLoaderService,
  ) { }
  ngOnInit() {
    debugger;
    this.termsncondition = false;
    this.getUserType();
    this.User = new LoginDetail();
    this.userdetails = new Userdetails();
    this.getUserDetails();
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', [Validators.required]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
    });

    this.getCountry();

    // this.products = this.shopCartService.get();
    try {
      debugger
      var arrDetails = this.sessionService.get("Cart");
      if (arrDetails != null)
        this.arrCart = arrDetails;
      if (this.arrCart.CartID != undefined) {
        this.shopCartService.get(this.arrCart.CartID).subscribe((res: any) => {
          if (res.retCode == 1) {
            this.Total = 0.00;
            this.products = [];
            this.arrProducts = res.CartProdDetailList;
            console.log(this.arrProducts)
            var i = 0, total = 0;
            this.arrProducts.forEach(arrProduct => {

              if (arrProduct.CartStatus)/*Check Expire Time */ {

                this.products.push(JSON.parse(arrProduct.ProductDetails));

                this.products[i].ProductID = arrProduct.ProductID;
                //this.products[i].PaxRate.charge = parseFloat(this.products[i].PaxRate.Rate).toFixed(2);
                total += parseFloat(this.products[i].PaxRate.Total);
                this.Currency = this.products[i].PaxRate.Currency;
                i++;
              }
              console.log(this.products)
            });
            this.Total = total;
            stricyBar();
          }
          else {
            this.sessionService.clear("Cart")
          }
          console.log(this.arrProducts);
        });
      }
    } catch (e) { }
  }
  getCountry() {
    this.genralService.getCountry().subscribe(
      (res: any) => {
        this.arrCountry = res;
      });
  }
  getCity(Country: string) {
    console.log(Country)
    this.genralService.getCity(Country).subscribe(
      (res: any) => {
        console.log(this.arrCity)
        this.arrCity = res;
      });
  }
  getUserType() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let userdata = JSON.parse(this.cookie.getcookie('login'));
      if (userdata.LoginDetail.UserType === 'B2B') {
        this.isB2B = true;
      }
    }
  }
  getUserDetails() {
    if (this.cookie.checkcookie('login')) {
      let user = JSON.parse(this.cookie.getcookie('login'));
      if (user !== null) {
        this.userdetails = user;
        this.User = user.LoginDetail;
      }
    }


  }

  changeTermsStatus() {
    if (this.termsncondition === false) {
      this.termsncondition = true;
    } else {
      this.termsncondition = false;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
  public initPay(IsCard: boolean) {
    debugger
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid || this.termsncondition === false) {
      return;
    }

    /* Get Product */
    var currency = "INR";
    this.arrSightseeing = [];
    this.products.forEach(sProduct => {
      if (sProduct.Type == "Tour") {
        var Adults = 1, child1 = 0, child2 = 0, Infant = 0;
        var Adult_Cost = 0, child1_Cost = 0, child2_Cost = 0, Infant_Cost = 0;
        var Total = 0;
        sProduct.PaxRate.arrayPax.forEach(Pax => {
          Total += Pax.Count * Pax.Rate;
          if (Pax.Pax_Type == "adult") {
            Adults = Pax.Count;
            Adult_Cost = Pax.Rate;
          }
          else if (Pax.Pax_Type == "child1") {
            child1 = Pax.Count;
            child1_Cost = Pax.Rate;
          }
          else if (Pax.Pax_Type == "child2") {
            child2 = Pax.Count;
            child2_Cost = Pax.Rate;
          }
          else if (Pax.Pax_Type == "infant") {
            Infant = Pax.Count;
            Infant_Cost = Pax.Rate;
          }
        });
        this.Product = new Booking_Tour();
        this.Product.Activity_Id = sProduct.ID;
        this.Product.Passenger_Name = this.User.Name + " " + this.User.LastName;
        this.Product.Email = this.User.Email;
        this.Product.Activity_Type = sProduct.Type;
        this.Product.Contact_No = this.User.ContacDetails.Mobile;
        this.Product.Adults = Adults;
        this.Product.Adult_Cost = Adult_Cost;
        this.Product.Child1 = child1;
        this.Product.Child1_Cost = child1_Cost;
        this.Product.Child2 = child2;
        this.Product.Child2_Cost = child2_Cost;
        this.Product.Booking_Date = sProduct.sDate;
        this.Product.Comment = "";
        this.Product.Discount = 0;
        this.Product.Driver_Id = 0;
        this.Product.Drop_Location = "";
        this.Product.Extras_Addon = 0;
        this.Product.Infant = Infant;
        this.Product.Infant_Cost = Infant_Cost;
        this.Product.InventoryType = "FreeSale";
        this.Product.Invoice_No = "";
        this.Product.IsPaid = true;
        this.Product.PaymentTransaction_Id = "";
        this.Product.Pickup_Location = "";
        this.Product.Vehicle_Id = 0;
        this.Product.Voucher_No = "";
        this.Product.ParentID = this.apiUrl.AdminID;
        this.Product.SupplierId = this.apiUrl.AdminID;
        this.Product.Sightseeing_Date = sProduct.sDate;
        this.Product.Status = "";
        this.Product.SlotID = sProduct.SlotID;
        this.Product.Ticket_Type = sProduct.TicketID;
        this.Product.Activity_Type = sProduct.TicketType;
        this.Product.BookingType = this.User.UserType;
        this.Product.PlanID = sProduct.PaxRate.PlanId;
        this.Product.TotalAmount = Total;
        this.Product.TotalPax = Adults + child1 + child2 + Infant;
        this.Product.User_Id = this.User.sid.toString() /*Click Ur Trip Agent */
        this.arrSightseeing.push(this.Product);
      }
    });
    /* Cart ID */
    var CartID = "";
    var arrDetails = this.sessionService.get("Cart");
    if (arrDetails != null)
      CartID = arrDetails.CartID;
    else {
      this.alertService.confirm("Invalid Booking", "error");
      return false;
    }
    if (IsCard) {
      const self = this;
      var total = this.Total;
       total = (parseFloat(total) * 100).toFixed(2).toString();
      this.options = {
        "key": self.apiUrl.RazorpayKey,
        "currency": currency,
        "amount":total , // 2000 paise = INR 20
        "name": self.apiUrl.CompanyName,
        "description": "Purchase Description",
        "image": self.apiUrl.logo,
        "handler": function (response) {
          // alert(response.razorpay_payment_id);
          self.Book(IsCard, self.arrSightseeing, CartID, response.razorpay_payment_id)
        },
        "prefill": {
          "name": this.User.Name,
          "email": this.User.Email,
          "mobile": this.User.ContacDetails.Mobile,
        },
        "notes": {
          "address": this.User.ContacDetails.Address
        },
        "theme": {
          "color": "#00a1ff"
        }
      };
      this.rzp1 = new Razorpay(this.options);
      this.rzp1.open();
    }
    else {
      this.Book(IsCard, this.arrSightseeing, CartID, '')
    }
  }

  public Book(IsCard: boolean, arrSightseeing: any, CartID: any, RazorPay: string): void {
    this.ngxService.start();
    var arrParams: any[] = [];
    if (IsCard) {
      arrParams.push("RazorPay");
      arrParams.push(RazorPay);
    }
    this.tourService.BookTours(arrSightseeing, CartID, arrParams).subscribe((res: any) => {
      if (res.retCode == 1) {
        this.ngxService.stop();
        this.sessionService.clear("Cart");
        this.alertService.confirm("Your Booking is Confirmed ,tickets are sent on your  email address", "success");
        setTimeout(() => {
           window.location.href = "/booking-report" 
          }, 1000)
      }
      else {
        this.alertService.confirm("Please Contact Adminstartor for this Booking", "error");
      }
    });
  }
}
