import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-exotics-footer',
  templateUrl: './exotics-footer.component.html',
  styleUrls: ['./exotics-footer.component.css']
})
export class ExoticsFooterComponent implements OnInit {

  constructor( public objGlobal : ApiUrlService) { }

  ngOnInit() {
  }

}
