import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExoticsPrivacypolicyComponent } from './exotics-privacypolicy.component';

describe('ExoticsPrivacypolicyComponent', () => {
  let component: ExoticsPrivacypolicyComponent;
  let fixture: ComponentFixture<ExoticsPrivacypolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExoticsPrivacypolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoticsPrivacypolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
