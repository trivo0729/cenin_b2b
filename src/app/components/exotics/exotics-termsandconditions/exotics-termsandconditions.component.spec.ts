import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExoticsTermsandconditionsComponent } from './exotics-termsandconditions.component';

describe('ExoticsTermsandconditionsComponent', () => {
  let component: ExoticsTermsandconditionsComponent;
  let fixture: ComponentFixture<ExoticsTermsandconditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExoticsTermsandconditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoticsTermsandconditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
