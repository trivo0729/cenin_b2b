import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExoticsPackageComponent } from './exotics-package.component';

describe('ExoticsPackageComponent', () => {
  let component: ExoticsPackageComponent;
  let fixture: ComponentFixture<ExoticsPackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExoticsPackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoticsPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
