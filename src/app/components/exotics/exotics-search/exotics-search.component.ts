import { Component, OnInit } from '@angular/core';
import { PackageService } from 'src/app/services/packages/package.service';
import { ToursService } from 'src/app/services/tours.service';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuService } from 'src/app/services/commons/menu.service';
import { AlertService } from 'src/app/services/alert.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Traveller, objTraveller, Childs } from 'src/app/modals/hotel/search.modal';
import * as moment from 'moment';
import { objTour, TourTypes } from 'src/app/modals/tours.modals';
import { UserService } from 'src/app/services/user.service';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
@Component({
  selector: 'app-exotics-search',
  templateUrl: './exotics-search.component.html',
  styleUrls: ['./exotics-search.component.css']
})
export class ExoticsSearchComponent implements OnInit {
  objTour: objTour;
  arrPopulars: TourTypes[];
  UsetId: number;
  Traveller: Traveller[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];
  menu: Menulist[];
  _homeTabs: Menulist[];
  constructor(private packageService: PackageService,
    private tourService: ToursService,
    private userService: UserService,
    private router: Router, private toastr: ToastrService,
    private menuService: MenuService,
    public alert: AlertService,
    private cookie: CommonCookieService,
    public objGlobalService: ApiUrlService) {


  }

  ngOnInit() {
    this.objTour = new objTour();
    this.objTour.Location = '';
    this.objTour.Date = {
      Date: moment(this.objTour.Date, 'DD-MM-YYYY')
    }
    this.objTour.TourType = '';
    this.tourService.getPopularTours().subscribe((res: any) => {
      this.arrPopulars = res.arrTourTypes;
      console.log(res);
    });
    /* set hotel travel data */
    this.Hoteltravel();
    this.getTabs();
  }
 
 
  Hoteltravel() {
    this.Traveller = [];
    this.Childs = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objTraveller = new objTraveller();
    this.objTraveller.Location = '';
    var start = new Date();
    var end = new Date();
    end.setDate(end.getDate() + 1);
    this.objTraveller.Date = {
      start: moment(start),
      end: moment(end),
    }
    this.Traveller.push({ adults: 1, childs: this.Childs });
  }

  getTabs() {
    this._homeTabs = [];
    if (this.cookie.checkcookie('menu')) {
      let s = this.cookie.checkcookie('menu');
      let data = JSON.parse(this.cookie.getcookie('menu'));
      if (data.UserId === this.getUserId()) {
        this.menu = data.Menu;
        this.generateTabs();
      }
      if (data.UserId !== this.getUserId()) {
        this.cookie.deletecookie('menu');
      }
    }
    if (!(this.cookie.checkcookie('menu'))) {
      this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
        this.menu = res;
        const menudetails = { UserId: this.getUserId(), Menu: this.menu }
        this.cookie.setcookie('menu', JSON.stringify(menudetails), 1);
        this.generateTabs();
      }, error => {
        console.log(error);
      });
    }
  }
  generateTabs() {
    this.menu.forEach(menu => {
      if (menu.Name === 'Hotel' || menu.Name === 'Sightseeing' || menu.Name === 'Package')
        this._homeTabs.push(menu);
    });
  }

  getUserId() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.objGlobalService.UserId;
        }
      }
    }
    else {
      this.UsetId = this.objGlobalService.UserId;
    }
    return this.UsetId;
  }
}
