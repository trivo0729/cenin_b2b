import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  AdminCode:any;
  constructor(
              public objGlobalService :ApiUrlService
             ) { }

  ngOnInit() {
    this.AdminCode = this.objGlobalService.Code;
  }
}
