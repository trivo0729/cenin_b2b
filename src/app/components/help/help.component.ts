import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  contact:string
  constructor(
    public objGlobalDefault : ApiUrlService
  ) { }
  ngOnInit() {
  }

}
