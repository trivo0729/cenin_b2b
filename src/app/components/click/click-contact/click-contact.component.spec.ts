import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickContactComponent } from './click-contact.component';

describe('ClickContactComponent', () => {
  let component: ClickContactComponent;
  let fixture: ComponentFixture<ClickContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
