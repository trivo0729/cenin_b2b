import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';

@Component({
  selector: 'app-click-footer',
  templateUrl: './click-footer.component.html',
  styleUrls: ['./click-footer.component.css']
})
export class ClickFooterComponent implements OnInit {

  constructor(
    public objGlobal: ApiUrlService,
  ) { }

  ngOnInit() {
  }

}
