import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Menulist } from 'src/app/modals/common/menu.modal';
import { MenuService } from 'src/app/services/commons/menu.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { ExchangeRate } from 'src/app/modals/dbhelper.model';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
import { arrBalance } from '../../trivo/trivo-header/trivo-header.component';
import { AccountService } from 'src/app/services/agent/account.service';
declare function Menu(): any;
@Component({
  selector: 'app-click-header',
  templateUrl: './click-header.component.html',
  styleUrls: ['./click-header.component.css']
})
export class ClickHeaderComponent implements OnInit {
  @Output() loginDialog = new EventEmitter();
  @Output() RegtrDialog = new EventEmitter();
  @Output() _logout = new EventEmitter();
  UsetId: number;
  @Input() login: any // Userdetails;
  arrBalance: arrBalance;
  public Currency: string = "AED";
  public exchange: ExchangeRate[];
  menu: Menulist[];
  _mainMenu: Menulist[];
  _userMenu: Menulist[];
  CustomerName: string = null;
  CustomerEmail: string = null;
  constructor(
    private menuService: MenuService,
    private ServiceUrl: ApiUrlService,
    private accountService: AccountService,
    public objGlobalService: ApiUrlService,
    private cookie: CommonCookieService
    
  ) { }

  ngOnInit() {
    this.arrBalance = new arrBalance();
    this.login = this.login
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.getManu();
        this.GetBalanceInformation(data.LoginDetail.sid)
      }
    }
    Menu();
  }

  GetBalanceInformation(uid: number) {
    this.accountService.GetBalanceInformation(uid).subscribe((res: any) => {
      if (res.retCode === 1) {
        this.arrBalance = res.arrBalance;
      }
    })
  }

  getManu() {
    this.menu = [];
    if (this.cookie.checkcookie('menu')) {
      let data = JSON.parse(this.cookie.getcookie('menu'));
      if (data.UserId === this.getUserId()) {
        this.menu = data.Menu;
        this.generateMenu();
      }
      if (data.UserId !== this.getUserId()) {
        this.cookie.deletecookie('menu');
      }
    }
    if (!(this.cookie.checkcookie('menu'))) {
      this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
        this.menu = res;
        const menudetails = { UserId: this.getUserId(), Menu: this.menu }
        this.cookie.setcookie('menu', JSON.stringify(menudetails), 1);
        this.generateMenu();
      }, error => {
        console.log(error);
      });
    }
  }

  getUserId() {
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.UsetId = data.LoginDetail.sid;
      }
    } else {
      this.UsetId = this.ServiceUrl.UserId;
    }
    return this.UsetId;
  }

  generateMenu() {
    this._mainMenu = [];
    this._userMenu = []
    this.menu.forEach(menu => {
      if (menu.Name === 'Home' || menu.Name === 'Hotel' || menu.Name === 'Flights' || menu.Name === 'Sightseeing' || menu.Name === 'Visa' || menu.Name === 'OTB' || menu.Name === 'Package')
        this._mainMenu.push(menu);
      if (menu.Name === 'Login')
        this._userMenu.push(menu)
    });
  }

  onSigninDialog() {
    this.loginDialog.emit();
  }

  onRegisterDialog() {
    this.RegtrDialog.emit();
  }

  onSignout() {
    this._logout.emit();
  }
}
