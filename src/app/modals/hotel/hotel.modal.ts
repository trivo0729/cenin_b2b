
// export interface Destination{
//     City: string;
//     Country: string
// }

// export interface Occupancy {
//     RoomCount: number;
//     AdultCount: number;
//     ChildCount: number;
//     ChildAges: number[];
// }

export class HotelSearch {
    id: number;
    value: string;
}

/* Hotel List Classes */

export interface HotelRespons {
    TockenID: string;
}

export class Hotels {
    HotelId: string;
    HotelName: string;
    Address: string;
    Category: string;
    Currency: string;
    Description: string;
    Facility: Facility[];
    Image: Image[];
    Langitude: any;
    Latitude: any;
    Location: Location[];
    UserRating: number;
    PreferedHotel: boolean;
    Priority: number;
    offer: boolean;
    MinPrice: string;
    MaxPrice: string;
    MinToolTip: null;
    MinNettPrice: null;
    MinNettToolTip: null;
    DateFrom: string;
    DateTo: string;
    Distance: any;
    Charge: Charge;
    Room: null;
    Price: number;
    CUTPrice: number;
    ListGst: null;
    FeeAtHotel: number;
    AgentMarkup: number;
    Supplier: string;
    List_RateGroup: ListRateGroup[];
    RoomOcupancy: null;
    sRooms: null;
    Night: number;
    TotalPayableToolTip: null;
    RateBreakups: null;
    ResultIndex: number;
    ContactNo: null;
    PostalCode: null;
    DotwCode: null;
    HotelBedsCode: null;
    MGHCode: null;
    GRNCode: null;
    ExpediaCode: null;
    GTACode: null;
    clicked: boolean;
    loading: boolean;
}

export interface Charge {
    Type: null;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency: null;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails: null;
    FranchiseeMarkup: number;
    FranchiseGSTdetails: null;
    FRCommission: number;
    FRTDS: number;
    HotelTaxes: any[] | null;
    HotelRate: number;
    RoomRate: number;
    CUHTaxes: any[] | null;
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes: any[] | null;
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes: any[] | null;
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export interface Facility {
    Name: string;
    Priority: number;
    icon: string;
    toolip: string;
    Checked: boolean;
    Count: number;
}

export interface Image {
    Type: null;
    Url: string;
    Title: null;
    IsDefault: boolean;
    Count: number;
}

export interface ListRateGroup {
    Name: string;
    AvailToken: string;
    Charge: Charge;
    CutPrice: number;
    AgentMarkup: number;
    RoomOccupancy: RoomOccupancy[];
    CategoryId: null;
    Nationality: string;
    _commonPrice: number;
}

export interface RoomOccupancy {
    RoomCount: number;
    RoomNo: number[];
    TotalRoomCount: number;
    AdultCount: number;
    ChildCount: number;
    ChildAges: string;
    GuestList: null;
    Rooms: Room[];
    MinPrice: number;
    MaxPrice: number;
    CategoryId: null;
}

export interface Room {
    Isbundle: boolean;
    Code: null;
    Boardcode: null;
    Roomcharacteristic: null;
    AdultCount: number;
    ChildCount: number;
    ChildAges: null;
    noRoom: number;
    RoomNo: number;
    RatePlanCode: null;
    RoomTypeId: string;
    RoomTypeName: string;
    RoomDescription: string;
    RoomDescriptionId: string;
    AvailCount: number;
    special: null;
    specialsApplied: number;
    SharingBedding: boolean;
    CancellationPolicy: CancellationPolicy[];
    RoomRateType: null;
    RoomRateTypeCurrency: string;
    RoomRateTypeCurrencyId: number;
    RoomAllocationDetails: null;
    minStay: null;
    dateApplyMinStay: null;
    CUTPrice: number;
    GSTdetails: null;
    MyProperty: number;
    AgentMarkup: number;
    tariffNotes: null;
    Total: number;
    LeftToSell: number;
    status: null;
    passengerNamesRequiredForBooking: number;
    validForOccupancy: null;
    changedOccupancy: null;
    dates: DateElement[];
    list_Aminity: null;
    ServiceTax: number;
    Currency: null;
    S2SMarkup: number;
    B2BMarkup: number;
    HotelTaxRates: any[];
    S2STaxRates: any[];
    B2BTaxRates: any[];
    ListCancel: string[];
    Cancellation_Code: null;
    objCharges: Charge;
    OnRequest: boolean;
    arrOffers: any[];
    arrMeals: any[];
    _roomCount: _arrRoom;
}

export class _arrRoom {
    _selected: any = 0;
    _rooms: any[];
}

export interface CancellationPolicy {
    nonRefundable: boolean;
    dayMin: null;
    dayMax: null;
    deduction: number;
    unit: null;
    CancellationAmount: number;
    CUTCancellationAmount: number;
    AgentCancellationMarkup: number;
    StaffCancellationMarkup: number;
    CutRoomAmount: number;
    AgentRoomMarkup: number;
    CancellationDateTime: string;
    AmendRestricted: boolean;
    cancelRestricted: boolean;
    CancelRestricted: boolean;
    ToolTip: null;
    CancellationPolicyCode: null;
    CancellationFee: null;
    arrChargeDate: DateElement[];
    SupplierMarkup: number;
    S2SMarkup: number;
    objCharges: ObjCharges;
}

export interface DateElement {
    runno: number;
    datetime: string;
    day: string;
    wday: null;
    price: number;
    Markup: number;
    Total: number;
    dayOnRequest: number;
    including: any[];
    discount: null;
    discountInfoText: null;
    InventoryType: null;
    Type: string;
    InvName: null;
    Currency: string;
    MealPlan: string;
    S2SMarkup: number;
    AdminMarkup: number;
    RoomRate: number;
    ChidWBedRate: number;
    CNBRate: number;
    EBRate: number;
    noCWB: number;
    noCNB: number;
    noEXB: number;
    offerID: null;
    RoomTypeId: number;
    RateTypeId: number;
    RatePlanId: number;
    NoOfInventory: any[];
    OfferRate: number;
    HotelRates: any[];
    S2CRates: any[];
    B2BRates: any[];
    NoOfCount: number;
    aarOffer: any[];
    UserCurrency: string;
    objCharges: Charge;
}

export interface ObjCharges {
    B2BMarkup: number;
    S2CMarkup: number;
    ListB2BTax: any[];
    ListS2CTax: any[];
    BaseRate: number;
    TotalRate: number;
    Charge: any[];
    objCharges: null;
}

export interface Location {
    Name: string;
    HotelCode: string;
    Supplier: null;
    Count: number;
}


/* Rate Group*/

export class SearchRateGroup {
    ResultIndex: number;
    TokenId: string
    Checkin: string;
    Checkout: string;
    nationality: string;
    SearchType: string;
    UserName: string;
    Password: string;
    Rooms: any;
}

export class RateList {
    HotelCode: any;
    RateGroupList: ListRateGroup;
}
