export class arrBlock {
    BookingDetails: BookingDetails[];
    TokenId: string;
    CustomerList: CustomerList[];
    ResultIndex: number;
    GroupIndex: number;
    Supplier: any
    Remark: string;
    Company: string;
    Mobile: string;
    Email: string;
}

export class BookingDetails {
    RoomTypeID: string;
    RoomDescription: string;
    Total: string;
    noRooms: number;
    AdultCount: number;
    ChildCount: number;
    ChildAges: string;
}

export interface CustomerList {
    Age: number;
    type: string;
    Name: string;
    LastName: string;
    Title: string;
    RoomNo: number;
}



// export class ArrReservation {
//     listRate: ListRate[];
//     TotalAmount: number;
//     objCharge: ObjCharge;
//     CancelattionAmount: number;
//     Dedline: string;
// }

export interface ListRate {
    Isbundle: boolean;
    Code?: any;
    Boardcode?: any;
    Roomcharacteristic?: any;
    AdultCount: number;
    ChildCount: number;
    ChildAges?: any;
    noRoom: number;
    RoomNo: number;
    RatePlanCode?: any;
    RoomTypeId: string;
    RoomTypeName: string;
    RoomDescription: string;
    RoomDescriptionId: string;
    AvailCount: number;
    special?: any;
    specialsApplied: number;
    SharingBedding: boolean;
    CancellationPolicy: CancellationPolicy[];
    RoomRateType?: any;
    RoomRateTypeCurrency: string;
    RoomRateTypeCurrencyId: number;
    RoomAllocationDetails?: any;
    minStay?: any;
    dateApplyMinStay?: any;
    CUTPrice: number;
    GSTdetails?: any;
    MyProperty: number;
    AgentMarkup: number;
    tariffNotes?: any;
    Total: number;
    LeftToSell: number;
    status?: any;
    passengerNamesRequiredForBooking: number;
    validForOccupancy?: any;
    changedOccupancy?: any;
    dates: Date[];
    list_Aminity?: any;
    ServiceTax: number;
    Currency?: any;
    S2SMarkup: number;
    B2BMarkup: number;
    HotelTaxRates: any[];
    S2STaxRates: any[];
    B2BTaxRates: any[];
    ListCancel: string[];
    Cancellation_Code?: any;
    objCharges: Charges;
    OnRequest: boolean;
    arrOffers: ArrOffer[];
    arrMeals: any[];
}

export interface CancellationPolicy {
    nonRefundable: boolean;
    dayMin?: any;
    dayMax: string;
    deduction: number;
    unit?: any;
    CancellationAmount: number;
    CUTCancellationAmount: number;
    AgentCancellationMarkup: number;
    StaffCancellationMarkup: number;
    CutRoomAmount: number;
    AgentRoomMarkup: number;
    CancellationDateTime: string;
    AmendRestricted: boolean;
    CancelRestricted: boolean;
    NoShowPolicy: boolean;
    ToolTip?: any;
    CancellationPolicyCode?: any;
    CancellationFee?: any;
    arrChargeDate: ArrChargeDate[];
    SupplierMarkup: number;
    S2SMarkup: number;
    objCharges: CancellationCharges;
}
export interface CancellationCharges {
    B2BMarkup: number;
    S2CMarkup: number;
    ListB2BTax: any[];
    ListS2CTax: any[];
    BaseRate: number;
    TotalRate: number;
    Charge: any[];
    objCharges?: any;
}

export interface Charges {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails?: any;
    FranchiseeMarkup: number;
    FranchiseGSTdetails?: any;
    FRCommission: number;
    FRTDS: number;
    HotelTaxes: any[];
    HotelRate: number;
    RoomRate: number;
    CUHTaxes: any[];
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes: any[];
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes: any[];
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export interface AarOffer {
    offerName: string;
    Dates: Date;
    OfferType: string;
    DisountAmount: number;
    DisountPercent: number;
    NewRate: number;
    OfferTerms: string;
    offerNote: string;
    FreeItem?: any;
    DayPrior?: any;
    MinNight?: any;
    FreeNight?: any;
    arrRate: any[];
}

export interface ObjCharges {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails?: any;
    FranchiseeMarkup: number;
    FranchiseGSTdetails?: any;
    FRCommission: number;
    FRTDS: number;
    HotelTaxes: any[];
    HotelRate: number;
    RoomRate: number;
    CUHTaxes: any[];
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes: any[];
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes: any[];
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export interface ArrChargeDate {
    runno: number;
    datetime: string;
    day: string;
    wday?: any;
    price: number;
    Markup: number;
    Total: number;
    dayOnRequest: number;
    including: any[];
    discount?: any;
    discountInfoText?: any;
    InventoryType?: any;
    Type: string;
    InvName?: any;
    Currency: string;
    MealPlan: string;
    S2SMarkup: number;
    AdminMarkup: number;
    RoomRate: number;
    ChidWBedRate: number;
    CNBRate: number;
    EBRate: number;
    noCWB: number;
    noCNB: number;
    noEXB: number;
    offerID: string[];
    RoomTypeId: number;
    RateTypeId: number;
    RatePlanId: number;
    NoOfInventory: any[];
    OfferRate: number;
    HotelRates: any[];
    S2CRates: any[];
    B2BRates: any[];
    NoOfCount: number;
    aarOffer: AarOffer[];
    UserCurrency: string;
    objCharges: ObjCharges;
}


export interface AarOffer2 {
    offerName: string;
    Dates: Date;
    OfferType: string;
    DisountAmount: number;
    DisountPercent: number;
    NewRate: number;
    OfferTerms: string;
    offerNote: string;
    FreeItem?: any;
    DayPrior?: any;
    MinNight?: any;
    FreeNight?: any;
    arrRate: any[];
}

export interface DateCharges {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails?: any;
    FranchiseeMarkup: number;
    FranchiseGSTdetails?: any;
    FRCommission: number;
    FRTDS: number;
    HotelTaxes: any[];
    HotelRate: number;
    RoomRate: number;
    CUHTaxes: any[];
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes: any[];
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes: any[];
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export interface Date {
    runno: number;
    datetime: string;
    day: string;
    wday?: any;
    price: number;
    Markup: number;
    Total: number;
    dayOnRequest: number;
    including: any[];
    discount?: any;
    discountInfoText?: any;
    InventoryType?: any;
    Type: string;
    InvName?: any;
    Currency: string;
    MealPlan: string;
    S2SMarkup: number;
    AdminMarkup: number;
    RoomRate: number;
    ChidWBedRate: number;
    CNBRate: number;
    EBRate: number;
    noCWB: number;
    noCNB: number;
    noEXB: number;
    offerID: string[];
    RoomTypeId: number;
    RateTypeId: number;
    RatePlanId: number;
    NoOfInventory: any[];
    OfferRate: number;
    HotelRates: any[];
    S2CRates: any[];
    B2BRates: any[];
    NoOfCount: number;
    aarOffer: AarOffer2[];
    UserCurrency: string;
    objCharges: DateCharges;
}


export interface ArrOffer {
    offerName: string;
    Dates: Date;
    OfferType: string;
    DisountAmount: number;
    DisountPercent: number;
    NewRate: number;
    OfferTerms: string;
    offerNote: string;
    FreeItem?: any;
    DayPrior?: any;
    MinNight?: any;
    FreeNight?: any;
    arrRate: any[];
}


export interface ObjCharge {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails?: any;
    FranchiseeMarkup: number;
    FranchiseGSTdetails: any[];
    FRCommission: number;
    FRTDS: number;
    HotelTaxes?: any;
    HotelRate: number;
    RoomRate: number;
    CUHTaxes?: any;
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes?: any;
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes?: any;
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}






