export class Voucher {
    retCode: number;
    arrResrvation: ArrResrvation;
    AgentDetails: AgentDetails;
    arrPaxes?: (GuestList)[] | null;
    arrRooms?: (Rooms)[] | null;
    dtHotelAdd?: (HotelDetails) | null;
    arrCurency?: string;
}

export class ArrResrvation {
    customer_Email: string;
    customer_name: string;
    VoucherNo: string;
    HotelConfirm?: null;
    Uid: number;
    Email: string;
    ContactPerson: string;
    CurrencyCode: string;
    SupplierName: string;
    SupplierMail: string;
    httplogo: string;
    SupplierID: string;
    HotelCode: string;
    HotelName: string;
    City: string;
    checkin: string;
    checkout: string;
    CancelDate?: null;
    Ammount: string;
    Nights: string;
    Passenger: string;
    InvoiceID: string;
    VoucherID: string;
    Bookingdate: string;
    Status: string;
    Type: string;
}
export class AgentDetails {
    sid?: null;
    adminID: number;
    Name: string;
    Code: string;
    Address: string;
    email: string;
    Mobile: string;
    phone: string;
    PinCode: string;
    Fax: string;
    sCountry?: null;
    Website: string;
    StateID: string;
    Countryname: string;
    City: string;
    ContactPerson: string;
    PassWord: string;
    Currency: string;
}

export class GuestList {
    sid: number;
    ReservationID: string;
    RoomCode: string;
    RoomNumber: string;
    Name: string;
    LastName: string;
    Age: number;
    PassengerType: string;
    IsLeading: boolean;
}
export class Rooms {
    sid: number;
    ReservationID: string;
    RoomCode: string;
    RoomType: string;
    RoomNumber: string;
    BoardText: string;
    MealPlanID: string;
    TotalRooms: number;
    LeadingGuest: string;
    Adults: number;
    Child: number;
    ChildAge?: null;
    Remark: string;
    CutCancellationDate: string;
    SupplierNoChargeDate: string;
    CancellationAmount: string;
    CanServiceTax: string;
    CanAmtWithTax: string;
    RoomAmount: number;
    RoomServiceTax?: null;
    RoomAmtWithTax: number;
    SupplierCurrency?: null;
    ExchangeRate?: null;
    SupplierAmount: number;
    TaxDetails: string;
    FranchiseeTaxDetails: string;
    FranchiseeMarkup: number;
    TDS: number;
    Discount?: null;
    Commision: number;
    EssentialInfo: string;
}
export class HotelDetails {
    Address: string;
    HotelName: string;
    Langitude: string;
    Latitude: string;
    Category: string;
    ContactNo?: any;
    PostalCode: string;
}