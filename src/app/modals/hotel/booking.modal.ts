export class arrConfirm {
    bookingDetails: bookingDetails[];
    tokenId: string;
    guestList: guestList[];
    resultIndex: number;
    groupIndex: number;
    remark: string;
    company: string;
    mobile: any;
    email: string;
    bCardPayment: boolean;
}

export interface bookingDetails {
    roomTypeID: any;
    roomDescription: string;
    total: any;
    noRooms: number;
    adultCount: number;
    childCount: number;
    childAges: string;
}

export class guestList {
    id: any;
    roomNumber: any;
    sCustomer: sCustomer[];
}

export class sCustomer {
    age: number;
    type: any;
    name: string;
    lastName: string;
    title: string;
    roomNo: number
}

export class _rooms {
    id: any;
    roomNumber: any;
    sCustomer: _Customers[];
}

export class _Customers {
    age: number;
    type: any;
    name: string;
    lastName: string;
    title: string;
    roomNo: number;
    srno: number;
}

export class arrParams {
    Email: string;
    PurchaseTocken: string;
    PayementGetWay: string;
}

export class arrCancel {
    ReservationID: string;
    UserDetail: UserDetail;
}

export class UserDetail {
    ParentID: number;
    Password: string;
    username: string;
}