export class BookingRequest {
    objFlightBooking: ObjFlightBooking;
    GstNo: string;
    GstCompanyName: string;
    GstCompanyContact: string;
    GstCompanyAddress: string;
    GstCompanyEmail: string;
    TokenId: string;
    arrParams?: (null)[] | null;
}

export class ObjFlightBooking {
    PreferredCurrency?: null;
    ResultIndexes?: (string)[] | null;
    AgentReferenceNo: string;
    Passengers?: (Passengers)[] | null;
    EndUserIp: string;
    TokenIded: string;
    TraceId: string;
}

export class Passengers {
    Title: string;
    FirstName: string;
    LastName: string;
    PaxType: number;
    DateOfBirth: string;
    Gender: number;
    PassportNo: string;
    PassportExpiry: string;
    AddressLine1: string;
    AddressLine2: string;
    Fare: Fare;
    City: string;
    CountryCode: string;
    CountryName: string;
    ContactNo: string;
    Email: string;
    IsLeadPax: boolean;
    FfAirlineCode: string;
    FfNumber: string;
    Baggage?: (null)[] | null;
    MealDynamic?: (null)[] | null;
    SeatDynamic?: (null)[] | null;
    GstCompanyAddress: string;
    GstCompanyContactNumber: string;
    GstCompanyName?: any;
    GstNumber?: any;
    GstCompanyEmail?: any;
}

export class Fare {
    BaseFare: number;
    Tax: number;
    YqTax: number;
    AdditionalTxnFeePub: number;
    AdditionalTxnFeeOfrd: number;
    OtherCharges: number;
}