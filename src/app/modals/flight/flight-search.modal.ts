export class FlightSearch {

}

export class objSearch {
    username: string;
    Password: string;
    EndUserIp: string;
    TokenId: string;
    AdultCount: any;
    ChildCount: any;
    InfantCount: any;
    DirectFlight: any;
    OneStopFlight: string;
    JourneyType: string;
    PreferredAirlines?: any;
    Segments: Segments[];
    Sources?: any;
}

export class Segments {
    Origin: string;
    OriginCity: string;
    Destination: string;
    DestinationCity: string;
    FlightCabinClass: string;
    PreferredDepartureTime: string;
    PreferredArrivalTime: string;
}

export class Sources {

}

export class Journey {
    name: string;
    id: string
}

export class Seat {
    name: string;
    id: string
}

export class FlightAutocomplete {
    id: string;
    value: string;
    AirportName: string;
}

export class Destinations {
    Origin: string;
    OriginCode: string;
    Destination: string;
    DestinationCode: string;
    JourneyDate: any;
    MinDate: any;
    OriginAirports: FlightAutocomplete[];
    DestinationAirports: FlightAutocomplete[];
}







