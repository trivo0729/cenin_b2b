

export class Airline {
    AirlineCode: string;
    AirlineName: string;
    FlightNumber: string;
    FareClass: string;
    OperatingCarrier: string;
}

export class Airport {
    AirportCode: string;
    AirportName: string;
    Terminal: string;
    CityCode: string;
    CityName: string;
    CountryCode: string;
    CountryName: string;
}

export class Origin {
    Airport: Airport;
    DepTime: any;
}


export class Destination {
    Airport: Airport;
    ArrTime: any;
}


export class Title {
    AirlineName: string;
    IsRequired: boolean;
}

export class FirstName {
    AirlineName: string;
    IsRequired: boolean;
}

export class LastName {
    AirlineName: string;
    IsRequired: boolean;
}

export class DOB {
    AirlineName: string;
    IsRequired: boolean;
}

export class Passport {
    AirlineName: string;
    IsRequired: boolean;
}

export class Address1 {
    AirlineName: string;
    IsRequired: boolean;
}

export class Address2 {
    AirlineName: string;
    IsRequired: boolean;
}

export class Email {
    AirlineName: string;
    IsRequired: boolean;
}

export class Mobile {
    AirlineName: string;
    IsRequired: boolean;
}

export class Country {
    AirlineName: string;
    IsRequired: boolean;
}

export class LeadingPax {
    Address1: Address1;
    Address2: Address2;
    Email: Email;
    Mobile: Mobile;
    Country: Country;
    Gstmandatory: boolean;
}

export class PaxDetail {
    Title: Title;
    FirstName: FirstName;
    LastName: LastName;
    DOB: DOB;
    Passport: Passport;
    PaxType: string;
    IsLeading: boolean;
    LeadingPax: LeadingPax;
    ResultIndex: string;
}

export class Seat {
    AirlineCode: string;
    FlightNumber: string;
    CraftType: string;
    Origin: string;
    Destination: string;
    AvailablityType: number;
    Description: number;
    Code: string;
    RowNo: string;
    SeatNo: string;
    SeatType: number;
    SeatWayType: number;
    Compartment: number;
    Deck: number;
    Currency: string;
    Price: number;
}

export class RowSeat {
    Seats: Seat[];
}

export class SegmentSeat {
    RowSeats: RowSeat[];
}

export class SeatDynamic {
    SegmentSeat: SegmentSeat[];
}

export class SsrService {
    Origin: string;
    Destination: string;
    DepartureTime: Date;
    AirlineCode: string;
    FlightNumber: string;
    Code: string;
    ServiceType: number;
    Text: string;
    WayType: number;
    Currency: string;
    Price: number;
}

export class SegmentSpecialService {
    SsrService: SsrService[];
}

export class SpecialService {
    SegmentSpecialService: SegmentSpecialService[];
}

export class SSR {
    Baggage: any[][];
    MealDynamic?: any[][];
    Meal?: any;
    TraceId: string;
    SeatDynamic: SeatDynamic[];
    SpecialServices: SpecialService[];
}

export class SSRRequest {
    SSR: SSR;
}

export class Validate {
    Segnments?: any;
    PaxDetails: PaxDetail[];
    Country?: any;
    Address?: any;
    Email?: any;
    PhoneNumber?: any;
    SSRRequest: SSRRequest;
}

export class Seat2 {
    AirlineCode: string;
    FlightNumber: string;
    CraftType: string;
    Origin: string;
    Destination: string;
    AvailablityType: number;
    Description: number;
    Code: string;
    RowNo: string;
    SeatNo: string;
    SeatType: number;
    SeatWayType: number;
    Compartment: number;
    Deck: number;
    Currency: string;
    Price: number;
}

export class RowSeat2 {
    Seats: Seat2[];
}

export class SegmentSeat2 {
    RowSeats: RowSeat2[];
}

export class SeatDynamic2 {
    SegmentSeat: SegmentSeat2[];
}

export class SsrService2 {
    Origin: string;
    Destination: string;
    DepartureTime: Date;
    AirlineCode: string;
    FlightNumber: string;
    Code: string;
    ServiceType: number;
    Text: string;
    WayType: number;
    Currency: string;
    Price: number;
}

export class SegmentSpecialService2 {
    SsrService: SsrService2[];
}

export class SpecialService2 {
    SegmentSpecialService: SegmentSpecialService2[];
}

export class arrResult {
    FlightDetails: FlightDetails[];
    Validate: Validate;
    JourneyType: string;
    IsDomestic: boolean;
    SSR: SSR[];
    Breackdowns: any[][];
    TotalPrice: string;
}

export interface FlightDetails {
    ResultIndex: string;
    Source: number;
    IsLcc: boolean;
    IsRefundable: boolean;
    GstAllowed: boolean;
    IsGstMandatory: boolean;
    AirlineRemark: string;
    Fare: Fare;
    FareBreakdown?: FareBreakdown[];
    Segments?: Segments[][];
    LastTicketDate: string;
    TicketAdvisory: string;
    FareRules?: FareRules[];
    AirlineCode: string;
    ValidatingAirline: string;
    SSR?: null;
}

export interface Fare {
    Charge: Charge;
    Currency: string;
    BaseFare: number;
    Tax: number;
    TaxBreakup?: TaxBreakupOrCharge[];
    YqTax: number;
    AdditionalTxnFeeOfrd: number;
    AdditionalTxnFeePub: number;
    PgCharge: number;
    OtherCharges: number;
    ChargeBu?: TaxBreakupOrCharge[];
    Discount: number;
    PublishedFare: number;
    CommissionEarned: number;
    PlbEarned: number;
    IncentiveEarned: number;
    OfferedFare: number;
    TdsOnCommission: number;
    TdsOnPlb: number;
    TdsOnIncentive: number;
    ServiceFee: number;
    TotalBaggageCharges: number;
    TotalMealCharges: number;
    TotalSeatCharges: number;
    TotalSpecialServiceCharges: number;
}

export interface TaxBreakupOrCharge {
    Key: string;
    Value: number;
}
export interface Charge {
    Type?: null;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: null;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails?: (null)[] | null;
    FranchiseeMarkup: number;
    FranchiseGSTdetails?: (null)[] | null;
    FRCommission: number;
    FRTDS: number;
    HotelTaxes?: null;
    HotelRate: number;
    RoomRate: number;
    CUHTaxes?: null;
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes?: null;
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes?: null;
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export interface FareBreakdown {
    Charge: Charge;
    Currency: string;
    PassengerType: number;
    PassengerCount: number;
    BaseFare: number;
    Tax: number;
    YqTax: number;
    AdditionalTxnFeeOfrd: number;
    AdditionalTxnFeePub: number;
    PgCharge: number;
}
export interface Segments {
    Baggage: string;
    CabinBaggage?: null;
    TripIndicator: number;
    SegmentIndicator: number;
    Airline: Airline;
    NoOfSeatAvailable: number;
    Origin: Origin;
    Destination: Destination;
    Duration: any;
    GroundTime: any;
    Mile: number;
    StopOver: boolean;
    StopPoint: string;
    StopPointArrivalTime?: string | null;
    StopPointDepartureTime?: string | null;
    Craft: string;
    Remark?: any;
    IsETicketEligible: boolean;
    FlightStatus: string;
    Status: string;
    Availability?: any;
    HaltingTime: any;
}

export interface FareRules {
    Origin: string;
    Destination: string;
    Airline: string;
    FareBasisCode: string;
    FareRuleDetail: string;
    FareRestriction: string;
}


export class Passengers {
    Title: string;
    FirstName: string;
    LastName: string;
    PaxType: number;
    DateOfBirth: string;
    Gender: number;
    PassportNo: string;
    PassportExpiry: string;
    AddressLine1: string;
    AddressLine2: string;
    Fare: Fare;
    City: string;
    CountryCode: string;
    CountryName: string;
    ContactNo: string;
    Email: string;
    IsLeadPax: boolean;
    FfAirlineCode: string;
    FfNumber: string;
    Baggage?: (null)[] | null;
    MealDynamic?: (null)[] | null;
    SeatDynamic?: (null)[] | null;
    GstCompanyAddress: string;
    GstCompanyContactNumber: string;
    GstCompanyName?: null;
    GstNumber?: null;
    GstCompanyEmail?: null;
}

export class Breackdowns {
    Breackdown: RateBreack[]
}

export class RateBreack {
    Currency: any;
    PassengerType: any;
    PassengerCount: any;
    Fare: any;
    Fees: any;
    Taxes: any;
    TaxComponent: any;
    Total: any;
    Baggage: any;
    Meal: any;
}



