export class FlightInvoice {
    retCode: number;
    arrResrvation: ArrResrvation;
    AdminUser: AdminUser;
    AgentDetails: AgentDetails;
    AmountInWord: string;
    Segment: Segment[];
    Pax: Pax[];
    Baggage: Baggage[];
    MealDynamic: MealDynamic[];
    fareDetails: FareDetail[];
}


export class AdminUser {
    sid: number;
    adminID: number;
    Name: string;
    Code: string;
    Address: string;
    email: string;
    Mobile: string;
    phone: string;
    PinCode: string;
    Fax: string;
    sCountry?: any;
    Website: string;
    StateID: string;
    Countryname: string;
    City: string;
    ContactID?: any;
    ContactPerson?: any;
    PassWord?: any;
    Currency?: any;
}

export class AgentDetails {
    sid?: any;
    adminID: number;
    Name: string;
    Code: string;
    Address: string;
    email: string;
    Mobile: string;
    phone: string;
    PinCode: string;
    Fax: string;
    sCountry: string;
    Website: string;
    StateID: string;
    Countryname: string;
    City: string;
    ContactID?: any;
    ContactPerson: string;
    PassWord: string;
    Currency: string;
}



export class ArrResult {
    sid: number;
    AgencyName: string;
    LeadingPaxName: string;
    AirlineName: any;
    AirlineCode?: any;
    PNR: string;
    Status: string;
    InvoiceAmount: string;
    PublishedFare: string;
    TicketStatus: string;
    BookingStatus?: any;
    InvoiceNo: string;
    Paxces: string;
    DestinationAirport?: any;
    OriginAirport?: any;
    DepartureDate: any
    BookingID: string;
    uid: string;
    Date: string;
}

export class ArrResrvation {
    sid: number;
    AgencyName: string;
    LeadingPaxName: string;
    AirlineName: any;
    AirlineCode: string;
    PNR: string;
    Status: string;
    InvoiceAmount: string;
    PublishedFare: string;
    TicketStatus: string;
    BookingStatus: string;
    InvoiceNo: string;
    Paxces: string;
    DestinationAirport: any
    OriginAirport: any
    DepartureDate: any
    BookingID: string;
    uid: string;
    Date: string;
    SupplierName: string;
    SupplierMail: string;
    httplogo: string;
    SupplierID: string;
    Source: string;
}


export class Segment {
    ID: number;
    BookingID: string;
    TicketID: string;
    Airline: any;
    AirlineCode: string;
    AirlinePNR: string;
    FareClass: string;
    FlightNumber: string;
    OperatingCarrier: string;
    Baggage: string;
    CabinBaggage: string;
    Craft: any;
    DestinationAirport: any;
    ArrTime: any;
    Duration: any;
    FlightStatus: string;
    GroundTime: string;
    IsETicketEligible: boolean;
    Mile: string;
    OriginAirport: any;
    DepTime: any;
    Remark: string;
    SegmentIndicator: number;
    Status: string;
    StopOver: boolean;
    StopPoint: string;
    StopPointArrivalTime: any
    StopPointDepartureTime: any
    uid: number;
    seat: any;
}

export class Pax {
    id: number;
    BookingID: string;
    Tiketid: string;
    PaxId: string;
    PaxType: string;
    PaxTitle: string;
    FirstName: string;
    LastName: string;
    DateOfBirth?: any;
    Gender: string;
    PassportNo: string;
    AddressLine1: string;
    AddressLine2: string;
    City: string;
    CountryCode: string;
    Nationality: string;
    ContactNo: string;
    Email: string;
    IsLeadPax: boolean;
    FFAirlineCode: string;
    FFNumber: string;
    uid: number;
}

export class Baggage {
    ID: number;
    Paxid: string;
    BookingID: string;
    TiketID: string;
    WayType: number;
    Code: string;
    Description: number;
    Weight: number;
    BaseCurrencyPrice: number;
    BaseCurrency?: any;
    Currency: string;
    Price: number;
    Origin: string;
    Destination: string;
    uid: number;
}

export class MealDynamic {
    ID: number;
    Paxid: string;
    BookingID: string;
    TicketID: string;
    WayType: number;
    Code: string;
    Description: number;
    AirlineDescription: string;
    Quantity: number;
    BaseCurrency?: any;
    BaseCurrencyPrice: number;
    Currency: string;
    Price: number;
    Origin: string;
    Destination: string;
    uid: number;
}

export class FareDetail {
    ID: number;
    Paxid: string;
    BookingID: string;
    TiketID: string;
    Currency: string;
    CutBaseFare: number;
    BaseFare: number;
    Tax: number;
    TaxBreakup: string;
    ChargeBU: string;
    YQTax: number;
    AdditionalTxnFeeOfrd: number;
    AdditionalTxnFeePub: number;
    PGCharge: number;
    OtherCharges: number;
    uid: number;
}


