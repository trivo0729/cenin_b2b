export class DomesticRoundTrip {
    Inbound: Flights[];
    Outbound: Flights[];
}


export class arrResult {
    objSearch: objSearch;
    SearchStartOn: Date;
    JourneyType: string;
    IsDomestic: boolean;
    IsGDS: boolean;
    TockenID: string;
    TraceId: string;
    Origin: string;
    Destination: string;
    Flights: any[][];
    AllAirlines: AllAirline[];
    arrFilter: ArrFilter;
}


export class Flights {
    ResultIndex: string;
    Source: number;
    IsLcc: boolean;
    IsRefundable: boolean;
    GstAllowed: boolean;
    IsGstMandatory: boolean;
    AirlineRemark: string;
    Fare: Fare;
    FareBreakdown: FareBreakdown[];
    Segments: any[][];
    LastTicketDate: string;
    TicketAdvisory: string;
    FareRules: FareRule[];
    AirlineCode: string;
    ValidatingAirline: string;
    SSR?: any;
    FareDetails: any;
    isloading: boolean;
}

export class Charge {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails: any[];
    FranchiseeMarkup: number;
    FranchiseGSTdetails: any[];
    FRCommission: number;
    FRTDS: number;
    HotelTaxes?: any;
    HotelRate: number;
    RoomRate: number;
    CUHTaxes?: any;
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes?: any;
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes?: any;
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export class TaxBreakup {
    Key: string;
    Value: number;
}

export class ChargeBu {
    Key: string;
    Value: number;
}

export class Fare {
    Charge: Charge;
    Currency: string;
    BaseFare: number;
    Tax: number;
    TaxBreakup: TaxBreakup[];
    YqTax: number;
    AdditionalTxnFeeOfrd: number;
    AdditionalTxnFeePub: number;
    PgCharge: number;
    OtherCharges: number;
    ChargeBu: ChargeBu[];
    Discount: number;
    PublishedFare: number;
    CommissionEarned: number;
    PlbEarned: number;
    IncentiveEarned: number;
    OfferedFare: number;
    TdsOnCommission: number;
    TdsOnPlb: number;
    TdsOnIncentive: number;
    ServiceFee: number;
    TotalBaggageCharges: number;
    TotalMealCharges: number;
    TotalSeatCharges: number;
    TotalSpecialServiceCharges: number;
}

export class Charge2 {
    Type?: any;
    Rate: number;
    ServiceTax: number;
    IsOnMarkup: boolean;
    AgentMarkup: number;
    AgentCurrency?: any;
    Commission: number;
    TDS: number;
    CUTPrice: number;
    GSTdetails: any[];
    FranchiseeMarkup: number;
    FranchiseGSTdetails: any[];
    FRCommission: number;
    FRTDS: number;
    HotelTaxes?: any;
    HotelRate: number;
    RoomRate: number;
    CUHTaxes?: any;
    CUHMarkup: number;
    CUTMarkup: number;
    CUHTotal: number;
    S2STaxes?: any;
    S2SMarkup: number;
    SupplierTotal: number;
    b2bTaxes?: any;
    AdminMarkup: number;
    Discount: number;
    TotalPrice: number;
}

export class FareBreakdown {
    Charge: Charge2;
    Currency: string;
    PassengerType: number;
    PassengerCount: number;
    BaseFare: number;
    Tax: number;
    YqTax: number;
    AdditionalTxnFeeOfrd: number;
    AdditionalTxnFeePub: number;
    PgCharge: number;
}

export class FareRule {
    Origin: string;
    Destination: string;
    Airline: string;
    FareBasisCode: string;
    FareRuleDetail: string;
    FareRestriction: string;
}



export class objSearch {
    UserName: string;
    Password: string;
    TokenID: string;
    EndUserIp: string;
    AdultCount: string;
    ChildCount: string;
    InfantCount: string;
    DirectFlight: string;
    OneStopFlight: string;
    JourneyType: string;
    PreferredAirlines?: any;
    Segments: Segment[];
    Sources?: any;
}

export class Segment {
    Origin: string;
    Destination: string;
    FlightCabinClass: string;
    PreferredDepartureTime: Date;
    PreferredArrivalTime: Date;
}

export class AllAirline {
    Name: string;
    Code: string;
    Count: number;
}


export class ArrFilter {
    arrOutBounds: ArrOutBound[];
    arrInbounds: ArrInBound[];
}

export class ArrInBound {
    OrderByDepart: number;
    OrderByArrival: number;
    OrderByDuration: number;
    OrderByPrice: number;
    Orderby: number;
    MinPrice: number;
    MaxPrice: number;
    FareType: FareType[];
    Airlines: Airline[];
    Type?: any;
    DepartureTime: DepartureTime[];
    arrStops: ArrStop[];
    Layover: string[];
    Destination: string[];
    OriginAirport: string[];
}

export class ArrOutBound {
    OrderByDepart: number;
    OrderByArrival: number;
    OrderByDuration: number;
    OrderByPrice: number;
    Orderby: number;
    MinPrice: number;
    MaxPrice: number;
    FareType: FareType[];
    Airlines: Airline[];
    Type?: any;
    DepartureTime: DepartureTime[];
    arrStops: ArrStop[];
    Layover: string[];
    Destination: string[];
    OriginAirport: string[];
}



export class FareType {
    Type: string;
    Counts: number;
    value: string;
}

export class Airline {
    Name: string;
    Code: string;
    Count: number;
}


export class DepartureTime {
    Type: string;
    Counts: number;
    value: string;
}

export class ArrStop {
    Type: string;
    Counts: number;
    value: string;
}