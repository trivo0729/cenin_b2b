export class Userdetails {
    LoginDetail: LoginDetail;
    retCode: number;
    sParentID: number;
    Message: string;
}

export class LoginDetail {
    sid: number;
    Name: string;
    Email: string;
    ContactID?: any;
    ParentId: number;
    Password: string;
    Remarks?: any;
    UserType?: any;
    ContacDetails: ContacDetails;
    LastName: string;
    Designation?: any;
    AgencyDetail: AgencyDetail;
}


export class ContacDetails {
    Mobile: string;
    Phone: string;
    CitiesID: string;
    CountryID?: any;
    Address: string;
    PinCode: string;
}


export class AgencyDetail {
    AgencyName: string;
    AgencyMail: string;
    Fax: string;
    Website: string;
    OtherDetail: OtherDetail;
}

export class OtherDetail {
    IATAStatus: string;
    IATANo?: any;
    Currency: string;
    PanNo?: any;
    ServiceTaxNumber?: any;
    Referral: string;
}



