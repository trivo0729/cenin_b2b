export class Menulist {
    ID: number;
    Name: string;
    Path: string;
    ChildItem: ChildItem[];
}

export interface ChildItem {
    ID: number;
    Name: string;
    Path: string;
    ChildItem: any[];
}