export class Cart {
    CartID: number
    Date: string;
	ExpiryTime: string;
    CartStatus: string;
}

export class Product {
    ProductID: number
    CartID: number;
	ProductDetails: string;
    ProductType: string;
    ExpiryTime :string;
    CartStatus:boolean;
    RecordID:Number;
}
