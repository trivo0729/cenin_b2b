export class Wishlist {
      ID :number
      Date :string
      ExpiryTime :string
      CartStatus :boolean
      Details:string
      UserID :string
      ProductID :number /* For Activity =1 ,Package =2 */
      ProductType :number
}
