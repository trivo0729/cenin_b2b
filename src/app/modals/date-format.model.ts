import { DatePipe } from '@angular/common';
import { PipeTransform } from '@angular/core';

export class DateFormat implements PipeTransform {
    transform(value: string) {
        var datePipe = new DatePipe("en-US");
         value = datePipe.transform(value, 'dd-mm-yyyy');
         return value;
     }
}