import { UserComponent } from '../components/user/user.component';
import { MatDialog } from '@angular/material';

export class User {
    sid: number;
    Name: string;
    LastName: string;
    Email: string;
    ContactID: number;
    ParentId: number;
    Password: string;
    Remarks: string;
    UserType: string;
    ContacDetails: ContacDetails
}
export class ContacDetails {
    Mobile: string;
    Phone: string;
    CitiesID: string;
    CountryID: string;
    Address: string;
    zipCode: string
}

export class UserCred {
    Password: string;
    ConfirmPassword: string;
    ConfirmNewPassword: string;
    sid: number;
}

export class UserEmail {
    Email: string;
    ConfirmEmail: string;
    ConfirmNewEmail: string;
    sid: number;
}


export class loginDetails {
    LoginDetail: User;
    retCode: number = 0;
    Message = 1;
    ParentID: number
    constructor() {
        this.ParentID = 232;
        this.retCode = 0;
    }
}

export function login() {
    var dialog: MatDialog;
    var dialogRef = dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe(result => {
        this.GetLoginDetails();
    });
}