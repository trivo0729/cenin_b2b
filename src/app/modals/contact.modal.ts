export class Contact {
    Firstname: string;
    Lastname: string;
    Email: string;
    Phone: string;
    Message: string;
    AdminID: number;
}
