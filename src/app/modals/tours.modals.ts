import { Dbhelper } from './dbhelper.model';
import { ToursService } from '../services/tours.service';

export class Tours {
    ActivityID: number;
    Supplier: string;
    Name: string;
    City: string;
    Country: string;
    CheckIN: string;
    Description: string;
    Tour_Note: string;
    List_Mode: any;
    List_Type: any;
    Duration: string;
    TotalPrice: number;
    CurrencyClass: string;
    ListImage: Image;
    Tooltip: string;
    Opening: string;
    Closing: string;
    LocationDetail: LocationDetail;
    ID: number;
}
export class LocationDetail {
    LocationName: string;
    Latitude: string
    Longitutde: string
}

export class Image {
    Count: string;
    IsDefault: string;
    Title: string;
    Type: string;
    Url: string;
}

export class TourType {
    Type: string;
}
export class Dates {
    sYear: string;
    arrDates: DateRange[];
}
export class DateRange {
    sDate: string;
    fDate: string;
    Month: string;
    Day: string;
    Date: string;
    Available: boolean
}
export class Slots {
    ID: number;
    SlotsStartTime: string;
    SlotsEndTime: string;
}
export class TicketType {
    Ticket: string;
    TicketID: string;
    InventoryType: String;
    MinPax: Number;
    MaxPax: Number;
    available: Number;
    arrayPax: PaxTyp[];
    Currency: string;
    PlanId: number;
    nonRefundable: boolean;
    arrCancellationPolicy: Cancellation[];
    ImportentNote:string;
    arrPickupDrop:any[];
    Total: number;
    Inclusions: string;
    Exclusions: string;
    arrInclusion :string[];
    arrExclusion:string[];
    showCancel:boolean;
    showInfo: boolean;
}
export class PaxTyp {
    Pax_Type: string;
    Rate: string;
    Age: string;
    Count: number = 1;
    Total=0.00;
}

export class Cancellation {
    AmountToCharge: string;
    CancelationPolicy: string;
    ChargesType: string;
    DaysPrior: string;
    IsDaysPrior: string;
    PercentageToCharge: string;
    RefundType: string;
    info:string[];
}

export class arrLocation {
    LocationName: string;
    City: string;
    Lid: number;
    Country: string;
    Latitude: string;
    Longitutde: string;
}

export class State {
    LocationName: string;
    City: string;
    flag: string;
}

export class TourTypes {
    Icon: string;
    Name: string;
    ActivityCount: number;
    MinPrice: number;
    checked: Boolean = false;
}

export class objTour {
    Date: any;
    Location: any;
    LocationName: any;
    TourType: any;
}


export class Booking_Tour {
    Sr_No: number;
    Request_Id: number;
    Activity_Id: number;
    Passenger_Name: string;
    Email: string;
    Contact_No: string;
    Adults: number;
    Child1: number;
    Child2: number;
    Infant: number;
    Adult_Cost: number;
    Child1_Cost: number;
    Child2_Cost: number;
    Infant_Cost: number;
    Extras_Addon: number;
    Taxes: number;
    Discount: number;
    TotalAmount: number;
    User_Id: string;
    IsPaid: boolean;
    TotalPax: number;
    PaymentTransaction_Id: string;
    Pickup_Location: string;
    Drop_Location: string;
    Comment: string;
    Vehicle_Id: number;
    Driver_Id: number;
    Booking_Date: string;
    Invoice_No: string;
    Voucher_No: string;
    ParentID: number;
    Sightseeing_Date: string;
    Status: string;
    SlotID: number;
    Ticket_Type: number;
    Activity_Type: string;
    BookingType: string;
    InventoryType: string;
    PlanID: number;
    SupplierId: number

}

export class Search_Params {
    LocationID: string;
    TourType: string;
    sDate: string;
    LocationName: string;
    Time: string
    Adults: number;
    Child: number;
    constructor() {
        this.LocationID = "";
        this.TourType = "";
        this.sDate = "";
        this.LocationName = "";
        this.Time = "12:00";
        this.Adults = 1;
        this.Child = 0;
    }

}

/*Search Resuls */

export class resultTours {

    retCode: number;
    Activities: Tours[];
    arrTouType: TourTypes[];
    PriceRange: sPriceRange;
    Massage: string
}

export class sPriceRange {
    MinPrice: number;
    MaxPrice: number
}

export class Rate_Params {
    ID: Number = 0;
    Name: string;
    Location: string;
    TourType: string;
    RateType: string;
    Date: string;
    SlotDate: string;
    SlotName: string;
    tourType: TourType[] = [];
    constructor(private tourService: ToursService) { }
}

export class PackageCities {
    City: string;
}

export class PackageNames{
PackageName: string;
PackageID : any;

}
export class bookings {
    ActivityId: string
    Adult: string
    AdultCost: string
    BookDate: string
    BookingID: string
    InvoiceNo:string
    VoucherID:string
    Child1: string
    Child1Age: string
    Child1Cost: string
    Child2: string
    Child2Age: string
    Name: string
    SightseeingDate: string;
    DateRange: DateRange;
    Status: string;
}


export class arrSearch {
    arrUserDetails: arrUserDetails;
    locationID: string;
    date: string;
    tourType: string;
}
export class arrUserDetails {
    userName: string;
    password: string;
    parentID: number;
}

export class BookingDetails {
    ActivityId: string;
    BookDate: string;
    ActivityName: string;
    DateRange?: null;
    SightseeingDate: string;
    Name?: null;
    PassengerName: string;
    Email: string;
    Contact: string;
    ActImage?: null;
    Adult: any;
    Child1: any;
    Child2: any;
    Infant: any;
    TotalPax: string;
    TotalAmount: string;
    BookingID?: null;
    InvoiceNo:string;
    VoucherID : string;
    Status: string;
    Location: string;
    AdultCost: number;
    Child1Cost: number;
    Child2Cost: number;
    InfantCost: number;
    TicketType: string;
    Slot: string;
    Duration?: null;
    Inclusions?: null;
    Exclusions?: null;
    AddOns?: null;
    Tax?: null;
    PricingNote?: null;
    ImpNote?: null;
    RateType?: null;
    StartTime?: null;
    EndTime?: null;
    Currency: string;
    Child1Age?: null;
    Child2Age?: null;
    ActivityType: string;
    Tikets?: null;
    AmountInWord:string;
}


