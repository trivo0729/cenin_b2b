export class Register {
}

export class arrLogin {
    OldId: string;
    NewId: string;
    UserName: string;
    UserType: string;
    Password: string;
    ParentId: string;
    CompanyDetails: CompanyDetails;
    Owner: ContactOrOwner;
    OperationsContact?: (ContactOrOwner)[] | null;
    AccountsContact?: (string)[] | null;
    Affilation?: (Affilation)[] | null;
    TaxationDetails: TaxationDetails;
    BankDetails?: null;
    OtherDetails: OtherDetails;
    Authority: Authority;
}
export class CompanyDetails {
    TradingName: string;
    LegalName: string;
    Contact: Contact;
    Address: Address;
    Website: string;
    RegistrationType: string;
    OtherType: string;
    RegistrationNo: string;
    RegistrationDate: string;
    PrefferedCurrency: string;
    DocumentLink: string;
    LogoLink: string;
}
export class Contact {
    Phone?: (string)[] | null;
    Email?: (string)[] | null;
}
export class Address {
    BuildingShop: string;
    Street: string;
    Landmark: string;
    City: string;
    State: string;
    Country?: null;
    PinCode: string;
    AddressProof: string;
}
export class ContactOrOwner {
    FirstName: string;
    LastName: string;
    Designation: string;
    Email: string;
    Phone: string;
    Mobile: string;
}
export class Affilation {
    Name: string;
    Number: string;
    Documentlink: string;
}
export class TaxationDetails {
    TaxName: string;
    TaxCode: string;
    TaxRegistrationNo: string;
    TaxRegistrationDate: string;
    CompanyPanNo: string;
    PanDocumentLink: string;
    DocumentLink: string;
}
export class OtherDetails {
    RegistrationDate: string;
    LastUpdated?: null;
    LastLogin: string;
    AccountStatus: string;
    Logo: string;
    SalesPerson: SalesPerson;
}
export class SalesPerson {
    Name: string;
    Mobile: string;
    Email: string;
}
export class Authority {
    ShowSupplier: boolean;
    Menu?: null;
}




