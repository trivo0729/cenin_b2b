export class Transaction {
    TransactionID?: number;
    TransactionDate: string;
    ReferenceNo: string;
    Particulars: string;
    CreditedAmount: number;
    DebitedAmount: number;
    Balance: number;
    Currency: string;
}

export class Credits {
    DepositDate: string;
    Amount: number;
    TransactionType: string;
    BankName: string;
    ReceiptNo: string;
    ApprovedDate: string;
    Status: string;
    Currency: string;
}

export class BankDetails {
    BankName: string;
    AccountNo: string;
    Branch: string;
    SwiftCode: string;
    Country: string;
    IFSCCode?: any;
    sid: number;
}


