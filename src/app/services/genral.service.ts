import { Injectable } from '@angular/core';
import { User } from '../modals/user.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dbhelper } from '../modals/dbhelper.model';
import { ApiUrlService } from './api-url.service';

@Injectable({
  providedIn: 'root'
})
export class GenralService {
  ExchnageUrl: string;
  locationUrl: string;
  constructor(private httpClient: HttpClient, private ServiceUrl: ApiUrlService) {
    this.ExchnageUrl = this.ServiceUrl.ExchnageUrl;
    this.locationUrl = this.ServiceUrl.locationUrl;
  }
  /*Get  Exchange*/
  getExchange(ParentID: number): Observable<Dbhelper> {
    return this.httpClient.post<Dbhelper>(this.ExchnageUrl + "GetCurrency",
      { ParentID: ParentID },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  /*Get  Country*/
  getCountry(): Observable<any> {
    return this.httpClient.post<any>(this.locationUrl + "GetCountry",
      {},
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  /*Get  States*/
  getState(): Observable<any> {
    return this.httpClient.post<any>(this.locationUrl + "GetState",
      {},
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  /*Get  City By Country*/
  getCity(Country: string): Observable<any> {
    return this.httpClient.post<any>(this.locationUrl + "GetCity",
      {
        Country: Country,
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }
}
