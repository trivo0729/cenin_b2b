import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of, forkJoin } from "rxjs";
import { pluck } from "rxjs/operators";
import { CommonCookieService } from './common-cookie.service';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: "root",
})
export class CurrencyService {
  protected baseUrl: string = "https://api.exchangeratesapi.io/latest";
  constructor(private http: HttpClient,
    private objGloble: ApiUrlService,
    private commonService: CommonCookieService
  ) { }

  getTickets(): Observable<any[]> {
    const tickets: any[] = [{ from: "INR", to: "USD", amount: 1200 }];
    return of(tickets);
  }

  getCurrencyConversion(base: string, convertTo: string): Observable<any> {
    return this.http
      .get(`${this.baseUrl}?base=${base}&symbols=${convertTo}`)
      .pipe(pluck("rates"), pluck(convertTo));
  }

  setExchange(rate: string) {
    localStorage.setItem("exchange", rate);
  }

  getExchange() {
    let exRate;
    try {
      return (exRate = JSON.parse(localStorage.getItem("exchange")));
    } catch (ex) {
      return null;
    }
  }

  setCurrency(Currency: string) {
    Currency = JSON.stringify(Currency);
    localStorage.setItem("currency", Currency);
  }

  getCurrency() {
    let Currency;
    try {
      return (Currency = JSON.parse(localStorage.getItem("currency")));
    } catch (ex) {
      return null;
    }
  }

  ConvertAmt(from: string, to: string, amount: any) {
    let total;
    let rates = this.getExchange();
    if (rates !== null) {
      if (from !== to) {
        let currency = rates.arrCurrency;
        let base = currency[0].BaseCurrency;
        let a = currency.filter((ex) => ex.Currency === from)[0];
        let b = currency.filter((ex) => ex.Currency === to)[0];
        if (from !== base)
          amount = amount * a.Rate;
        total = amount / b.Rate;
      } else total = amount;
    } else total = amount;
    return total;
  }

  ReverseAmt(from: string, to: string, amount: any) {
    let total;
    let rates = this.getExchange();
    if (rates !== null) {
      if (from !== to) {
        let currency = rates.arrCurrency;
        let a = currency.filter((ex) => ex.Currency === from)[0];
        let b = currency.filter((ex) => ex.Currency === to)[0];
        total = amount * b.Rate;
      } else total = amount;
    } else total = amount;
    return total;
  }

  setExRates(rate: any) {
    localStorage.setItem("exRate", rate);
  }

  clearSession(key: string) {
    try {
      localStorage.removeItem(key);
    } catch (ex) { }
  }

  getBaseCurrency() {
    debugger
    var Currency: string;
    if (this.commonService.checkcookie('login')) {
      let data = JSON.parse(this.commonService.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          return Currency = data.LoginDetail.AgencyDetail.OtherDetail.Currency
        }
        else return this.objGloble.Currency;
      }
    }
    else return this.objGloble.Currency;
  }
}
