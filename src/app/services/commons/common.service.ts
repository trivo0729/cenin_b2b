import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Wishlist } from 'src/app/modals/common/wishlist.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../session.service';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  Parent: number = 232;
  Url: string;
  constructor(private httpClient: HttpClient, private sessionService: SessionService,
    private ServiceUrl: ApiUrlService) {
    this.Url = this.ServiceUrl.CartUrl;
    this.Parent = this.ServiceUrl.AdminID;
  }

  /*Addto Wish*/
  AddtoWishList(arrWish: Wishlist): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "AddtoWishList",
      {
        arrWish
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Remove Wish*/
  RemoveWish(ProductType: number, ProductID: number, UserName: string): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "RemoveWish",
      {
        ProductType,
        ProductID,
        UserName
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /* Wishs*/
  CheckWish(UserName: string): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "CheckExpreWish",
      {
        UserName
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }
}
