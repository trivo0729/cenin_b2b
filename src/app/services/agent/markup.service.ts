import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: 'root'
})
export class MarkupService {

  constructor(private http: HttpClient, private service: ApiUrlService) { }

  getMarkupByAgent(Uid: number) {
    return this.http.post<any>(this.service.MarkupUrl + '_getMarkupByAgent',
      {
        uid: Uid,
        ParentID: this.service.AdminID
      }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
  }

  setMarkupByAgent(Uid: number, arrMarkup: any) {
    return this.http.post<any>(this.service.MarkupUrl + '_setMarkupByAgent',
      {
        uid: Uid,
        arrMarkup: arrMarkup
      }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
  }
}
