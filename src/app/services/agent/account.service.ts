import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient, private service: ApiUrlService) { }

  GetTransactions(Uid: number, UserType: string) {
    return this.http.post<any>(this.service.AccountUrl + 'GetTransactions',
      {
        Uid: Uid,
        UserType: UserType
      }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
  }

  GetCreditInformation(uid: number) {
    return this.http.post(this.service.AccountUrl + 'GetCreditInformation',
      {
        uid: uid
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  GetBalanceInformation(uid: number) {
    return this.http.post(this.service.AccountUrl + 'GetBalanceInformation',
      {
        uid: uid
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  GetTaxes(uid: number) {
    return this.http.post(this.service.AccountUrl + 'GetTaxes',
      {
        uid: uid
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  getSalesPerson(UserID: any, CityID: string, StateID: string) {
    return this.http.post(this.service.AccountUrl + 'GetSalesPerson',
      {
        UserID: UserID,
        CityID: CityID,
        StateID: StateID
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }
}


