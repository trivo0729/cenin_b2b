import { Injectable } from '@angular/core';
import { Tours, TourType, DateRange, Slots, TicketType, arrLocation, Search_Params, resultTours, TourTypes, Dates, PackageCities, bookings, arrUserDetails, Cancellation } from '../modals/tours.modals';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promise } from 'q';
import { Observable } from 'rxjs';
import { GlobalDefaultService, SerchParams_Tour } from './global-variable.service';
import { SessionService } from './session.service';
import { User } from '../modals/user.model';
import { ApiUrlService } from './api-url.service';
@Injectable({
  providedIn: 'root'
})
export class ToursService {
  arrTours: Tours;
  arrTourType: TourType;
  arrDates: DateRange;
  objGlobal: GlobalDefaultService;
  Parent: number;
  Url: string;
  arrUserDetails: arrUserDetails;
  arrDetails: any;
  Search_Params: Search_Params = new Search_Params();
  constructor(
    private httpClient: HttpClient,
    private sessionService: SessionService,
    private ServiceUrl: ApiUrlService) {
    this.Url = this.ServiceUrl.TourUrl;
    this.Parent = this.ServiceUrl.AdminID;
    this.arrUserDetails = new arrUserDetails();
    this.arrUserDetails.userName = this.ServiceUrl.UserName;
    this.arrUserDetails.password = this.ServiceUrl.Password;
    this.arrUserDetails.parentID = this.ServiceUrl.AdminID;

  }

  setSerachParams(arrParams: Search_Params) {
    debugger;
    try {
      this.sessionService.set("SearchParams", JSON.stringify(arrParams));
      this.Search_Params = arrParams;
    } catch (e) { }
  }
  getSerachParams() {
    this.Search_Params = this.sessionService.get("SearchParams");
    if (this.Search_Params == null)
      this.Search_Params = new Search_Params();
    return this.Search_Params;
  }

  /*Get All Tours*/
  getLocation(Name: string): Observable<arrLocation[]> {
    return this.httpClient.post<arrLocation[]>(this.Url + "GetLocations",
      {
        ParentID: this.Parent, Name
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Get Popular  Tours*/
  getPopularTours(): Observable<any> {
    return this.httpClient.post<any>(this.Url + "GetPopulars",
      { AdminID: this.Parent, }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Save Popular Tours*/
  SavePopularTour(ActivitID: number, MinPrice: number): Observable<any> {
    return this.httpClient.post<any>(this.Url + "SetPopularTour",
      {
        AdminID: this.Parent,
        ActivitID: ActivitID,
        MinPrice: MinPrice,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Get  Tour Images*/
  GetImage(ActivitID: number): Observable<any> {
    return this.httpClient.post<any>(this.Url + "GetImages",
      {
        ActivityId: ActivitID,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Get All Tours*/
  getTours(arrSearch): Observable<resultTours> {
    // this.Search_Params =  this.getSerachParams();
    // var arrUserDetails =  this.arrUserDetails;

    debugger
    return this.httpClient.post<resultTours>(this.Url + "SearchActivities",
      {
        arrSearch: arrSearch
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  /*Get  Tour Type By Tour*/
  getTourtype(ID: Number, ActivityDate: string): Observable<TourType[]> {
    return this.httpClient.post<TourType[]>(this.Url + "GetRateType", { ID, ActivityDate }, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }
  /*Get  All Tour Type*/
  getAllTourtype(Location: string): Observable<TourTypes[]> {
    return this.httpClient.post<TourTypes[]>(this.Url + "GetTourType", {
      ParentID: this.Parent,
      LocationID: Location
    }, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }


  /*Avail Dates*/
  getDateRange({ Ratetype, ID, Date, next }: { Ratetype: string; ID: Number; Date: string; next: boolean; }): Observable<Dates> {
    return this.httpClient.post<Dates>(this.Url + "DateArray",
      { ID, Date, Ratetype, next }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
  }

  /*Avail Slots*/
  getSlots({ Type, ID, Date, PickupDrop }: { Type: string; ID: Number; Date: string; PickupDrop: any }): Observable<Slots[]> {
    return this.httpClient.post<Slots[]>(this.Url + "SlotByDate",
      {
        ID, Date, Type, PickupDrop
      },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
  }

  /*Slot  Rates*/
  getSlotRate({ Type, ID, SlotID, Date }: { Type: string; ID: Number; SlotID: Number; Date: string; }): Observable<TicketType[]> {
    return this.httpClient.post<TicketType[]>(this.Url + "GetRates",
      {
        arrUserDetails: this.arrUserDetails,
        Type, ID, SlotID, Date
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
  }

  /*Slot  Rates*/
  BookTours(arrBooking: any, CartID: String, arrParams: any[]): Observable<any> {
    return this.httpClient.post<any>(this.Url + "Book",
      {
        arrBooking: arrBooking,
        CartID,
        arrParams
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
  }

  /*Get  Booking By User*/
  getBooking(UserName: string,UserType:string): Observable<bookings[]> {
    return this.httpClient.post<bookings[]>(this.Url + "GetBookingList", {
      UserName ,
      UserType
      }, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }


  /*Get  Booking By User*/
  getTickets(ResrvationID: string, Individual: boolean): Observable<bookings[]> {
    return this.httpClient.post<bookings[]>(this.Url + "_getTickets",
      {
        ResrvationID: ResrvationID,
        Individual: Individual,
        Pdf: false,
        ParentID: this.ServiceUrl.AdminID
      },
      {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      });
  }
  getCancellations(arrCancellation: Cancellation[]) {
    try {
      debugger
      arrCancellation.forEach(d => {
        d.info = [];
        if (d.RefundType != "NoShow") {
          if (d.ChargesType == "Percentile") {
            if (d.DaysPrior.includes("hr")){
              d.info.push("If cancelled before " + d.DaysPrior + " from starting time " + d.PercentageToCharge + " will be charged");
              d.info.push("If cancelled after " + d.DaysPrior + " from starting time 100% will be charged");
            }
            else{
              d.info.push("If cancelled before " + d.DaysPrior + " days from starting time " + d.PercentageToCharge + " will be charged");
              d.info.push("If cancelled after " + d.DaysPrior + " days from starting time 100% will be charged");
            }
          } else if (d.ChargesType == "Amount") {
            if (d.DaysPrior.includes("hr"))
              d.info.push("If cancelled before " + d.DaysPrior + " from starting time " + d.AmountToCharge + " will be charged");
            else
              d.info.push("If cancelled before " + d.DaysPrior + " days from starting time " + d.AmountToCharge + " will be charged");

            d.info.push("If cancelled after " + d.DaysPrior + " days from starting time 100% will be charged");
          }
        }
        else {
          if (d.ChargesType == "Percentile") {
            d.info.push(d.PercentageToCharge + " will be charged if No Show (if service not used / service missed)")
          } else if (d.ChargesType == "Amount") {
            d.info.push(d.AmountToCharge + "amount will be charged if No Show (if service not used / service missed)")
          }
        }
      });
    } catch (e) { console.log(e.message) }
    return arrCancellation;
  }


  /*Get  All Pickup Drop*/
  getAllPickupDrop(Location: string, ID: Number, RateType: string, travelType: string, Date: string, PickupID: string): Observable<TourTypes[]> {
    var arrPickupID = new Array();
    if (PickupID != "")
      arrPickupID.push(PickupID);
    return this.httpClient.post<TourTypes[]>(this.Url + "GetPickupDrop", {
      ID: ID,
      Type: RateType,
      Date: Date,
      Location: Location,
      travelType: travelType,
      PickupID: arrPickupID
    }, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }



}