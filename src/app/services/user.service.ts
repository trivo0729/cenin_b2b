import { Injectable } from '@angular/core';
import { User, loginDetails } from '../modals/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from './api-url.service';
import { CommonCookieService } from './commons/common-cookie.service';
@Injectable({
  providedIn: 'root'
})

export class UserService {
  data: any;
  Url: string;
  UserType: string;
  constructor(private httpClient: HttpClient, private UrlService: ApiUrlService,
    private cookie: CommonCookieService
    ) {
    this.Url = UrlService.AuthorizeUrl;
  }

  verifyEmail(email: string) {
    return this.httpClient.post<any>(this.Url + "VerifyAccount", {
      Email: email
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*UserLogin*/
  public LoginDetails: loginDetails = new loginDetails();
  UserLogin(UserName: string, Password: string, sParentID: number, UserType: string[]) {
    return this.httpClient.post<loginDetails>(this.Url + "Userlogin",
      { UserName, Password, sParentID, UserType },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  AgentInfo(uid: number) {
    return this.httpClient.post<any>(this.Url + "UserInfo", {
      uid: uid
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  signup(user: User) {
    return this.httpClient.post<any>(this.Url + "SignUp",
      { arrLogin: user },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  UpdateProfile(LoginDetails: any) {
    return this.httpClient.post<any>(this.Url + "ChangeProfile",
      { arrDetails: LoginDetails },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })

      });
  }


  forgetPassword(Email: string, ParentID: number, UserType: any) {
    return this.httpClient.post<any>(this.UrlService.EmailUrl + "ForgotPassword",
      {
        Email: Email,
        ParentID: ParentID,
        UserType: UserType
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }


  ChangePassword(sid: number, OldPassword: string, NewPassword: string, UserType: string) {
    return this.httpClient.post<any>(this.Url + "ChangePassword",
      {
        sid: sid,
        OldPassword: OldPassword,
        NewPassword: NewPassword,
        UserType: UserType
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  setLogedIn(loginDetails: string) {
    localStorage.setItem('LoginDetails', loginDetails)
  }

  getUserdata() {
    var userdata : any = new Array();
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          userdata = {
            userName: data.LoginDetail.Email,
            password: data.LoginDetail.Password,
            parentID: this.UrlService.AdminID
          }
        }
        if (data.LoginDetail.UserType === "B2C") {
          userdata = {
            userName: this.UrlService.UserName,
            password: this.UrlService.Password,
            parentID: this.UrlService.AdminID
          }
        }
      }
    }
    else {
      userdata = {
        userName: this.UrlService.UserName,
        password: this.UrlService.Password,
        parentID: this.UrlService.AdminID
      }
    }
    return userdata;
  }

  getLogedIn() {
    try {
      return this.LoginDetails = JSON.parse(localStorage.getItem('LoginDetails'));
    }
    catch (ex) {
      return null;
    }
  }

  clearSession(key: string) {
    try {
      localStorage.removeItem(key)
    }
    catch (ex) {

    }
  }
}
