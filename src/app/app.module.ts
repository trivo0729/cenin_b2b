import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TourListComponent } from './components/tour-list/tour-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { MatExpansionModule } from '@angular/material/expansion';
import {
  MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatDatepickerModule,
  MatNativeDateModule, MatListModule, MatSelectModule, MAT_DATE_FORMATS,
  NativeDateModule, MatRippleModule, MatIconModule, MatButtonModule, MatButtonToggleModule,
  MAT_DATE_LOCALE, MatRadioModule, MatSliderModule, MatCardModule, MatDialogModule, MatTableModule, MatDialogRef, MatTabsModule, MatCheckboxModule, MatDividerModule, MatSlideToggleModule, MatTreeModule, MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR, MatTooltipModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserComponent } from './components/user/user.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { BookingsComponent } from './components/user/bookings/bookings.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';
import { WishlistComponent } from './components/user/wishlist/wishlist.component';
import { EditprofileComponent } from './components/user/editprofile/editprofile.component';
import { SignUpComponent } from './components/user/sign-up/sign-up.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { AddCartComponent } from './components/shopping-cart/add-cart/add-cart.component';
import { ShoppingCartService } from './services/shopping-cart.service';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CartComponent } from './components/shopping-cart/cart/cart.component';
import { ListComponent } from './components/tour-list/view/list/list.component';
import { TourDetailsComponent } from './components/tour-list/tour-details/tour-details.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { GridComponent } from './components/tour-list/view/grid/grid.component';
import { MapComponent } from './components/tour-list/view/map/map.component';
import { SearchComponent } from './components/tour-list/search/search.component';
import { TourwizardComponent } from './components/tour-list/tourwizard/tourwizard.component';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { AddReviewsComponent } from './components/add-reviews/add-reviews.component';
import { PackageListComponent } from './components/package/package-list/package-list.component';
import { PackageDetailsComponent } from './components/package/package-details/package-details.component';
import { DatePipe } from '@angular/common';
import { AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, LinkedinLoginProvider, SocialLoginModule } from 'angular-6-social-login';
import { AddwishComponent } from './components/addwish/addwish.component';
import { ReviewratingsComponent } from './components/reviews/reviewratings/reviewratings.component';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AddRemoveWishComponent } from './components/addwish/add-remove-wish/add-remove-wish.component';
import { PackageDocumentsComponent } from './components/package/package-documents/package-documents.component';
import { ApiUrlService } from './services/api-url.service';
import { ImageSliderComponent } from './components/image-slider/image-slider.component';
import { OrderModule } from 'ngx-order-pipe';
import { HelpComponent } from './components/help/help.component';
import { PackageBookingComponent } from './components/package/package-booking/package-booking.component';
import { SliderComponent } from './components/slider/slider.component';
import { CurrencyComponent } from './components/currency/currency.component';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LoaderComponent } from './components/loader/loader.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { SightseeingComponent } from './components/sightseeing/sightseeing.component';
import { PackageGridComponent } from './components/package/package-grid/package-grid.component';
import { HotelComponent } from './components/hotel/hotel/hotel.component';
import { SearchPanelComponent } from './components/hotel/search-panel/search-panel.component';
import { HotelListComponent } from './components/hotel/hotel-list/hotel-list.component';
import { HotelDetailsComponent } from './components/hotel/hotel-details/hotel-details.component';
import { RateGroupComponent } from './components/hotel/rate-group/rate-group.component';
import { HotelFilterComponent } from './components/hotel/hotel-filter/hotel-filter.component';
import { HotelMapComponent } from './components/hotel/hotel-map/hotel-map.component';
import { TravellerComponent } from './components/hotel/traveller/traveller.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MessageComponent } from './components/Alerts/message/message.component';
import { HotelBookingComponent } from './components/hotel/hotel-booking/hotel-booking.component';
import { ListLoaderComponent } from './components/hotel/loader/list-loader/list-loader.component';
import { SideLoaderComponent } from './components/hotel/loader/side-loader/side-loader.component';
import { HotelTermNConditionsComponent } from './components/hotel/hotel-term-n-conditions/hotel-term-n-conditions.component';
import { TravellerDetailsComponent } from './components/hotel/traveller-details/traveller-details.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AgmCoreModule } from '@agm/core';
import { HotelConfirmComponent } from './components/hotel/hotel-confirm/hotel-confirm.component';
import { BookingReportComponent } from './components/user/booking-report/booking-report.component';
import { GridModule, PDFModule, ExcelModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { CookieService } from 'ngx-cookie-service';
// Import ng-circle-progress
//import { NgCircleProgressModule } from 'ng-circle-progress';

import 'hammerjs';
import { NeedHelpComponent } from './components/footer/need-help/need-help.component';
import { QuickLinksComponent } from './components/footer/quick-links/quick-links.component';
import { SubscribeComponent } from './components/footer/subscribe/subscribe.component';
import { SocialLinksComponent } from './components/footer/social-links/social-links.component';
import { DevelopComponent } from './components/footer/develop/develop.component';
import { PayPartnersComponent } from './components/footer/pay-partners/pay-partners.component';
import { BookingDetailsComponent } from './components/user/booking-details/booking-details.component';
import { HotelVoucherComponent } from './components/hotel/hotel-voucher/hotel-voucher.component';
import { HotelInvoiceComponent } from './components/hotel/hotel-invoice/hotel-invoice.component';
import { HotelBookingsComponent } from './components/user/booking-report/hotel-bookings/hotel-bookings.component';
import { TourBookingsComponent } from './components/user/booking-report/tour-bookings/tour-bookings.component';
import { PackageBookingsComponent } from './components/user/booking-report/package-bookings/package-bookings.component';
import { RegisterComponent } from './components/user/agent/register/register.component';
import { CenninComponent } from './components/cennin/cennin.component';
import { ImagesliderComponent } from './components/cennin/imageslider/imageslider.component';
import { CenninHeaderComponent } from './components/cennin/cennin-header/cennin-header.component';
import { CenninFooterComponent } from './components/cennin/cennin-footer/cennin-footer.component';
import { TrivoServiceComponent } from './components/trivo/trivo-service/trivo-service.component';
import { ClickSearchbarComponent } from './components/click/click-searchbar/click-searchbar.component';
import { ClickHeaderComponent } from './components/click/click-header/click-header.component';
import { ClickFooterComponent } from './components/click/click-footer/click-footer.component';
import { CenninAboutComponent } from './components/cennin/cennin-about/cennin-about.component';
import { ClickAboutComponent } from './components/click/click-about/click-about.component';
import { ClickContactComponent } from './components/click/click-contact/click-contact.component';
import { CenninContactComponent } from './components/cennin/cennin-contact/cennin-contact.component';
import { CenninHomeComponent } from './components/cennin/cennin-home/cennin-home.component';
import { PackageComponent } from './components/package/package/package.component';
import { SearchPackageComponent } from './components/package/search-package/search-package.component';
import { TrivoHeaderComponent } from './components/trivo/trivo-header/trivo-header.component';
import { CenninPrivacypolicyComponent } from './components/cennin/cennin-privacypolicy/cennin-privacypolicy.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsAndConditionComponent } from './components/terms-and-condition/terms-and-condition.component';
import { CenninTermsandconditionComponent } from './components/cennin/cennin-termsandcondition/cennin-termsandcondition.component';
import { TravelHomeComponent } from './components/travelsphere/travel-home/travel-home.component';
import { TravelHeaderComponent } from './components/travelsphere/travel-header/travel-header.component';
import { TravelFooterComponent } from './components/travelsphere/travel-footer/travel-footer.component';
import { TravelAboutComponent } from './components/travelsphere/travel-about/travel-about.component';
import { TravelContactComponent } from './components/travelsphere/travel-contact/travel-contact.component';
import { TravelHotelComponent } from './components/travelsphere/hotels/travel-hotel/travel-hotel.component';
import { TravelResturantsComponent } from './components/travelsphere/resturant/travel-resturants/travel-resturants.component';
import { TravelSliderComponent } from './components/travelsphere/travel-slider/travel-slider.component';
import { TravelLocationGridComponent } from './components/travelsphere/location/travel-location-grid/travel-location-grid.component';
import { TrivoContactComponent } from './components/trivo/trivo-contact/trivo-contact.component';
import { ForgetPasswordComponent } from './components/user/forget-password/forget-password.component';
import { AgencyStatementComponent } from './components/user/agent/agency-statement/agency-statement.component';
import { CreditInformationComponent } from './components/user/agent/credit-information/credit-information.component';
import { CreditRequestComponent } from './components/user/agent/credit-request/credit-request.component';
import { ExoticsHomeComponent } from './components/exotics/exotics-home/exotics-home.component';
import { ExoticsHeaderComponent } from './components/exotics/exotics-header/exotics-header.component';
import { ExoticsFooterComponent } from './components/exotics/exotics-footer/exotics-footer.component';
import { ExoticsContactComponent } from './components/exotics/exotics-contact/exotics-contact.component';
import { ExoticsAboutComponent } from './components/exotics/exotics-about/exotics-about.component';
import { ExoticsSearchComponent } from './components/exotics/exotics-search/exotics-search.component';
import { ExoticsPrivacypolicyComponent } from './components/exotics/exotics-privacypolicy/exotics-privacypolicy.component';
import { ExoticsTermsandconditionsComponent } from './components/exotics/exotics-termsandconditions/exotics-termsandconditions.component';
import { MeetmeHomeComponent } from './components/meetme/meetme-home/meetme-home.component';
import { MeetmeAboutComponent } from './components/meetme/meetme-about/meetme-about.component';
import { MeetmeContactComponent } from './components/meetme/meetme-contact/meetme-contact.component';
import { ExoticsPackageComponent } from './components/exotics/exotics-package/exotics-package.component';
import { MeetmeHeaderComponent } from './components/meetme/meetme-header/meetme-header.component';
import { MeetmeFooterComponent } from './components/meetme/meetme-footer/meetme-footer.component';
import { MeetmeSearchComponent } from './components/meetme/meetme-search/meetme-search.component';
import { MeetmePackageGridComponent } from './components/meetme/meetme-package-grid/meetme-package-grid.component';
import { MeetmeHomesliderComponent } from './components/meetme/meetme-homeslider/meetme-homeslider.component';
import { LoginComponent } from './components/user/agent/login/login.component';
import { TravelVideoComponent } from './components/travelsphere/travel-video/travel-video.component';
import { MeetmeSearchhomeComponent } from './components/meetme/meetme-searchhome/meetme-searchhome.component';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export';
import { TravelAboutdetailsComponent } from './components/travelsphere/about/travel-aboutdetails/travel-aboutdetails.component';
import { SightseeingBookingDetailsComponent } from './components/user/booking-details/sightseeing-booking-details/sightseeing-booking-details.component';
import { SightseeingInvoiceComponent } from './components/tour-list/sightseeing-invoice/sightseeing-invoice.component';
import { SightseeingVoucherComponent } from './components/tour-list/sightseeing-voucher/sightseeing-voucher.component';
import { AgentProfileComponent } from './components/user/agent/agent-profile/agent-profile.component';
import { CenninSearchComponent } from './components/cennin/package/cennin-search/cennin-search.component';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { PackageVoucherComponent } from './components/package/package-voucher/package-voucher.component';

export function getAuthServiceConfigs() {
  // click=  "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com"
  // Cennin=  "165521631707-0hkt0646vk1u99b5q28okrjbht8lsfpi.apps.googleusercontent.com"

  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        // cennin fb 481632676030440
        provider: new FacebookLoginProvider("481632676030440")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        //provider: new GoogleLoginProvider("")
        provider: new GoogleLoginProvider("165521631707-0hkt0646vk1u99b5q28okrjbht8lsfpi.apps.googleusercontent.com")
      }
    ]
  );
  return config;
}
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { FacebookComponent } from './components/share/facebook/facebook.component';
import { ConfirmationDialogComponent } from './components/common/confirmation-dialog/confirmation-dialog.component';
import { FlightComponent } from './components/flight/flight/flight.component';
import { SearchFlightComponent } from './components/flight/search-flight/search-flight.component';
import { CeninPackageTabsComponent } from './components/cennin/package/cenin-package-tabs/cenin-package-tabs.component';
import { NgbPaginationModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SessionExpirationAlert, SessionInteruptService } from 'session-expiration-alert';
import { FlightBookingComponent } from './components/flight/flight-booking/flight-booking.component';
import { FlightListComponent } from './components/flight/flight-list/flight-list.component';
import { Hi5flyContactComponent } from './components/hi5fly/hi5fly-contact/hi5fly-contact.component';
import { Hi5flyHomeComponent } from './components/hi5fly/hi5fly-home/hi5fly-home.component';
import { Hi5flyHeaderComponent } from './components/hi5fly/hi5fly-header/hi5fly-header.component';
import { Hi5flyFooterComponent } from './components/hi5fly/hi5fly-footer/hi5fly-footer.component';
import { Hi5flyAboutComponent } from './components/hi5fly/hi5fly-about/hi5fly-about.component';
import { Hi5flySearchComponent } from './components/hi5fly/hi5fly-search/hi5fly-search.component';
import { Hi5flyPackageComponent } from './components/hi5fly/hi5fly-package/hi5fly-package.component';
import { DetsinationJeddahComponent } from './components/travelsphere/location/detsination-jeddah/detsination-jeddah.component';
import { FlightFilterComponent } from './components/flight/flight-filter/flight-filter.component';
import { from } from 'rxjs';
import { CommSessionInteruptService } from './services/commons/comm-session-interupt.service';
import { TravelSightseeingComponent } from './components/travelsphere/sightseeing/travel-sightseeing/travel-sightseeing.component';
import { TravleThingsIconsComponent } from './components/travelsphere/thingstodo/travle-things-icons/travle-things-icons.component';
import { TravelHotelgridComponent } from './components/travelsphere/hotels/travel-hotelgrid/travel-hotelgrid.component';
import { TravelReasturantgridComponent } from './components/travelsphere/resturant/travel-reasturantgrid/travel-reasturantgrid.component';
import { IdealjungleHomeComponent } from './components/idealjungle/idealjungle-home/idealjungle-home.component';
import { IdealjungleHeaderComponent } from './components/idealjungle/idealjungle-header/idealjungle-header.component';
import { IdealjungleFooterComponent } from './components/idealjungle/idealjungle-footer/idealjungle-footer.component';
import { IdealjungleAboutComponent } from './components/idealjungle/idealjungle-about/idealjungle-about.component';
import { IdealjungleImagesliderComponent } from './components/idealjungle/idealjungle-imageslider/idealjungle-imageslider.component';
import { IdealjungleSearchComponent } from './components/idealjungle/idealjungle-search/idealjungle-search.component';
import { IdealjungleContactComponent } from './components/idealjungle/idealjungle-contact/idealjungle-contact.component';
import { IdealjunglePackagegridComponent } from './components/idealjungle/idealjungle-packagegrid/idealjungle-packagegrid.component';
import { PenchComponent } from './components/idealjungle/parks/pench/pench.component';
import { IdealjungleClientComponent } from './components/idealjungle/idealjungle-client/idealjungle-client.component';
import { TadobaComponent } from './components/idealjungle/parks/tadoba/tadoba.component';
import { NagziraComponent } from './components/idealjungle/parks/nagzira/nagzira.component';
import { KanhaComponent } from './components/idealjungle/parks/kanha/kanha.component';
import { ConfirmComponent } from './components/common/confirm/confirm.component';
import { TrivoLoginComponent } from './components/trivo/trivo-login/trivo-login.component';
import { TrivoFooterComponent } from './components/trivo/trivo-footer/trivo-footer.component';
import { LatestPanelComponent } from './components/search-panel/latest-panel/latest-panel.component';
import { HotelSearchComponent } from './components/search-panel/latest-panel/hotel-search/hotel-search.component';
import { SightseeingSearchComponent } from './components/search-panel/latest-panel/sightseeing-search/sightseeing-search.component';
import { PackageSearchComponent } from './components/search-panel/latest-panel/package-search/package-search.component';

import { TripserAboutUsComponent } from './components/tripser/tripser-about-us/tripser-about-us.component';
import { TripserTermsAndConditionsComponent } from './components/tripser/tripser-terms-and-conditions/tripser-terms-and-conditions.component';
import { TripserPrivacyPolicyComponent } from './components/tripser/tripser-privacy-policy/tripser-privacy-policy.component';
import { TripserThreeDSecureComponent } from './components/tripser/tripser-three-d-secure/tripser-three-d-secure.component';
import { AgentMarkupComponent } from './components/user/agent/agent-markup/agent-markup.component';
import { FlightConfirmComponent } from './components/flight/flight-confirm/flight-confirm.component';
import { FlightInvoiceComponent } from './components/flight/flight-invoice/flight-invoice.component';
import { FlightTicketComponent } from './components/flight/flight-ticket/flight-ticket.component';
import { FlightBookingsComponent } from './components/user/booking-report/flight-bookings/flight-bookings.component';
import { PackageInvoiceComponent } from './components/package/package-invoice/package-invoice.component';

const shareProp = {
  facebook: {
    icon: ['faFacebookF']
  }
};

@NgModule({
  declarations: [
    AppComponent,
    TourListComponent,
    HomeComponent,
    UserComponent,
    ProfileComponent,
    BookingsComponent,
    ChangepasswordComponent,
    WishlistComponent,
    EditprofileComponent,
    SignUpComponent,
    ShoppingCartComponent,
    AddCartComponent,
    CheckoutComponent,
    CartComponent,
    ListComponent,
    TourDetailsComponent,
    GridComponent,
    MapComponent,
    SearchComponent,
    TourwizardComponent,
    ReviewsComponent,
    AddReviewsComponent,
    PackageListComponent,
    PackageDetailsComponent,
    AddwishComponent,
    ReviewratingsComponent,
    AddRemoveWishComponent,
    PackageDocumentsComponent,
    ImageSliderComponent,
    HelpComponent,
    PackageBookingComponent,
    SliderComponent,
    CurrencyComponent,
    LoaderComponent,
    AboutComponent,
    ContactComponent,
    SightseeingComponent,
    PackageGridComponent,
    HotelComponent,
    SearchPanelComponent,
    HotelListComponent,
    HotelDetailsComponent,
    RateGroupComponent,
    HotelFilterComponent,
    HotelMapComponent,
    TravellerComponent,
    MessageComponent,
    HotelBookingComponent,
    ListLoaderComponent,
    SideLoaderComponent,
    HotelTermNConditionsComponent,
    TravellerDetailsComponent,
    HotelConfirmComponent,
    BookingReportComponent,
    NeedHelpComponent,
    AgentMarkupComponent,
    QuickLinksComponent,
    SubscribeComponent,
    SocialLinksComponent,
    DevelopComponent,
    PayPartnersComponent,
    BookingDetailsComponent,
    HotelVoucherComponent,
    HotelInvoiceComponent,
    HotelBookingsComponent,
    TourBookingsComponent,
    PackageBookingsComponent,
    RegisterComponent,
    CenninComponent,
    ImagesliderComponent,
    CenninHeaderComponent,
    CenninFooterComponent,
    TrivoServiceComponent,
    ClickSearchbarComponent,
    ClickHeaderComponent,
    ClickFooterComponent,
    CenninAboutComponent,
    ClickAboutComponent,
    ClickContactComponent,
    CenninContactComponent,
    CenninHomeComponent,
    PackageComponent,
    SearchPackageComponent,
    TrivoHeaderComponent,
    CenninPrivacypolicyComponent,
    PrivacyPolicyComponent,
    TermsAndConditionComponent,
    CenninTermsandconditionComponent,
    TravelHomeComponent,
    TravelHeaderComponent,
    TravelFooterComponent,
    TravelAboutComponent,
    TravelContactComponent,
    TravelHotelComponent,
    TravelResturantsComponent,
    TravelSliderComponent,
    TravelLocationGridComponent,
    TrivoContactComponent,
    ForgetPasswordComponent,
    AgencyStatementComponent,
    CreditInformationComponent,
    CreditRequestComponent,
    ExoticsHomeComponent,
    ExoticsHeaderComponent,
    ExoticsFooterComponent,
    ExoticsContactComponent,
    ExoticsAboutComponent,
    ExoticsSearchComponent,
    ExoticsPrivacypolicyComponent,
    ExoticsTermsandconditionsComponent,
    MeetmeHomeComponent,
    MeetmeAboutComponent,
    MeetmeContactComponent,
    ExoticsPackageComponent,
    MeetmeHeaderComponent,
    MeetmeFooterComponent,
    MeetmeSearchComponent,
    MeetmePackageGridComponent,
    MeetmeHomesliderComponent,
    TravelVideoComponent,
    TravelAboutdetailsComponent,
    LoginComponent,
    TravelVideoComponent,
    MeetmeSearchhomeComponent,
    SightseeingBookingDetailsComponent,
    SightseeingInvoiceComponent,
    SightseeingVoucherComponent,
    AgentProfileComponent,
    CenninSearchComponent,
    FacebookComponent,
    ConfirmationDialogComponent,
    FlightComponent,
    FlightConfirmComponent,
    FlightInvoiceComponent,
    FlightTicketComponent,
    FlightBookingsComponent,
    SearchFlightComponent,
    CeninPackageTabsComponent,
    FlightBookingComponent,
    FlightListComponent,
    Hi5flyContactComponent,
    Hi5flyHomeComponent,
    Hi5flyHeaderComponent,
    Hi5flyFooterComponent,
    Hi5flyAboutComponent,
    Hi5flySearchComponent,
    Hi5flyPackageComponent,
    DetsinationJeddahComponent,
    FlightFilterComponent,
    TravelSightseeingComponent,
    TravleThingsIconsComponent,
    TravelHotelgridComponent,
    TravelReasturantgridComponent,
    IdealjungleHomeComponent,
    IdealjungleHeaderComponent,
    IdealjungleFooterComponent,
    IdealjungleAboutComponent,
    IdealjungleImagesliderComponent,
    IdealjungleSearchComponent,
    IdealjungleContactComponent,
    IdealjunglePackagegridComponent,
    PenchComponent,
    IdealjungleClientComponent,
    TadobaComponent,
    NagziraComponent,
    KanhaComponent,
    ConfirmComponent,
    TrivoLoginComponent,
    TrivoFooterComponent,
    LatestPanelComponent,
    HotelSearchComponent,
    SightseeingSearchComponent,
    PackageSearchComponent,
    PackageVoucherComponent,
    // tripser

    TripserAboutUsComponent,

    TripserTermsAndConditionsComponent,

    TripserPrivacyPolicyComponent,

    TripserThreeDSecureComponent,

    PackageInvoiceComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatListModule,
    MatSelectModule,
    MatFormFieldModule,
    MatSliderModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRippleModule,
    HttpClientModule,
    MatTableModule,
    MatDatepickerModule,        // <----- import(must)
    NativeDateModule,
    MatNativeDateModule,        // <----- import for date formating(optional)
    MatFormFieldModule,
    MatRadioModule,
    MatInputModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),// ToastrModule added
    // Specify ng-circle-progress as an import
    //NgCircleProgressModule.forRoot(),
    SocialLoginModule,
    NgxPaginationModule,
    NgxUiLoaderModule,
    MatTreeModule,
    BrowserModule,
    OrderModule,
    GooglePlaceModule,
    NgxSkeletonLoaderModule,
    GridModule,
    InputsModule,
    PDFModule,
    ExcelModule,
    NgxDaterangepickerMd.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k'
    }),
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    }),
    SessionExpirationAlert.forRoot({ totalMinutes: 1 }),
    PDFExportModule,
    ButtonsModule,
    ExcelExportModule,
    HttpClientModule,       // (Required) For share counts
    HttpClientJsonpModule,  // (Optional) Add if you want tumblr share counts
    ShareButtonsModule.withConfig({ prop: shareProp }),
    NgbPaginationModule, NgbAlertModule, NgbModule
  ],
  bootstrap: [AppComponent],
  exports: [SignUpComponent, AddReviewsComponent, UserComponent],
  entryComponents: [UserComponent, ForgetPasswordComponent, SignUpComponent, AddReviewsComponent, ConfirmationDialogComponent, RateGroupComponent, ConfirmComponent],
  providers: [
    DatePipe,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs,
    },
    ApiUrlService,
    CookieService,
    {
      provide: SessionInteruptService,
      useClass: CommSessionInteruptService
    }
  ],
})
export class AppModule { }

