import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TourListComponent } from './components/tour-list/tour-list.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { BookingsComponent } from './components/user/bookings/bookings.component';
import { WishlistComponent } from './components/user/wishlist/wishlist.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';
import { SignUpComponent } from './components/user/sign-up/sign-up.component';
import { CartComponent } from './components/shopping-cart/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { TourDetailsComponent } from './components/tour-list/tour-details/tour-details.component';
import { PackageListComponent } from './components/package/package-list/package-list.component';
import { PackageDetailsComponent } from './components/package/package-details/package-details.component';
import { PackageBookingComponent } from './components/package/package-booking/package-booking.component';
import { LoaderComponent } from './components/loader/loader.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { SightseeingComponent } from './components/sightseeing/sightseeing.component';
import { HotelComponent } from './components/hotel/hotel/hotel.component';
import { HotelListComponent } from './components/hotel/hotel-list/hotel-list.component';
import { HotelBookingComponent } from './components/hotel/hotel-booking/hotel-booking.component';
import { HotelTermNConditionsComponent } from './components/hotel/hotel-term-n-conditions/hotel-term-n-conditions.component';
import { HotelConfirmComponent } from './components/hotel/hotel-confirm/hotel-confirm.component';
import { HotelDetailsComponent } from './components/hotel/hotel-details/hotel-details.component';
import { BookingReportComponent } from './components/user/booking-report/booking-report.component';
import { BookingDetailsComponent } from './components/user/booking-details/booking-details.component';
import { RouteAuthorizationService } from './services/commons/route-authorization.service';
import { HotelVoucherComponent } from './components/hotel/hotel-voucher/hotel-voucher.component';
import { HotelInvoiceComponent } from './components/hotel/hotel-invoice/hotel-invoice.component';
import { PackageComponent } from './components/package/package/package.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsAndConditionComponent } from './components/terms-and-condition/terms-and-condition.component';
import { RegisterComponent } from './components/user/agent/register/register.component';
import { AgencyStatementComponent } from './components/user/agent/agency-statement/agency-statement.component';
import { CreditInformationComponent } from './components/user/agent/credit-information/credit-information.component';
import { CreditRequestComponent } from './components/user/agent/credit-request/credit-request.component';
import { LoginComponent } from './components/user/agent/login/login.component';
import { SightseeingBookingDetailsComponent } from './components/user/booking-details/sightseeing-booking-details/sightseeing-booking-details.component';
import { AgentProfileComponent } from './components/user/agent/agent-profile/agent-profile.component';
import { SightseeingVoucherComponent } from './components/tour-list/sightseeing-voucher/sightseeing-voucher.component';
import { FlightComponent } from './components/flight/flight/flight.component';
import { FlightBookingComponent } from './components/flight/flight-booking/flight-booking.component';
import { FlightListComponent } from './components/flight/flight-list/flight-list.component';
import { DetsinationJeddahComponent } from './components/travelsphere/location/detsination-jeddah/detsination-jeddah.component';
import { TravelHotelgridComponent } from './components/travelsphere/hotels/travel-hotelgrid/travel-hotelgrid.component';
import { TravelReasturantgridComponent } from './components/travelsphere/resturant/travel-reasturantgrid/travel-reasturantgrid.component';
import { PenchComponent } from './components/idealjungle/parks/pench/pench.component';
import { TadobaComponent } from './components/idealjungle/parks/tadoba/tadoba.component';
import { NagziraComponent } from './components/idealjungle/parks/nagzira/nagzira.component';
import { KanhaComponent } from './components/idealjungle/parks/kanha/kanha.component'
import { TripserAboutUsComponent } from './components/tripser/tripser-about-us/tripser-about-us.component';
import { TripserTermsAndConditionsComponent } from './components/tripser/tripser-terms-and-conditions/tripser-terms-and-conditions.component';
import { TripserThreeDSecureComponent } from './components/tripser/tripser-three-d-secure/tripser-three-d-secure.component';
import { AgentMarkupComponent } from './components/user/agent/agent-markup/agent-markup.component';
import { PackageVoucherComponent } from './components/package/package-voucher/package-voucher.component';
import { PackageInvoiceComponent } from './components/package/package-invoice/package-invoice.component';
const routes: Routes = [

  {
    path: '', component: HomeComponent, data: {
      title: 'Home',
      description: ''
    }
  },
  {
    path: 'home', component: HomeComponent,
    data: {
      title: 'Home',
      description: ''
    }
  },
  {
    path: 'sightseeing', component: SightseeingComponent, data: {
      title: 'sightseeing',
      description: ''
    }
  },
  {
    path: 'tours-list', component: TourListComponent, data: {
      title: 'Tours',
      description: ''
    }
  },
  {
    path: 'profile', component: ProfileComponent, data: {
      title: 'Profile',
      description: ''
    }
  },
  {
    path: 'bookings', component: BookingsComponent, data: {
      title: 'Bookings',
      description: ''
    }
  },
  {
    path: 'wishlist', component: WishlistComponent, data: {
      title: 'Wish List',
      description: ''
    }
  },
  {
    path: 'changepassword', component: ChangepasswordComponent, data: {
      title: 'Change Password',
      description: ''
    }
  },
  { path: 'cart', component: CartComponent },
  {
    path: 'checkout', component: CheckoutComponent, data: {
      title: 'checkout',
      description: ''
    }
  },
  {
    path: 'tour-details', component: TourDetailsComponent, data: {
      title: 'Sightseeing Details',
      description: ''
    }
  },
  {
    path: 'package-search', component: PackageComponent, data: {
      title: 'Package Search',
      description: ''
    }
  },
  {
    path: 'package-list', component: PackageListComponent, canActivate: [RouteAuthorizationService], data: {
      title: 'Packages',
      description: ''
    }
  },
  {
    path: 'package-details', component: PackageDetailsComponent, data: {
      title: 'Package Details',
      description: ''
    }
  },
  {
    path: 'package-voucher', component: PackageVoucherComponent, data: {
      title: 'Package Voucher',
      description: ''
    }
  },
  {
    path: 'package-invoice', component: PackageInvoiceComponent, data: {
      title: 'Package Invoice',
      description: ''
    }
  },
  { path: 'bookings', component: BookingsComponent },
  {
    path: 'register', component: SignUpComponent, data: {
      title: 'Register',
      description: ''
    }
  },
  {
    path: 'package-booking', component: PackageBookingComponent, data: {
      title: 'Package Booking Details',
      description: ''
    }
  },
  { path: 'loader', component: LoaderComponent },
  {
    path: 'about', component: AboutComponent, data: {
      title: 'About',
      description: ''
    }
  },
  {
    path: 'contact', component: ContactComponent, data: {
      title: 'Contact',
      description: ''
    }
  },
  {
    path: 'hotel', component: HotelComponent, data: {
      title: 'Hotels Search',
      description: ''
    }
  },
  {
    path: 'hotel-list', component: HotelListComponent, data: {
      title: 'Hotels',
      description: ''
    }
  },
  {
    path: 'hotel-booking', component: HotelBookingComponent, data: {
      title: 'hotel booking',
      description: ''
    }
  },
  {
    path: 'hotel-term-n-conditions', component: HotelTermNConditionsComponent, data: {
      title: 'Hotel Terms',
      description: ''
    }
  },
  {
    path: 'hotel-confirm', component: HotelConfirmComponent, data: {
      title: 'Hotel Confirm',
      description: ''
    }
  },
  {
    path: 'hotel-details', component: HotelDetailsComponent, data: {
      title: 'Hotel Details',
      description: ''
    }
  },
  {
    path: 'hotel-voucher', component: HotelVoucherComponent, data: {
      title: 'Hotel Voucher',
      description: ''
    }
  },
  {
    path: 'hotel-invoice', component: HotelInvoiceComponent, data: {
      title: 'Hotel Invoice',
      description: ''
    }
  },
  {
    path: 'booking-report', component: BookingReportComponent, data: {
      title: 'Bookings',
      description: ''
    }
  },
  {
    path: 'booking-details', component: BookingDetailsComponent, data: {
      title: 'Booking Details',
      description: ''
    }
  },
  {
    path: 'privacy-policy', component: PrivacyPolicyComponent, data: {
      title: 'Privacy Policy',
      description: ''
    }
  },
  {
    path: 'terms-and-conditions', component: TermsAndConditionComponent, data: {
      title: 'Terms-and-Conditions',
      description: ''
    }
  },
  {
    path: 'agent-register', component: RegisterComponent, data: {
      title: 'Registration',
      description: ''
    }
  },
  {
    path: 'agent-profile', component: AgentProfileComponent, data: {
      title: 'Profile',
      description: ''
    }
  },
  {
    path: 'agency-statement', component: AgencyStatementComponent, data: {
      title: 'Agency Statement',
      description: ''
    }
  },
  {
    path: 'agent-markup', component: AgentMarkupComponent, data: {
      title: 'Markup',
      description: ''
    }
  },
  {
    path: 'credit-information', component: CreditInformationComponent, data: {
      title: 'Credit Information',
      description: ''
    }
  },
  {
    path: 'credit-request', component: CreditRequestComponent, data: {
      title: 'Credit Request',
      description: ''
    }
  },
  {
    path: 'sightseeing-booking-details', component: SightseeingBookingDetailsComponent, data: {
      title: 'sightseeing booking details',
      description: ''
    }
  },
  {
    path: 'login', component: LoginComponent, data: {
      title: 'login',
      description: ''
    }
  },
  {
    path: 'sightseeing-tickets', component: SightseeingVoucherComponent, data: {
      title: 'Tickets',
      description: ''
    }
  },

  // flight routing start
  {
    path: 'flight', component: FlightComponent, data: {
      title: 'Flight',
      description: ''
    }
  },

  {
    path: 'flight-booking', component: FlightBookingComponent, data: {
      title: 'Flight Booking',
      description: ''
    }
  },
  {
    path: 'flight-list', component: FlightListComponent, data: {
      title: 'Flight List',
      description: ''
    }
  },

  // Travelsphere Pages 
  {
    path: 'city', component: DetsinationJeddahComponent, data: {
      title: 'Things to do in Jeddah',
      description: ''
    }
  },
  {
    path: 'hotels', component: TravelHotelgridComponent, data: {
      title: 'Hotels',
      description: ''
    }
  },
  {
    path: 'restaurants', component: TravelReasturantgridComponent, data: {
      title: 'Hotels',
      description: ''
    }
  },
  {
    path: 'sightseeings', component: DetsinationJeddahComponent, data: {
      title: 'Sigtseeing',
      description: ''
    }
  },
  {
    path: 'things-to-do', component: DetsinationJeddahComponent, data: {
      title: 'Sigtseeing',
      description: ''
    }
  },
  {
    path: 'pench', component: PenchComponent, data: {
      title: 'Parks | Pench',
      description: ''
    }
  },
  {
    path: 'tadoba', component: TadobaComponent, data: {
      title: 'Parks | tadoba',
      description: ''
    }
  },
  {
    path: 'nagzira', component: NagziraComponent, data: {
      title: 'Parks | nagzira',
      description: ''
    }
  },
  {
    path: 'kanha', component: KanhaComponent, data: {
      title: 'Parks | kanha',
      description: ''
    }
  },

  // Tripser

  {
    path: 'about-us', component: TripserAboutUsComponent, data: {
      title: 'about us',
      description: ''
    }
  },

  {
    path: '3d-secure', component: TripserThreeDSecureComponent, data: {
      title: '3d secure',
      description: ''
    }
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
